#!/usr/bin/env xcrun --sdk macosx swift

//
//  genstrings.swift
//  Keanu
//
//  Created by Benjamin Erhart on 03.11.21.
//  Copyright © 2021 Guardian Project. All rights reserved.
//

import Foundation

// MARK: Config

let roots = [
    resolve("../../KeanuCore/"),
    resolve("../../Keanu/"),
    resolve("../../KeanuExtension/")]



let regex = try? NSRegularExpression(pattern: "\"(([^\"\\\\]|\\\\.|\\\\\\n)*)\"\\.localize\\(")

let target = resolve("../Shared/en.lproj/Localizable.strings")

// There's some dummy text in XIBs, which we want to ignore.
let xibIgnorelist = ["foobar", "lorem ipsum", ">>", "^<x> Untrusted New Devices for <user>$",
                     "^@bob:matrix.org$", "^Action 1$", "^Action 2$", "^Button$",
                     "^Label$", "^Subtitle$", "^Title$", "^\\\\u\\{e1b1\\}$",
                     "^device key fingerprint$"]


// MARK: Helper Methods

func resolve(_ path: String) -> URL {
    let cwd = URL(fileURLWithPath: FileManager.default.currentDirectoryPath)
    let script = URL(fileURLWithPath: CommandLine.arguments.first ?? "", relativeTo: cwd).deletingLastPathComponent()

    return URL(fileURLWithPath: path, relativeTo: script)
}

func getAllFiles(with extension: String, in directories: [URL]) -> [URL] {
    var files = [URL]()

    for dir in directories {
        let enumerator = FileManager.default.enumerator(at: dir, includingPropertiesForKeys: [.isDirectoryKey]) { url, error in
            print(error.localizedDescription)

            return true
        }

        while let file = enumerator?.nextObject() as? URL {
            if (try? file.resourceValues(forKeys: [.isDirectoryKey]).isDirectory) ?? false {
                continue
            }

            if file.pathExtension == `extension` {
                files.append(file)
            }
        }
    }

    return files
}

var ignorelistCache = [String: NSRegularExpression]()

func isIgnored(_ value: String) -> Bool {
    // Ignore keys with one character. That's most probably references to
    // an icon font.
    guard value.count > 1 else {
        return true
    }

    let range = NSRange(value.startIndex ..< value.endIndex, in: value)

    for pattern in xibIgnorelist {
        let regex = ignorelistCache[pattern] ?? (try? NSRegularExpression(pattern: pattern, options: .caseInsensitive))
        ignorelistCache[pattern] = regex

        if regex?.numberOfMatches(in: value, range: range) ?? 0 > 0 {
            return true
        }
    }

    return false
}


// MARK: Main

var translationTable = [String: Set<URL>]()

for file in getAllFiles(with: "swift", in: roots) {
    guard let content = try? String(contentsOf: file) else {
        continue
    }

    for match in regex?.matches(in: content, range: NSRange(content.startIndex ..< content.endIndex, in: content)) ?? [] {
        guard match.numberOfRanges > 1,
              let r = Range(match.range(at: 1), in: content)
        else {
            continue
        }

        let key = String(content[r])

        translationTable[key] = Set(arrayLiteral: file).union(translationTable[key] ?? [])
    }
}

let tempFile = FileManager.default.temporaryDirectory.appendingPathComponent("temp.strings")

for file in getAllFiles(with: "xib", in: roots) {
    let task = Process()
    task.launchPath = "/usr/bin/env"
    task.arguments = ["ibtool", file.path, "--generate-strings-file", tempFile.path]
    task.launch()
    task.waitUntilExit()

    guard let strings = NSDictionary(contentsOf: tempFile) as? Dictionary<String, String> else {
        continue
    }

    // The key is a random identifier of the Interface Builder UIView element,
    // the value is the actual text to localize.
    for key in strings.values {
        if isIgnored(key) {
            continue
        }

        translationTable[key] = Set(arrayLiteral: file).union(translationTable[key] ?? [])
    }
}

let df = DateFormatter()
df.locale = Locale(identifier: "en_US_POSIX")
df.dateStyle = .short
df.timeStyle = .none

let day = df.string(from: Date())

df.setLocalizedDateFormatFromTemplate("YYYY")

let year = df.string(from: Date())

var output = "//\n"
output += "// \(target.lastPathComponent)\n"
output += "// Keanu\n"
output += "// \n"
output += "// Created by genstrings.swift on \(day).\n"
output += "// Copyright © \(year) Guardian Project. All rights reserved.\n"
output += "//\n"
output += "\n"

for key in translationTable.keys.sorted() {
    output += "/* \(translationTable[key]?.map({ $0.lastPathComponent }).sorted().joined(separator: ", ") ?? "Unknown Source") */\n"
    output += "\"\(key)\" = \"\(key)\";\n"
    output += "\n"
}

try output.write(to: target, atomically: true, encoding: .utf8)
