//
//  NotificationService.swift
//  NotificationServiceExtension
//
//  Created by Benjamin Erhart on 12.07.22.
//  Copyright © 2022 Guardian Project. All rights reserved.
//

import KeanuNSE

class NotificationService: BaseNotificationService {

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        BaseNotificationService.config = Config.self
        BaseNotificationService.applicationGroupIdentifier = Constants.appGroup as String?

        super.didReceive(request, withContentHandler: contentHandler)
    }
}
