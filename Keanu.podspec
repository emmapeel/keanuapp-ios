#
# Be sure to run `pod lib lint Keanu.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Keanu'
  s.version          = '0.1.0'
  s.summary          = 'Keanu is a native iOS Swift implementation of a Matrix client.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Keanu is a native iOS Swift implementation of a Matrix client.

It is provided as a CocoaPods library, so others can easily spin off of it
without the need to fork it. That way, spin-offs can easily stay up to date.
                       DESC

  s.homepage         = 'https://gitlab.com/keanuapp/keanuapp-ios.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'Apache License, Version 2.0', :file => 'LICENSE' }
  s.author           = { 'Guardian Project' => 'support@guardianproject.info' }
  s.source           = { :git => 'https://gitlab.com/keanuapp/keanuapp-ios.git', :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/guardianproject'

  s.swift_version = '5.0'

  s.ios.deployment_target = '13.0'

  s.source_files = 'Keanu/Classes/**/*'

  s.resources = [
    'Keanu/Assets/**/*.storyboard',
    'Keanu/Assets/**/*.xib',
    'Keanu/Assets/**/*.ttf',
    'Keanu/Assets/*.css',
    'Keanu/Assets/**/*.xcassets',
    'Keanu/Assets/*.lproj/*.*',
  ]

  s.dependency 'KeanuCore'

  s.dependency 'Eureka', '~> 5.2'
  s.dependency 'ViewRow', '~> 0.6'
  s.dependency 'PureLayout', '~> 3.1'
  s.dependency 'libPhoneNumber-iOS', '~> 0.9'
  s.dependency 'INSPhotoGallery', '~> 1.2'
  s.dependency 'FLAnimatedImage', '~> 1.0'
  s.dependency 'SearchTextField', '~> 1.2'
  s.dependency 'RichEditorView'
  s.dependency 'AFNetworking'
  s.dependency 'ISEmojiView', '~> 0.3'
  s.dependency 'ZXingObjC', '~> 3.6'
  s.dependency 'MobileVLCKit', '~> 3.5'
  s.dependency 'MBProgressHUD', '~> 1.1'

  # Thanks, VLCKit!
  s.pod_target_xcconfig = {
      'ENABLE_BITCODE' => 'NO'
  }

end
