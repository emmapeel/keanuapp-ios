//
//  Config.swift
//  Keanu
//
//  Created by Benjamin Erhart on 10.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//
//
// This is gladly taken from:
// https://edit.theappbusiness.com/modular-ios-part-4-sharing-configuration-between-modules-b08a31490447

import FontBlaster
import Localize
import MatrixSDK

// TODO: Actually load Localized.strings file!

/**
 Protocol to define the needed configuration for the Keanu module.
 */
public protocol KeanuConfig {

    /**
     Default Matrix home server domain. (Just domain, not a complete URL!)
    */
    static var defaultHomeServer: String { get }

    /**
     Default Matrix ID server domain. (Just domain, not a complete URL!)
    */
    static var defaultIdServer: String { get }

    /**
     Scheme, this app registered for.
    */
    static var appUrlScheme: String { get }

    /**
     The push server app ID for development.
    */
    static var pushAppIdDev: String { get }

    /**
     The push server app ID for release.
    */
    static var pushAppIdRelease: String { get }

    /**
     Push server domain. (Just domain, not a complete URL!)
     */
    static var pushServer: String { get }

    /**
     The app group ID, used to share data between the main app and extensions.
     
     If you change this, Matrix won't find its old config and cache anymore!
    */
    static var appGroupId: String? { get }
    
    /**
     Host for universal links for this app.
     */
    static var universalLinkHost: String { get }

    /**
     Format of app invite links, use %@ for user id placeholder. Note - currently the user id is assumed to always be at the end of the string, e.g. "https://example.com/i/#%@".
     */
    static var inviteLinkFormat: String { get }

    /**
     List of alternate domains for given server domains.

     A server domain will be replaced with a random alternate domain of the list before connecting that Matrix server,
     if that server domain is a key in this dictionary and the according list is non-empty.
     */
    static var altDomains: [String: [String]] { get }
}

/**
 Object storing configuration which was handed over at runtime via `#setup`.
 */
public class ConfigType {

    static fileprivate var shared: ConfigType?

    public let defaultHomeServer: String
    public let defaultIdServer: String
    public let pushAppIdDev: String
    public let pushAppIdRelease: String
    public let pushServer: String
    public let appGroupId: String?
    public let universalLinkHost: String
    public let inviteLinkFormat: String
    public let altDomains: [String: [String]]

    fileprivate init(_ config: KeanuConfig.Type) {
        defaultHomeServer = config.defaultHomeServer
        defaultIdServer = config.defaultIdServer
        pushAppIdDev = config.pushAppIdDev
        pushAppIdRelease = config.pushAppIdRelease
        pushServer = config.pushServer
        appGroupId = config.appGroupId
        universalLinkHost = config.universalLinkHost
        inviteLinkFormat = config.inviteLinkFormat
        altDomains = config.altDomains
    }
}

/**
 Set up Keanu with the given configuration at runtime.

 Best called in `AppDelegate`!

 - parameter config: The configuration object conforming to `ConfigType`.
 */
public func setUp(with config: KeanuConfig.Type) {
    ConfigType.shared = ConfigType(config)
}

/**
 Load Keanu bundled fonts. This is needed to show some of the
 icons used properly.

 Best called in `AppDelegate`!
 */
public func loadFonts() {
    FontBlaster.blast(bundle: bundle)
}

/**
 Set up Keanu to use a localization provided by the consuming app.

 Best called in `AppDelegate`!

 - parameter fileName: The base filename of the localization file.
 - parameter bundle: The bundle to search the localization file in. Optional, defaults to the main bundle.
 - parameter provider: The file type to read localization from. Optional, defaults to Apple Strings file.
 */
public func setUpLocalization(fileName: String, bundle: Bundle = Bundle.main, provider: LocalizeType = .strings) {
    Localize.update(provider: provider)
    Localize.update(bundle: bundle)
    Localize.update(fileName: fileName)
}

/**
 Set up MatrixSDK with some preferred options.
 */
public func setUpMatrix() {
    let sdkOptions = MXSDKOptions.sharedInstance()

    // Don't use identicon avatars, we have our own implementation.
    sdkOptions.disableIdenticonUseForUserAvatar = true
    
    // Enable e2e encryption for newly created MXSession.
    sdkOptions.enableCryptoWhenStartingMXSession = true

    // Use UIKit BackgroundTask for handling background tasks in the SDK.
    sdkOptions.backgroundModeHandler = MXUIKitBackgroundModeHandler()

    // Configure the app group ID.
    sdkOptions.applicationGroupIdentifier = ConfigType.shared?.appGroupId

//    sdkOptions.httpAdditionalHeaders = ["X-Token": "foobar"]
    
    // Kind of ugly, but swizzle the MXEvent to handle arbitrary userData
    MXEvent.enableUserDataFunctionality()

    MXKeyProvider.sharedInstance().delegate = EncryptionKeyManager.shared
}

/**
 Keanu internal configuration struct.
 */
public var Config: ConfigType {
    if let config = ConfigType.shared {
        return config
    }

    fatalError("Please set the config for \(bundle)!")
}

public let bundle = Bundle(for: ConfigType.self)
