//
//  RoomCell.swift
//  Keanu
//
//  Created by Benjamin Erhart on 06.04.22.
//  Copyright © 2022 Guardian Project. All rights reserved.
//

import MatrixSDK

open class RoomCell: UITableViewCell {
    
    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    open class var defaultReuseId: String {
        return String(describing: self)
    }
    
    open class var height: CGFloat {
        return 70
    }
    
    @IBOutlet weak var avatarImg: AvatarView!
    @IBOutlet weak var unreadLb: UILabel!
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var timestampLb: UILabel!
    @IBOutlet weak var infoLb: UILabel!
    
    var lastProcessedEventId: String?
    var lastProcessedEventText: String?
    
    /**
     Applies an `MXRoomSummary` to this cell.
     
     - returns: self for convenience.
     */
    open func apply(_ roomSummary: MXRoomSummary) -> Self {
        avatarImg.load(for: roomSummary)
        
        let count = roomSummary.notificationCount
        unreadLb.text = Formatters.format(int: Int(count))
        unreadLb.isHidden = count < 1
        
        titleLb.text = roomSummary.displayName
        
        if let ts = roomSummary.lastMessage?.originServerTs {
            timestampLb.text = Formatters.format(friendlyTimestamp: Date(timeIntervalSince1970: Double(ts) / 1000))
        }
        else {
            timestampLb.text = nil
        }
        
        if lastProcessedEventId == roomSummary.lastMessage?.eventId {
            // Already processed, use that text.
            infoLb.text = lastProcessedEventText
        } else {
            // Use an approximate placeholder to allow for better row height calculation.
            lastProcessedEventId = roomSummary.lastMessage?.eventId
            lastProcessedEventText = ""
            let message = roomSummary.lastMessage?.text
            infoLb.text = (message?.isEmpty ?? true) ? roomSummary.topic : message
            let eventId = lastProcessedEventId
            roomSummary.getLastMessageFormatted { [weak self] message in
                // Make sure this cell still exists and shows the same room summary as earlier...
                guard let self = self, eventId == self.lastProcessedEventId else { return }
                let updatedMessage = (message?.isEmpty ?? true) ? roomSummary.topic : message
                if updatedMessage != self.infoLb.text {
                    self.infoLb.text = updatedMessage
                    self.lastProcessedEventText = updatedMessage
                    // Get table view to update cell height correctly after async content update here...
                    if let tableView = self.superview(of: UITableView.self), let indexPath = tableView.indexPath(for: self) {
                        tableView.reloadRows(at: [indexPath], with: .automatic)
                    }
                }
            }
        }
        return self
    }
}
