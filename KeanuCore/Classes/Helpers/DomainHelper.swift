//
//  DomainHelper.swift
//  KeanuCore
//
//  Created by Benjamin Erhart on 22.09.21.
//

import Foundation

/**
 Helps with replacing home and identity server domains with randomly selected alternative domains.

 The random selection is remembered and stays the same per app session.
 */
open class DomainHelper {

    public static var shared = DomainHelper()

    private var sessionMap = [String: String]()


    /**
     Replaces the given home and identity server domains with randomly selected alternative domains, if available.
     */
    public func replaceWithAltDomain(homeServer: inout URL?, idServer: inout URL?) {
        if let altDomain = getAltDomain(for: homeServer) {
            homeServer = DomainHelper.replaceDomain(of: homeServer, with: altDomain)
        }

        if let altDomain = getAltDomain(for: idServer) {
            idServer = DomainHelper.replaceDomain(of: idServer, with: altDomain)
        }
    }

    /**
     Start all active account sessions. Replace home and identity server domains with alternative domains,
     if available.
     */
    public func prepareSessionForActiveAccounts() {
        for account in MXKAccountManager.shared().accounts {
            var homeServer: URL? = nil
            var idServer: URL? = nil

            if let hs = account.mxCredentials.homeServer {
                homeServer = URL(string: hs)
            }

            if let ids = account.mxCredentials.identityServer {
                idServer = URL(string: ids)
            }

            replaceWithAltDomain(homeServer: &homeServer, idServer: &idServer)

            account.mxCredentials.homeServer = homeServer?.absoluteString
            account.mxCredentials.identityServer = idServer?.absoluteString
        }

        MXKAccountManager.shared().prepareSessionForActiveAccounts()
    }


    // MARK: Private Methods

    /**
     Randomly selects an alternative domain for a given domain, if available, once, and remembers that
     mapping for that session.

     - returns: `nil` if `url` or `url.host` is `nil` or there is no alt domain, or the alternative domain.
     */
    private func getAltDomain(for url: URL?) -> String? {
        guard var domain = url?.host else {
            return nil
        }

        domain = findMainDomain(domain)

        // If there already is a mapping, return that.
        if let altDomain = sessionMap[domain] {
            return altDomain
        }

        if let altDomains = Config.altDomains[domain],
           !altDomains.isEmpty
        {
            sessionMap[domain] = altDomains[Int.random(in: 0 ..< altDomains.count)]
        }

        return sessionMap[domain]
    }

    /**
     Find the base domain of a given domain. Could well be, that the credentials are stored with an
     alternate domain, already, so we need to find the main domain again.
     */
    private func findMainDomain(_ domain: String) -> String {
        if Config.altDomains[domain] != nil {
            return domain
        }

        for main in Config.altDomains.keys {
            if Config.altDomains[main]?.contains(domain) == true {
                return main
            }
        }

        return domain
    }

    /**
     Replace the domain (aka. host) of a `URL` with the given domain and return that, or the original on failure.

     - returns: A `URL` with the domain (aka. host) replaced, or the same on failure.
     */
    private class func replaceDomain(of url: URL?, with domain: String) -> URL? {
        guard let url = url else {
            return nil
        }

        var urlComp = URLComponents(url: url, resolvingAgainstBaseURL: false)
        urlComp?.host = domain

        return urlComp?.url ?? url
    }
}
