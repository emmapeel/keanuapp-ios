//
//  AttachmentItemProvider.swift
//  KeanuCore
//
//  Created by Benjamin Erhart on 26.08.19.
//

import MatrixSDK
import QuickLook

public enum AttachmentError: LocalizedError {

    case fileNotFound

    public var errorDescription: String? {
        return "File not found.".localize()
    }
}

/**
 Provides asynchronous preparation for sharing an attachment, which can't be
 achieved by implementing the `UIActivityItemSource` protocol.

 Also implements the `QLPreviewItem` protocol, so can be used for QuickLook, too.
 */
open class AttachmentItem: UIActivityItemProvider, QLPreviewItem {

    public let attachment: MXKAttachment
    
    /**
     Event used for getting meta data, like date and sender (they are not present in MXKAttachment)
     */
    public let event: MXEvent
    
    public var error: Error?

    private let completed: ((AttachmentItem) -> ())?

    private var file: URL?
    private var tempFile: URL?

    private var done = false {
        didSet {
            if done {
                // Delay, in case prepare is faster than expected.
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.completed?(self)
                }
            }
        }
    }

    private let fm = FileManager.default


    public init(_ attachment: MXKAttachment, event: MXEvent, completed: ((AttachmentItem) -> ())?) {
        self.attachment = attachment
        self.event = event
        self.completed = completed

        super.init(placeholderItem: URL(fileURLWithPath: attachment.cacheFilePath ?? ""))

        prepare()
    }


    // MARK: UIActivityItemProvider

    override open var item: Any {
        return previewItemURL ?? URL(fileURLWithPath: "")
    }


    // MARK: QLPreviewItem

    public var previewItemURL: URL? {
        // Wait for preparation.
        while !done {
            Thread.sleep(forTimeInterval: 0.1)
        }

        return file
    }

    public var previewItemTitle: String? {
        return attachment.fileDisplayName
    }


    deinit {
        if let tempFile = tempFile {
            try? fm.removeItem(at: tempFile)
            self.tempFile = nil
        }

        if let file = file {
            try? fm.removeItem(at: file)
            self.file = nil
        }
    }


    // MARK: Private Methods

    /**
     Download and decrypt (if encrypted) the attachment and prepare a
     hard link of that file, so it bears the correct file name which is important
     for sharing.

     This is asynchronous and will return early.
     Wait for the `done` flag to become true, before trying to use anything.
    */
    private func prepare() {
        if attachment.isEncrypted {
            attachment.decrypt(toTempFile: { file in
                if let file = file {
                    self.tempFile = URL(fileURLWithPath: file)
                    self.file = self.hardlink(at: self.tempFile!)
                }

                if self.file == nil {
                    self.error = AttachmentError.fileNotFound
                }

                self.done = true
            }) { error in
                self.error = error
                self.done = true
            }
        }
        else {
            attachment.prepare({
                if let cacheFilePath = self.attachment.cacheFilePath,
                    self.fm.fileExists(atPath: cacheFilePath) {

                    self.file = self.hardlink(at: URL(fileURLWithPath: cacheFilePath))
                }

                if self.file == nil {
                    self.error = AttachmentError.fileNotFound
                }

                self.done = true
            }) { error in
                self.error = error
                self.done = true
            }
        }
    }

    /**
     Creates a file system hard link between the given file and a file, which will be
     situated in `MXMediaManager`'s cache path and bear the original file name
     of the attachment, in order so when it gets shared to another app, it will
     have the correct name.

     Why a hard link? As per comment in `MXKAttachment#prepareShare`, symbolic
     links don't always work properly. Their solution was to copy the file, but
     that seems a lot of overhead, if we could achieve the same with hard linking.

     - parameter src: The file URL that identifies the source of the link.
       The URL in this parameter must not be a file reference URL; it must specify
       the actual path to the item. The value in this parameter must not be nil.
     - returns: The URL of the created file or nil, if unsuccessful.
    */
    private func hardlink(at src: URL) -> URL? {
        guard let filename = attachment.originalFileName else {
            return nil
        }

        var dst = URL(fileURLWithPath: MXMediaManager.getCachePath()).appendingPathComponent(filename)

        // Maaan. If a video file doesn't have a proper video file extension,
        // it won't get played. So: try harder to have a good extension!
        if dst.pathExtension.isEmpty {
            dst.appendPathExtension(src.pathExtension)
        }

        try? fm.removeItem(at: dst)

        do {
            try fm.linkItem(at: src, to: dst)

            return dst
        } catch {
            return nil
        }
    }
}
