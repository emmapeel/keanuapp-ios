//
//  AvatarAnd3LabelsCell.swift
//  Keanu
//
//  Created by N-Pex on 26.09.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixSDK

open class AvatarAnd3LabelsCell: UITableViewCell {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    open class var defaultReuseId: String {
        return String(describing: self)
    }

    open class var height: CGFloat {
        return 99
    }

    @IBOutlet weak var avatarImg: AvatarView!
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var subtitleLb: UILabel!
    @IBOutlet weak var infoLb: UILabel!

    /**
     Applies a `Friend` to this cell.
     
     - returns: self for convenience.
     */
    open func apply(_ friend: Friend) -> AvatarAnd3LabelsCell {
        avatarImg.load(friend: friend)
        titleLb.text = friend.name
        subtitleLb.text = friend.matrixId
        infoLb.text = friend.presence != .unknown ? friend.friendlyPresence : nil

        return self
    }

    /**
     Applies a `MXKAccount` to this cell.

     - returns: self for convenience.
    */
    open func apply(_ account: MXKAccount) -> AvatarAnd3LabelsCell {
        avatarImg.load(account: account)

        titleLb.text = account.friendlyName
        subtitleLb.text = account.matrixId

        infoLb.text = account.isDisabled
            ? "disabled".localize()
            // When we're actually not connected but in a state in between,
            // presence shows "online" which is actually wrong, so consider
            // network status, too.
            : account.mxSession == nil || account.mxSession!.state != .running
                ? "offline".localize()
                : account.mxSession?.myUser?.friendlyPresence
                ?? "enabled".localize()

        return self
    }
}
