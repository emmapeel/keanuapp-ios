//
//  MenuButton.swift
//  Keanu
//
//  Created by Benjamin Erhart on 27.01.22.
//

import UIKit

open class MenuButton: UIButton {

    private var icon: String?
    private var label: String?

    public init(_ icon: String, _ label: String) {
        self.icon = icon
        self.label = label

        super.init(frame: .zero)

        setup()
    }

    required public init?(coder: NSCoder) {
        icon = coder.decodeObject(forKey: "icon") as? String
        label = coder.decodeObject(forKey: "label") as? String

        super.init(coder: coder)

        setup()
    }

    open override func encode(with coder: NSCoder) {
        super.encode(with: coder)

        coder.encode(icon, forKey: "icon")
        coder.encode(label, forKey: "label")
    }


    open func setup() {
        let title = NSMutableAttributedString()

        if let icon = icon {
            title.append(NSAttributedString(string: icon,
                                            attributes: [.font: UIFont.materialIcons(),
                                                         .baselineOffset: -5]))
            title.append(NSAttributedString(string: " ", attributes: nil))
        }

        if let label = label {
            title.append(NSAttributedString(string: label, attributes: nil))
        }

        setAttributedTitle(title)

        setTitleColor(.keanuLabel)

        translatesAutoresizingMaskIntoConstraints = false
    }
}
