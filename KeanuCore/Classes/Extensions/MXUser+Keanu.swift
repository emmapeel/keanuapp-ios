//
//  MXUser+Keanu.swift
//  Keanu
//
//  Created by Benjamin Erhart on 24.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixSDK
import CoreServices

extension MXUser: UIActivityItemSource {

    /**
     The `displayname` if not empty or the local part of the `userId`.
     */
    public var friendlyName: String {
        get {
            return Friend.getFriendlyName(displayname, userId)
        }
    }

    /**
     Localized, human readable representation of `presence`.
     
     In case of `MXPresenceUnavailable` and `MXPresenceOffline also contains
     `friendlyLastActiveAgo`.
    */
    public var friendlyPresence: String {
        get {
            var text: String
            
            switch presence {
            case .online:
                text = "online".localize()
                
            case .unavailable:
                // Argument is a localized version of something like '1 hour, 25 minutes'.
                text = "unavailable, last active % ago".localize(value: friendlyLastActiveAgo)
                
            case .offline:
                // Argument is a localized version of something like '1 hour, 25 minutes'.
                text = "offline, last active % ago".localize(value: friendlyLastActiveAgo)
                
            default:
                text = "presence unclear".localize()
            }
            
            return text
        }
    }
    
    /**
     Localized, human readable representation of `lastActiveAgo`.
    */
    public var friendlyLastActiveAgo: String {
        get {
            let interval = Double(lastActiveAgo) / 1000

            if interval > 3 * 365 * 24 * 60 * 60 {
                return "years".localize()
            }

            return Formatters.format(lastActive: interval)
                ?? "\(lastActiveAgo / 60000) min"
        }
    }

    
    // MARK: UIActivityItemSource
    
    public func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return userId!
    }
    
    public func activityViewController(_ activityViewController: UIActivityViewController,
                                itemForActivityType activityType: UIActivity.ActivityType?) -> Any?
    {
        return URL(string: String(format: Config.inviteLinkFormat, userId))
    }
    
    public func activityViewController(_ activityViewController: UIActivityViewController,
                                subjectForActivityType activityType: UIActivity.ActivityType?) -> String
    {
        // Add Friend invitation subject; placeholder is friendly name of user.
        return "% wants to chat.".localize(value: friendlyName)
    }

    public func activityViewController(_ activityViewController: UIActivityViewController,
                                       dataTypeIdentifierForActivityType activityType: UIActivity.ActivityType?) -> String
    {
        kUTTypeURL as String
    }

    public func activityViewController(_ activityViewController: UIActivityViewController,
                                       thumbnailImageForActivityType activityType: UIActivity.ActivityType?,
                                       suggestedSize size: CGSize) -> UIImage?
    {
        return nil
    }
}
