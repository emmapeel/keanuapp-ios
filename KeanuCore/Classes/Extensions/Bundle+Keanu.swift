//
//  Bundle+displayName.swift
//  Keanu
//
//  Created by Benjamin Erhart on 01.10.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import Foundation

public extension Bundle {

    static let matrixKitAssets: Bundle? = {
        guard let url = Bundle(for: ConfigType.self).url(forResource: "MatrixKitAssets", withExtension: "bundle")
        else {
            return nil
        }

        return Bundle(url: url)
    }()


    var displayName: String {
        return object(forInfoDictionaryKey: "CFBundleDisplayName") as? String
            ?? object(forInfoDictionaryKey: kCFBundleNameKey as String) as? String
            ?? ""
    }

    var version: String {
        return infoDictionary?["CFBundleShortVersionString"] as? String ?? "unknown"
    }

    var build: String {
        Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "unknown"
    }
}
