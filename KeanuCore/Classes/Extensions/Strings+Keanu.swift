//
//  Strings+KeanuCore.swift
//  KeanuCore
//
//  Created by Benjamin Erhart on 11.11.22.
//

import Foundation


public extension VectorL10n {

    static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
        let format = NSLocalizedString(key, tableName: table, bundle: .matrixKitAssets ?? .main, comment: "")

        return String(format: format, locale: .current, arguments: args)
    }
}
