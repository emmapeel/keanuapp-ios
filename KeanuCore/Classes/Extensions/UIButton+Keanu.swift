//
//  UIButton+Keanu.swift
//  KeanuCore
//
//  Created by Benjamin Erhart on 04.03.20.
//

import UIKit

extension UIButton {

    /**
     Sets the title to use for all states except `reserved`.

     Use this method to set the title for the button. The title you specify derives its formatting from the button’s associated label object.
     If you set both a title and an attributed title for the button, the button prefers the use of the attributed title over this one.

     - parameter title: The title to use for all states.
     */
    public func setTitle(_ title: String?) {
        setTitle(title, for: .application)
        setTitle(title, for: .disabled)
        setTitle(title, for: .focused)
        setTitle(title, for: .highlighted)
        setTitle(title, for: .normal)
        setTitle(title, for: .selected)
    }

    /**
     Sets the attributed title to use for all states except `reserved`.

     Use this method to set the attributed title for the button. The title you specify derives its formatting from the button’s associated label object.
     If you set both a title and an attributed title for the button, the button prefers the use of the attributed title over this one.

     - parameter title: The attributed title to use for all states.
     */
    public func setAttributedTitle(_ title: NSAttributedString) {
        setAttributedTitle(title, for: .application)
        setAttributedTitle(title, for: .disabled)
        setAttributedTitle(title, for: .focused)
        setAttributedTitle(title, for: .highlighted)
        setAttributedTitle(title, for: .normal)
        setAttributedTitle(title, for: .selected)
    }

    /**
     Sets the color of the title to use for all states except `reserved`.

     - parameter color: The color of the title to use for all states.
     */
    public func setTitleColor(_ color: UIColor?) {
        setTitleColor(color, for: .application)
        setTitleColor(color, for: .disabled)
        setTitleColor(color, for: .focused)
        setTitleColor(color, for: .highlighted)
        setTitleColor(color, for: .normal)
        setTitleColor(color, for: .selected)
    }
}
