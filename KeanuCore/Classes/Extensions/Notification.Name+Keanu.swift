//
//  Notification.Name+Keanu.swift
//  KeanuCore
//
//  Created by Benjamin Erhart on 07.04.22.
//

import Foundation

extension Notification.Name {

    /**
     A notification that is sent when the user has either accepted or rejected permissions for push.
     The "userInfo" array will contain a boolean value for the key "granted", either true or false.
     */
    public static let didGetPushManagerAuthorizationResult = Notification.Name("didGetPushManagerAuthorizationResult")

    /**
     A notification that is sent when the friends list managed by the `FriendsManager` changed.
     */
    public static let friendsManagerChanged = Notification.Name("friendsManagerChanged")

    /**
     Post this notification to trigger the display of a "Syncing…" overlay in the `MainViewController` (the `UITabBarViewController`).

     If the `MatrixSDK` will not automatically post a `.mxSessionDidSync` notification at the end of the
     operation you triggered, you will need to post that or `.syncingStopped` yourself to make the overlay go away again!
     */
    public static let syncingStarts = Notification.Name("syncingStarts")

    /**
     Post this notification to hide the "Syncing…" overlay in the `MainViewController` (the `UITabBarViewController`).
     */
    public static let syncingStopped = Notification.Name("syncingStopped")
}

