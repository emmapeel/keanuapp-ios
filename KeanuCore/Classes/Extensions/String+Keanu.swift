//
//  String+Keanu.swift
//  KeanuCore
//
//  Created by Benjamin Erhart on 28.05.20.
//

import Foundation

/**
 For a Material Icons reference see: https://fonts.google.com/icons
 */
extension String {

    /**
     Material icon.
     */
    public static var account_circle = "\u{e853}"

    /**
     Material icon.
     */
    public static var add_reaction = "\u{e1d3}"

    /**
     Material icon.
     */
    public static var close = "\u{e5cd}"

    /**
     Material icon.
     */
    public static var content_copy = "\u{e14d}"

    /**
     Material icon.
     */
    public static var delete = "\u{e872}"

    /**
     Material icon.
     */
    public static var devices = "\u{e1b1}"

    /**
     Material icon.
     */
    public static var exit_to_app = "\u{e879}"

    /**
     Material icon.
     */
    public static var flash_on = "\u{e3e7}"

    /**
     Material icon.
     */
    public static var flash_off = "\u{e3e6}"

    /**
     Material icon.
     */
    public static var forward = "\u{e154}"

    /**
     Material icon.
     */
    public static var group_add = "\u{e7f0}"

    /**
     Material icon.
     */
    public static var ios_share = "\u{e6b8}"

    /**
     Material icon.
     */
    public static var language = "\u{e894}"

    /**
     Material icon.
     */
    public static var lock = "\u{e897}"

    /**
     Material icon.
     */
    public static var notifications_active = "\u{e7f7}"

    /**
     Material icon.
     */
    public static var person_add = "\u{e7fe}"

    /**
     Material icon.
     */
    public static var photo = "\u{e410}"

    /**
     Material icon.
     */
    public static var play_arrow = "\u{e037}"

    /**
     Material icon.
     */
    public static var playlist_play = "\u{e05f}"

    /**
     Material icon.
     */
    public static var reply = "\u{e15e}"

    /**
     Material icon.
     */
    public static var send = "\u{e163}"

    /**
     Material icon.
     */
    public static var storage = "\u{e1db}"

    /**
     Material icon.
     */
    public static var switch_camera = "\u{e41e}"

    /**
     Material icon.
     */
    public static var verified_user = "\u{e8e8}"

    /**
     Material icon.
     */
    public static var warning = "\u{e002}"

    /**
     Material icon.
     */
    public static var expand_more = "\u{e5cf}"

    /**
     Splits string in even pieces of given length.

     Taken from [Stack Overflow](https://stackoverflow.com/questions/32212220/how-to-split-a-string-into-substrings-of-equal-length)

     - parameter length: The length per piece.
     - returns: An array of subsequences, split from this collection’s elements.
    */
    public func split(by length: Int) -> [Substring] {
        var startIndex = self.startIndex
        var results = [Substring]()

        while startIndex < self.endIndex {
            let endIndex = self.index(startIndex, offsetBy: length, limitedBy: self.endIndex) ?? self.endIndex
            results.append(self[startIndex..<endIndex])
            startIndex = endIndex
        }

        return results
    }

    /**
     Generate a random alphanumeric string of the given length.

     Taken from [Stack Overflow](https://stackoverflow.com/questions/26845307/generate-random-alphanumeric-string-in-swift)
     */
    public static func randomAlphanumericString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }

    public static func randomNumericString(length: Int) -> String {
        let letters = "0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }

    subscript(bounds: CountableClosedRange<Int>) -> Substring {
        return self[index(startIndex, offsetBy: bounds.lowerBound)...index(startIndex, offsetBy: bounds.upperBound)]
    }

    subscript(bounds: CountableRange<Int>) -> Substring {
        return self[index(startIndex, offsetBy: bounds.lowerBound)..<index(startIndex, offsetBy: bounds.upperBound)]
    }
}
