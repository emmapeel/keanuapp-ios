//
//  UIColor+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 12.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

/**
 For color tables, see: https://noahgilmore.com/blog/dark-mode-uicolor-compatibility/
 */
extension UIColor {

    public class var keanuLabel: UIColor {
        if #available(iOS 13.0, *) {
            return .label
        }

        return .black
    }

    public class var keanuBackground: UIColor {
        if #available(iOS 13.0, *) {
            return .systemBackground
        }

        return .white
    }

    public class var keanuGray: UIColor {
        if #available(iOS 13.0, *) {
            return .systemGray
        }

        return UIColor(red: 142, green: 142, blue: 147, alpha: 1)
    }

    public class var keanuGray2: UIColor {
        if #available(iOS 13.0, *) {
            return .systemGray2
        }

        return UIColor(red: 174, green: 174, blue: 178, alpha: 1)
    }

    public class var keanuGray3: UIColor {
        if #available(iOS 13.0, *) {
            return .systemGray3
        }

        return UIColor(red: 199, green: 199, blue: 204, alpha: 1)
    }

    public class var keanuGray4: UIColor {
        if #available(iOS 13.0, *) {
            return .systemGray4
        }

        return UIColor(red: 209, green: 209, blue: 214, alpha: 1)
    }

    public class var keanuGray5: UIColor {
        if #available(iOS 13.0, *) {
            return .systemGray5
        }

        return UIColor(red: 229, green: 229, blue: 234, alpha: 1)
    }

    public class var keanuGray6: UIColor {
        if #available(iOS 13.0, *) {
            return .systemGray6
        }

        return UIColor(red: 242, green: 242, blue: 247, alpha: 1)
    }

    /**
     Create a UIColor object from a color hex string in the format `#aarrggbb`.

     ATTENTION: The first character will be ignored, so provide a leading "#"!

     - parameter hexString: The hexadecimal color code.
     */
    public convenience init(hexString: String) {
        let scanner = Scanner(string: hexString)
        scanner.currentIndex = scanner.string.index(after: scanner.currentIndex)

        var rgb: UInt64 = 0

        guard scanner.scanHexInt64(&rgb) else {
            self.init(white: 1, alpha: 1)
            return
        }

        let a = CGFloat((rgb & 0xFF000000) >> 24) / 255
        let r = CGFloat((rgb & 0x00FF0000) >> 16) / 255
        let g = CGFloat((rgb & 0x0000FF00) >> 8) / 255
        let b = CGFloat(rgb & 0x000000FF) / 255

        self.init(red: r, green: g, blue: b, alpha: a)
    }

    /**
     Creates an image filled with this color.

     - parameter size: The size of the created image.
     - returns: A `UIImage` filled with this color.
    */
    public func asImage(size: CGSize = CGSize(width: 1, height: 1)) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)

        setFill()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))

        let image = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        return image
    }
}
