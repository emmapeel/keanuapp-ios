//
//  Collection+Keanu.swift
//  KeanuCore
//
//  Created by Benjamin Erhart on 10.03.22.
//

import Foundation

public extension Collection {

    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
