//
//  Formatters.swift
//  Keanu
//
//  Created by Benjamin Erhart on 10.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.

import Foundation

open class Formatters {

    /**
     Shared date formatter.
     */
    public static let date: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .full

        return formatter
    }()

    static let friendlyTimestamp: DateFormatter = {
        let formatter = DateFormatter()
        formatter.doesRelativeDateFormatting = true
        formatter.dateStyle = .medium
        formatter.timeStyle = .short

        return formatter
    }()

    static let friendlyTimestampDateOnly: DateFormatter = {
        let formatter = DateFormatter()
        formatter.doesRelativeDateFormatting = true
        formatter.dateStyle = .medium
        formatter.timeStyle = .none

        return formatter
    }()

    /**
     Formatter for `Friend` and `MXUser`'s `friendlyLastActiveAgo` representation.
     */
    public static let lastActiveAgo: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()

        formatter.unitsStyle = .full
        formatter.allowedUnits = [.year, .month, .day, .hour, .minute, .second]
        formatter.maximumUnitCount = 2

        return formatter
    }()

    /**
     Formatter to display A/V duration.
    */
    public static let duration: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()

        formatter.unitsStyle = .positional // Use the appropriate positioning for the current locale
        formatter.allowedUnits = [ .hour, .minute, .second ] // Units to display in the formatted string
        formatter.zeroFormattingBehavior = [ .dropLeading ] // Pad with zeroes where appropriate for the locale

        return formatter
    }()

    public static let int: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.allowsFloats = false
        formatter.maximumFractionDigits = 0
        formatter.minimumFractionDigits = 0

        return formatter
    }()

    /**
     Format a `Date`.

     - parameter date: The `Date` to format.
     - returns: A localized formatted string representation of `date`.
     */
    open class func format(date: Date) -> String {
        return Self.date.string(from: date)
    }

    /**
     Format a `TimeInterval` appropriately for A/V player display.

     - parameter duration: The `TimeInterval` to format.
     - returns: A localized formatted string representation of `duration`.
    */
    open class func format(duration: TimeInterval) -> String? {
        return Self.duration.string(from: duration)
    }

    /**
     Format a `TimeInterval` appropriately to show, when something was last active.

     - parameter lastActive: The `TimeInterval`  to format.
     - returns: A localized formatted string representation of `lastActive`.
     */
    open class func format(lastActive: TimeInterval) -> String? {
        return Self.lastActiveAgo.string(from: lastActive)
    }


    /**
     Format a `Date` in a human-friendly way, like "today at", "yesterday" etc.

     Doesn't show time on dates before today.

     - parameter date: The `Date`  to format.
     - returns: A localized formatted string representation of `date`.
     */
    open class func format(friendlyTimestamp date: Date) -> String? {
        if Calendar.current.isDateInToday(date) {
            return Self.friendlyTimestamp.string(from: date)
        }

        return Self.friendlyTimestampDateOnly.string(from: date)
    }

    /**
     Format an `Int`.

     If the formatter returned `nil` will fall back to a formatted version of
     the integer 0.
     If that fails, will fall back to the string "0".

     - parameter int: The `Int` to format.
     - returns: A localized formatted string representation of `int`.
     */
    open class func format(int: Int) -> String {
        return Self.int.string(from: NSNumber(value: int))
            ?? Self.int.string(from: NSNumber(value: 0))
            ?? "0"
    }
}
