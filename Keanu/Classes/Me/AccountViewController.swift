//
//  AccountViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 14.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore
import MobileCoreServices
import Eureka

open class AccountViewController: ProfileViewController {

    open weak var account: MXKAccount?

    open var showNameRow = true
    open var showLinkedIds = true
    open var showShareProfile = true
    open var showCloseAccount = true
    open var showVersion = true
    
    open override func createForm() {
        avatarRow.view?.load(account: account)

        nameRow.value = account?.friendlyName
        nameRow.placeholder = "Your Public Name".localize()

        if showAvatarRow {
        form
            +++ avatarRow.onCellSelection { [weak self] _, _ in
                guard let self = self, let account = self.account else {
                    return
                }

                AvatarPicker.showPickerFor(account: account, avatarView: self.avatarRow.view,
                                           inViewController: self)
            }
        }

        if showNameRow {
            form.allSections.last! <<< nameRow.cellUpdate { [weak self] cell, row in
                row.value = self?.account?.friendlyName
                cell.textField?.font = .boldSystemFont(ofSize: 25)
            }
            .onChange { [weak self] row in
                if let self = self, let value = row.value {
                    self.account?.setUserDisplayName(value, success: {}) { error in
                        row.value = self.account?.friendlyName
                        AlertHelper.present(self, message: error.localizedDescription)
                    }

                    self.friend?.name = value
                }
            }
        }
        
        let idRow = LabelRow() {
            $0.title = account?.matrixId
        }
        if let lastSection = form.allSections.last {
            lastSection <<< idRow
        } else {
            form +++ idRow
        }

        if showLinkedIds {
            form +++ MultivaluedSection(header: "Other linked IDs".localize()) {
                $0.tag = "3pids"

                // TODO: Currently unused.
                $0.addButtonProvider = { section in
                    return ButtonRow("add_row") {
                        $0.title = "Link another ID".localize()
                    }
                    .cellUpdate { cell, _ in
                        cell.textLabel?.textAlignment = .natural
                    }
                }

                $0.multivaluedRowToInsertAt = { _ in LabelRow() }
            }
        }

            form +++ ButtonRow() {
                $0.title = "Change Password".localize()
            }
            .cellUpdate { cell, _ in
                cell.textLabel?.textAlignment = .natural
            }
            .onCellSelection { [weak self] _, _ in
                self?.changePassword()
            }
        
        if showShareProfile {
            form.allSections.last! <<< shareRow.onCellSelection { [weak self] cell, _ in
                if let user = self?.account?.mxSession?.myUser {
                    self?.present(ProfileViewController.shareProfiles([user], cell), animated: true)
                }
            }
        }
        
        form +++ Section("My sessions".localize())
            <<< devicesRow.onCellSelection { [weak self] _, _ in
                let vc = MyDevicesViewController()
                vc.session = self?.account?.mxSession

                self?.navigationController?.pushViewController(vc, animated: true)
            }

        +++ ButtonRow() {
            $0.title = "Fix Push Notifications".localize()
        }
        .cellUpdate { cell, _ in
            cell.textLabel?.textAlignment = .natural
        }
        .onCellSelection { [weak self] _, _ in
            self?.fixPush()
        }

        +++ ButtonRow() {
            $0.title = "Log Out".localize()
        }
        .cellUpdate { cell, _ in
            cell.textLabel?.textAlignment = .natural
        }
        .onCellSelection { [weak self] _, _ in
            self?.logout()
        }

        if showCloseAccount, let section = form.allSections.last {
            section <<< ButtonRow() {
                $0.title = "Close Account".localize()
            }
            .cellUpdate { cell, _ in
                cell.textLabel?.textAlignment = .natural
            }
            .onCellSelection { [weak self] _, _ in
                self?.closeAccount()
            }
        }
        
        if showVersion {
            form.allSections.last! <<< LabelRow() {
                $0.title = "Version: %, Build: %".localize(values: Bundle.main.version, Bundle.main.build)
            }
            .cellUpdate { cell, _ in
                cell.textLabel?.font = .italicSystemFont(ofSize: UIFont.smallSystemFontSize)
            }
        }
        
        // Override onCellSelection of MultivaluedSection's add row here, because
        // otherwise it's too early.
        (form.rowBy(tag: "add_row") as? ButtonRow)?.onCellSelection { [weak self] cell, row in
            let vc = Link3pidViewController()
            vc.account = self?.account

            self?.navigationController?.pushViewController(vc, animated: true)
        }

        update3pids()
    }

    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        reload3pids()
    }


    // MARK: UITableViewDelegate

    open override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete,
            let cell = tableView.cellForRow(at: indexPath) as? LabelCell,
            let address = cell.row.value,
           let threePid = account?.threePIDs?.first(where: { $0.address == address }) {

            let delete = AlertHelper.destructiveAction("Delete".localize()) { _ in
                self.account?.mxRestClient?.remove3PID(address: threePid.address, medium: threePid.medium) { response in
                    if let error = response.error {
                        AlertHelper.present(self, message: error.localizedDescription)
                    }

                    self.reload3pids()
                }
            }

            let cancel = AlertHelper.cancelAction() { _ in
                self.reload3pids()
            }

            AlertHelper.present(
                self, message: "Are you sure you want to delete % from your account?".localize(value: threePid.address),
                title: "Delete linked ID".localize(), actions: [delete, cancel])
        }

        super.tableView(tableView, commit: editingStyle, forRowAt: indexPath)
    }


    // MARK: Public Methods

    open func changePassword() {
        present(PasswordViewController.instantiate(
            style: .newPassword,
            title: "Change Password".localize(),
            prefilledPassword: AuthenticationManager.storedCredentials(for: account?.matrixId)?.password,
            okLabel: "Change Password".localize()) { [weak self] vc in
                guard let oldPassword = vc.oldPassword,
                    let newPassword = vc.password else {
                        return
                }

                self?.account?.changePassword(
                    oldPassword, with: newPassword, logoutDevices: false,
                    success: {
                        guard let self = self else {
                            return
                        }
                        self.didChangePassword()

                        AlertHelper.present(
                            self,
                            message: "Password successfully changed!".localize(),
                            title: "Success".localize())
                    }) { error in
                        guard let self = self else {
                            return
                        }
                        AlertHelper.present(self, message: error.localizedDescription)
                    }
            }, animated: true)
    }

    open func didChangePassword() {
        guard let matrixId = account?.matrixId else {
            return
        }

        try? KeanuKeychain.delete(AuthenticationManager.credentials(from: matrixId))
    }
    
    open func fixPush() {
        PushManager.shared.clearPushToken()

        PushManager.shared.isPushAllowed { allowed, asked in
            EnablePushViewController.enablePush(self)

            // If we already asked, there will be no dialogue at all. Show this
            // alert, so the user knows, that something happened.
            if (allowed && asked) {
                AlertHelper.present(self, message: "Push Notification settings successfully refreshed.".localize(), title: "Push Notifications".localize())
            }
        }
    }

    open func logout() {
        if let account = account {
            let actions = [
                AlertHelper.cancelAction(),
                AlertHelper.destructiveAction("Log Out".localize()) { action in
                    MXKAccountManager.shared().removeAccount(account, completion: {
                        // If no more accounts, start onboarding
                        if MXKAccountManager.shared().accounts.isEmpty {
                            DispatchQueue.main.async {
                                UIApplication.shared.popToChatListViewController()
                                UIApplication.shared.startOnboardingFlow()
                            }
                        }
                    })
                }
            ]

            AlertHelper.present(
                self, message:
                "Are you sure you want to log out of the account \"%\"?\n".localize(value: account.friendlyName),
                title: "Log Out".localize(),
                actions: actions)
        }
    }

    open func closeAccount() {
        present(PasswordViewController.instantiate(
            title: "Close Account".localize(),
            message: "Close your account on the server.".localize()
            + "\n\n"
            + "ATTENTION: This is non-reversible! You will never again be able to log in, re-register or reactivate that account!".localize()
            + "\n\n"
            + "The account will also be removed from this device!".localize(),
            prefilledPassword: AuthenticationManager.storedCredentials(for: account?.matrixId)?.password,
            okLabel: "Close Account Forever".localize()) { [weak self] vc in
                guard let password = vc.password,
                    let account = self?.account else {
                    return
                }

                let auth: [String: Any] = [
                    "type": kMXLoginFlowTypePassword,

                    // This is how it needs to be.
                    "user": account.matrixId,

                    // This is, how it *should* be according to the "User-interactive API" documentation.
                    // Since it doesn't hurt to have this here, I'll leave it for forward
                    // compatibility. (Hope dies last...)
                    "identifier": [
                        "type": kMXLoginIdentifierTypeUser,
                        "user": account.matrixId,
                    ],

                    "password": password,
                ]

                account.mxSession?.deactivateAccount(withAuthParameters: auth, eraseAccount: true) { response in
                    if let error = response.error, let self = self {
                        return AlertHelper.present(self, message: error.localizedDescription)
                    }

                    MXKAccountManager.shared().removeAccount(account, sendLogoutRequest: false) {
                        self?.navigationController?.popViewController(animated: true)
                        if MXKAccountManager.shared().accounts.isEmpty {
                            DispatchQueue.main.async {
                                UIApplication.shared.popToChatListViewController()
                                UIApplication.shared.startOnboardingFlow()
                            }
                        }
                    }
                }
        }, animated: true)
    }


    // MARK: Private Methods

    private func reload3pids() {
        account?.load3PIDs(update3pids) { _ in self.update3pids() }
    }

    private func update3pids() {
        guard var section = form.sectionBy(tag: "3pids") as? MultivaluedSection else {
            return
        }

        section.removeFirst(max(0, section.count - 1))

        var i = 0

        for threePid in self.account?.threePIDs ?? [] {
            if let row = section.multivaluedRowToInsertAt?(i) {

                row.title = AccountViewController.label(for3PidMedium: threePid.medium)
                row.baseValue = threePid.address

                section.insert(row, at: i)

                i += 1
            }
        }
    }

    /**
     Unified place to create friendly labels for `kMX3PIDMedium` IDs.

     - parameter threePidMedium: Currently supports `kMX3PIDMediumEmail` and `kMX3PIDMediumMSISDN`.
     - returns: a user-friendly label for `kMX3PIDMedium` IDs.
    */
    class func label(for3PidMedium threePidMedium: String?) -> String {
        switch threePidMedium ?? "" {
        case kMX3PIDMediumEmail:
            return "E-Mail".localize()

        case kMX3PIDMediumMSISDN:
            return "Mobile Phone".localize()

        default:
            return "Other".localize()
        }
    }
}
