//
//  PreferencesViewController.swift
//  Keanu
//
//  Created by N-Pex on 15.6.22.
//

import UIKit
import MatrixSDK
import KeanuCore
import Eureka
import ViewRow
import Localize

open class PreferencesViewController: FormViewController {

    open lazy var languages: [String: String] = {
        Bundle.main.localizations
            .filter({ $0 != "Base" })
            .reduce(into: [String: String](), {
                $0[Locale.current.localizedString(forIdentifier: $1) ?? $1] = $1
            })
    }()

    public let languageRow = ActionSheetRow<String>() {
        $0.title = "Language".localize()
        $0.selectorTitle = "Language".localize()

        if let code = UserDefaults.standard.string(forKey: "i18n_language") {
            $0.value = Locale.current.localizedString(forIdentifier: code) ?? code
        }
    }

    public init() {
        super.init(style: .grouped)
    }
    
    public override init(style: UITableView.Style) {
        super.init(style: style)
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }


    open override func viewDidLoad() {
        super.viewDidLoad()
        createForm()
    }


    // MARK: Public Methods

    open func createForm() {
        form +++
        languageRow
        .cellUpdate { cell, row in
            row.options = Array(self.languages.keys).sorted()

            cell.textLabel?.textAlignment = .natural
        }
        .onChange({ [weak self] row in
            guard let lang = row.value,
                  let code = self?.languages[lang]
            else {
                return
            }

            UserDefaults.standard.set([code, "en"], forKey: "AppleLanguages")
            UserDefaults.standard.set(code, forKey: "i18n_language")

            Localize.update(language: code)
            Localize.update(defaultLanguage: code)

            UIApplication.shared.startMainFlow()
        })
    }
}
