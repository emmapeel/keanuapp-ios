//
//  MyProfileViewController.swift
//  Keanu
//
//  Created by N-Pex on 10.6.22.
//
//

import KeanuCore
import MatrixSDK
import CoreLocation

public protocol MyProfileViewControllerDelegate: AnyObject {
    func viewOrEditStatus()
}

open class MyProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var avatarView: AvatarView!
    @IBOutlet weak var labelDisplayName: UILabel!
    @IBOutlet weak var labelPresence: UILabel!
    @IBOutlet weak var settingsTableView: UITableView!
    
    weak var delegate : MyProfileViewControllerDelegate?
    private weak var session: MXSession?
    private var sessionStateObserver: NSObjectProtocol?
    
    // MARK: Properties
    var account: MXKAccount? {
        didSet {
            updateViews()
        }
    }
        
    open override func viewDidLoad() {
        super.viewDidLoad()
        settingsTableView.register(ProfileViewControllerCell.self, forCellReuseIdentifier: ProfileViewControllerCell.defaultReuseId)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.delegate = self
        updateViews()
        sessionStateObserver = NotificationCenter.default.addObserver(
            forName: .mxSessionStateDidChange, object: nil,
            queue: OperationQueue.main, using: { notification in
                self.updateViews()
            })
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let sessionStateObserver = sessionStateObserver {
            NotificationCenter.default.removeObserver(sessionStateObserver)
        }
        sessionStateObserver = nil
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.delegate = nil
    }
    
    func updateViews() {
        guard isViewLoaded else { return }
        if let myAccount = self.account {
            avatarView.load(account: myAccount)
            labelDisplayName.text = myAccount.userDisplayName
            session = myAccount.mxSession
        }
        labelPresence.text = ((session?.state ?? .closed)   == .running) ? "Available".localize() : "Offline".localize()
        settingsTableView.reloadData()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: ProfileViewControllerCell.defaultReuseId, for: indexPath)
        if let cell = cell as? ProfileViewControllerCell {
        switch indexPath.row {
        
        case 3:
            cell.apply(UIImage(named: "ic_about", in: Bundle(for: MyProfileViewController.self), compatibleWith: nil), "About".localize())
            
        case 2:
            cell.apply(UIImage(named: "ic_account", in: Bundle(for: MyProfileViewController.self), compatibleWith: nil), "Account".localize())

        case 1:
            cell.apply(UIImage(named: "ic_settings", in: Bundle(for: MyProfileViewController.self), compatibleWith: nil), "Preferences".localize())
            cell.accessoryType = .disclosureIndicator
            
        default:
            var notificationsEnabled = true
            if let rule = self.session?.notificationCenter?.rule(byId: ".m.rule.master") {
                notificationsEnabled = !rule.enabled // Note: reverse logic, the rule is a "dontnotify" rule.
            }
            cell.apply(UIImage(named: "ic_notifications", in: Bundle(for: MyProfileViewController.self), compatibleWith: nil), "Notifications".localize(),  notificationsEnabled ? "On".localize() : "Off".localize())
        }
        }
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            showNotificationSettings()
        } else if indexPath.row == 1 {
            showPreferences()
        } else if indexPath.row == 2 {
            showMe()
        } else if indexPath.row == 3 {
            showAbout()
        }
    }

    @IBAction func editProfile(_ sender: Any) {
        let vc = UIApplication.shared.router.profileEdit()
        vc.account = self.account
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func showNotificationSettings() {
        let vc = NotificationsViewController(style: .grouped)
        vc.session = self.session
        navigationController?.pushViewController(vc, animated: true)
    }

    func showPreferences() {
        let vc = UIApplication.shared.router.profilePreferences()
        navigationController?.pushViewController(vc, animated: true)
    }

    func showMe() {
        let meVC = UIApplication.shared.router.profileAccount()
        meVC.showAvatarRow = false
        meVC.showNameRow = false
        meVC.showLinkedIds = false
        meVC.showShareProfile = false
        meVC.showVersion = false
        meVC.account = self.account
        navigationController?.pushViewController(meVC, animated: true)
    }
    
    func showAbout() {
        let vc = UIApplication.shared.router.profileAbout()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func showQr(_ sender: Any) {
        let vc = UIApplication.shared.router.showQr()
        vc.qrCode = ""
        if let userId = self.session?.myUserId ?? self.account?.matrixId {
            vc.qrCode = String(format: Config.inviteLinkFormat, userId)
        }
        let navC = UINavigationController(rootViewController: vc)
        navC.modalPresentationStyle = .formSheet
        navC.popoverPresentationController?.sourceView = self.view
        navC.popoverPresentationController?.sourceRect = self.view.bounds
        navigationController?.present(navC, animated: true)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        super.prepare(for: segue, sender: sender)
//        if let editProfile = segue.destination as? CirculoEditProfileViewController {
//            if let me = membersDataSource?.me {
//                editProfile.member = me
//            } else {
//                editProfile.account = MXKAccountManager.shared().activeAccounts?.first
//            }
//
//            editProfile.delegate = self
//        }
    }
    
    open func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController is EditProfileViewController {
            self.navigationItem.backButtonTitle = "Profile".localize()
        } else if viewController is NotificationsViewController {
            self.navigationItem.backButtonTitle = "Notifications".localize()
        } else if viewController is PreferencesViewController {
            self.navigationItem.backButtonTitle = "Preferences".localize()
        } else if viewController is AboutViewController {
            self.navigationItem.backButtonTitle = "About".localize()
        } else {
            self.navigationItem.backButtonTitle = nil
        }
    }

}

extension MyProfileViewController: EditProfileViewControllerDelegate {
    public func updateProfile(name: String?, avatar: UIImage?, done: ((Bool) -> Void)?) {
        guard let account = self.account else { return }

        let updateName = {
            if let name = name {
                account.setUserDisplayName(name) {
                    done?(true)
                } failure: { err in
                    done?(false)
                }
            } else {
               done?(true)
            }
        }
        if let avatar = avatar {
            AvatarPicker.useAsAvatar(account: account, image: avatar, avatarView: nil, done: { success in
                if !success {
                    done?(false)
                } else {
                    updateName()
                }
            })
        } else {
            updateName()
        }
    }
}
