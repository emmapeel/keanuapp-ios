//
//  AvatarPicker.swift
//  Keanu
//
//  Created by N-Pex on 17.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import KeanuCore
import Photos
import MobileCoreServices

/**
 A class that handles picking an image from the photo library and setting it as an avatar for the given account.
 */
open class AvatarPicker: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    /**
     Singleton instance. Will load the friends list from disk on first access.
     */
    public static let shared: AvatarPicker = {
        return AvatarPicker()
    }()
    
    weak var account: MXKAccount?
    weak var avatar: AvatarView?
    var viewController: UIViewController?
    var callback: ((UIImage?) -> Void)?
    
    private lazy var imagePicker: UIImagePickerController = {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [UtiHelper.typeImage]
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .popover
        
        return imagePicker
    }()
    
    /**
     Display an image picker for the account in the view controller given. Also, when done, update the given AvatarView (if any) with the new image.
     */
    open class func showPickerFor(account: MXKAccount, avatarView: AvatarView?, inViewController viewController: UIViewController) {
        shared.account = account
        shared.avatar = avatarView
        shared.viewController = viewController
        shared.callback = nil
        shared.showPicker()
    }
    
    /**
     Display an image picker in the view controller given. Call the given callback with chosen image on success. Also, when done, update the given AvatarView (if any) with the new image.
     */
    open class func showPickerFor(avatarView: AvatarView?, inViewController viewController: UIViewController, callback:@escaping (UIImage?) -> Void) {
        shared.account = nil
        shared.avatar = avatarView
        shared.viewController = viewController
        shared.callback = callback
        shared.showPicker()
    }
    
    open class func useAsAvatar(account:MXKAccount, image:UIImage, avatarView:AvatarView?, done:((Bool) -> Void)?) {
        shared.useImageAsAvatar(account: account, image: image, avatarView: avatarView, done: done)
    }
    
    private func showPicker() {
        guard let avatar = avatar, let vc = viewController else { return }
        imagePicker.popoverPresentationController?.sourceView = avatar
        imagePicker.popoverPresentationController?.sourceRect = avatar.bounds
        
        var status: PHAuthorizationStatus
        if #available(iOS 14, *) {
            status = PHPhotoLibrary.authorizationStatus(for: .readWrite)
        } else {
            status = PHPhotoLibrary.authorizationStatus()
        }
        
        switch status {
        case .authorized:
            vc.present(imagePicker, animated: true)
            
        case .notDetermined:
            let authHandler:((PHAuthorizationStatus) -> Void) = { newStatus in
                if newStatus == .authorized {
                    DispatchQueue.main.async {
                        vc.present(self.imagePicker, animated: true)
                    }
                }
            }
            if #available(iOS 14, *) {
                PHPhotoLibrary.requestAuthorization(for: .readWrite, handler: authHandler)
            } else {
                PHPhotoLibrary.requestAuthorization(authHandler)
            }
            
        case .limited:
            vc.present(imagePicker, animated: true)
            
        case .restricted:
            AlertHelper.present(
                vc,
                message: "Sorry, you are not allowed to view the photo library.".localize(),
                title: "Access Restricted".localize())
            
        case .denied:
            AlertHelper.present(
                vc, message:
                    "Please go to the Settings app to grant this app access to your photo library, if you want to upload photos or videos."
                    .localize(),
                title: "Access Denied".localize(),
                actions: [
                    AlertHelper.cancelAction(),
                    AlertHelper.defaultAction("Settings".localize(), handler: { _ in
                        if let url = URL(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.open(url)
                        }
                    })
                ])
            
        @unknown default:
            break
        }
    }
    
    // MARK: UIImagePickerControllerDelegate
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo
                                      info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = (info[.editedImage] ?? info[.originalImage]) as? UIImage {
            let image = MXKTools.forceImageOrientationUp(image)
            
            if let image = image,
               let account = account
            {
                useImageAsAvatar(account: account, image: image, avatarView: self.avatar)
            }
            else {
                callback?(image)
                self.avatar?.image = image
            }
        }
        
        picker.dismiss(animated: true)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    private func useImageAsAvatar(account:MXKAccount, image:UIImage, avatarView:AvatarView?, done:((Bool) -> Void)? = nil) {
        // Upload picture
        let uploader = MXMediaManager.prepareUploader(
            withMatrixSession: account.mxSession, initialRange: 0, andRange: 1)
        
        uploader?.uploadData(
            (MXKTools.reduce(image, toFitIn: CGSize(width: 120, height: 120)) ?? image).jpegData(compressionQuality: 0.5),
            filename: nil, mimeType: UtiHelper.mimeJpeg,
            success: { url in
                guard let url = url else {
                    done?(false)

                    return
                }
                
                // Set as avatar.
                account.setUserAvatarUrl(url, success: {
                    MXKAccountManager.shared().saveAccounts()
                    self.avatar?.load(account: account)
                    done?(true)
                }, failure: { error in
                    if let vc = self.viewController {
                        AlertHelper.present(vc, message: error.localizedDescription)
                    }
                    done?(false)
                })
            },
            failure: { error in
                if let vc = self.viewController {
                    AlertHelper.present(vc, message: error?.localizedDescription)
                }
                done?(false)
            })
    }
}
