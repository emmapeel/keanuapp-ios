//
//  AboutViewController.swift
//  Keanu
//
//  Created by N-Pex on 15.6.22.
//

import UIKit
import MatrixSDK
import KeanuCore
import Eureka
import ViewRow

open class AboutViewController: FormViewController {
    open override func viewDidLoad() {
        super.viewDidLoad()
        createForm()
    }
    
    public init() {
        super.init(style: .grouped)
    }
    
    public override init(style: UITableView.Style) {
        super.init(style: style)
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: Public Methods

    open func createForm() {
        form +++ LabelRow() {
                $0.title = "Version: %, Build: %".localize(values: Bundle.main.version, Bundle.main.build)
            }
            .cellUpdate { cell, _ in
                cell.textLabel?.font = .italicSystemFont(ofSize: UIFont.smallSystemFontSize)
            }
    }
}
