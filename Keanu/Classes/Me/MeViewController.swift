//
//  MeViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 22.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore

open class MeViewController: UITableViewController {

    open var accountManager: MXKAccountManager?

    override open func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Me".localize()
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .add, target: self, action: #selector(addAccount))

        accountManager = MXKAccountManager.shared()

        tableView.register(AvatarAnd3LabelsCell.nib, forCellReuseIdentifier: AvatarAnd3LabelsCell.defaultReuseId)

        let nc = NotificationCenter.default

        nc.addObserver(self, selector: #selector(refresh),
                       name: .mxkAccountManagerDidAddAccount, object: nil)

        nc.addObserver(self, selector: #selector(refresh),
                       name: .mxkAccountManagerDidRemoveAccount, object: nil)

        nc.addObserver(self, selector: #selector(refresh),
                       name: .mxSessionStateDidChange, object: nil)
        
        // If only one account (and it is active) show the account profile view controller by default.
        // In that case an "edit" button is added to show the list (to get to the add/remove operations)
        if accountManager?.accounts.count == 1,
           let account = accountManager?.accounts.first,
           !account.isDisabled {

            navigationController?.pushViewController(createProfileViewController(account), animated: false)
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accountManager?.accounts.count ?? 0
    }

    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let account = accountManager?.accounts[indexPath.row],
           let cell = tableView.dequeueReusableCell(withIdentifier:
                                                        AvatarAnd3LabelsCell.defaultReuseId, for: indexPath) as? AvatarAnd3LabelsCell {

            return cell.apply(account)
        }

        return UITableViewCell()
    }

    
    // MARK: UITableViewDelegate

    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }

    override open func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let actions = accountManager?.accounts[indexPath.row].isDisabled ?? true
            ? [enableAction(indexPath), logoutAction(indexPath)]
            : [disableAction(indexPath), logoutAction(indexPath), editAction(indexPath)]

        return UISwipeActionsConfiguration(actions: actions)
    }

    override open func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return accountManager?.accounts[indexPath.row].isDisabled ?? true
        ? nil : indexPath
    }

    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let account = accountManager?.accounts[indexPath.row] {
            pushProfile(account)
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }


    // MARK: Actions

    @IBAction func addAccount() {
        let vc = UIApplication.shared.router.addAccount()
        vc.calledInline = true

        present(UINavigationController(rootViewController: vc), animated: true)
    }

    @objc func popMyProfileVc() {
        navigationController?.popViewController(animated: true)
    }


    // MARK: Notifications

    @objc func refresh() {
        // Give the UITableView#setEditing animation some time before reloading.
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.tableView.reloadData()
        }
    }


    // MARK: Private Methods
    
    /**
     Performs segue to the `ProfileViewController`.
     */
    private func pushProfile(_ account: MXKAccount) {
        navigationController?.pushViewController(createProfileViewController(account), animated: true)
    }

    /**
     Closes any edit buttons on rows, animated.
     */
    private func endEditing() {
        DispatchQueue.main.async {
            self.tableView.setEditing(false, animated: true)
        }
    }

    private func enableBackup(_ session: MXSession?, _ completion: (() -> Void)? = nil) {

        // Race condition. This might happen if the user logs out twice.
        guard let session = session else {
            completion?()
            return
        }

        VerificationManager.shared.checkBackup(session) { [weak self] isBackingUp, error in
            guard let self = self, !isBackingUp else {
                completion?()
                return
            }

            self.present(PasswordViewController.instantiate(
                style: .confirm,
                title: "See your messages?".localize(),
                message: "We'll remember your encryption keys so you can see encrypted messages when you log back in.".localize(),
                cancelLabel: "Skip".localize(), okLabel: "Backup Keys".localize()) { vc in
                    guard let password = vc.password else {
                        completion?()
                        return
                    }

                    VerificationManager.shared.addNewBackup(session, password, vc: self) { error in
                        if let error = error {
                            AlertHelper.present(
                                self, message: error.localizedDescription,
                                actions: [AlertHelper.defaultAction(handler: { _ in
                                    completion?()
                                })])
                        }
                        else {
                            completion?()
                        }
                    }
                }, animated: true)
        }
    }

    /**
     Create a MyProfileViewController that we can push. If there is only one
     account (and it is active) show the VC without a back button and instead
     add an "edit" button to show the whole list (so we can add/delete).
     */
    open func createProfileViewController(_ account: MXKAccount) -> UIViewController {
        let profileVc = UIApplication.shared.router.myProfile()
        profileVc.account = account

        if accountManager?.accounts.count == 1,
           let account = accountManager?.accounts.first,
           !account.isDisabled {

            profileVc.navigationItem.leftBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: .edit, target: self, action: #selector(popMyProfileVc))
        }
        
        return profileVc
    }

    /**
     Logout action for table list row. Logs out of an account after confirmation.
     */
    private func logoutAction(_ indexPath: IndexPath) -> UIContextualAction {
        UIContextualAction(
            style: .destructive,
            title: "Remove".localize())
        { [weak self] (_, _, completionHandler) in

            guard let self = self,
                  let account = self.accountManager?.accounts[indexPath.row]
            else {
                return completionHandler(false)
            }

            self.enableBackup(account.mxSession) {
                let actions = [
                    AlertHelper.cancelAction() { action in
                        completionHandler(false)
                    },
                    AlertHelper.destructiveAction("Remove".localize()) { action in
                        completionHandler(true)

                        self.accountManager?.removeAccount(account, completion: {
                            // If no more accounts, start onboarding
                            if MXKAccountManager.shared().accounts.isEmpty {
                                DispatchQueue.main.async {
                                    UIApplication.shared.popToChatListViewController()
                                    UIApplication.shared.startOnboardingFlow()
                                }
                            }
                        })
                    }
                ]

                AlertHelper.present(
                    self, message:
                    "Are you sure you want to remove the account \"%\"?\n".localize(value: account.friendlyName),
                    title: "Remove".localize(),
                    actions: actions)
            }
        }
    }

    /**
     Disable action for table list row. Disables an account.
     */
    private func disableAction(_ indexPath: IndexPath) -> UIContextualAction {
        UIContextualAction(
            style: .normal,
            title: "Disable".localize())
        { [weak self] (_, _, completionHandler) in

            guard let self = self,
                  let account = self.accountManager?.accounts[indexPath.row]
            else {
                return completionHandler(false)
            }

            self.enableBackup(account.mxSession) {
                account.isDisabled = true

                completionHandler(true)
            }
        }
    }

    /**
     Enable action for table list row. Enables an account.
     */
    private func enableAction(_ indexPath: IndexPath) -> UIContextualAction {
        UIContextualAction(
            style: .normal,
            title: "Enable".localize())
        { [weak self] (_, _, completionHandler) in

            guard let account = self?.accountManager?.accounts[indexPath.row]
            else {
                return completionHandler(false)
            }

            account.isDisabled = false

            completionHandler(true)
        }
    }

    /**
     Edit action for table list row. User can edit the name of a friend.
     */
    private func editAction(_ indexPath: IndexPath) -> UIContextualAction {
        UIContextualAction(
            style: .normal,
            title: "Edit Name".localize())
        { [weak self] (_, _, completionHandler) in

            guard let self = self,
                  let account = self.accountManager?.accounts[indexPath.row]
            else {
                return completionHandler(false)
            }

            AlertHelper.presentEditNameAlert(self, account.friendlyName) { newName in
                account.setUserDisplayName(newName, success: {
                    completionHandler(true)

                    self.tableView.reloadRows(at: [indexPath], with: .automatic)
                }, failure: { error in
                    completionHandler(false)

                    AlertHelper.present(self, message: error.localizedDescription)
                })
            }
        }
    }
}
