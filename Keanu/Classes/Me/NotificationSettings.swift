//
//  NotificationSettings.swift
//  Keanu
//
//  Created by N-Pex on 27.6.22.
//

import KeanuCore

class NotificationSettings: NSObject {
    
    public static let keanuPushRuleId = "keanu_push_rule.master"
    public static let keanuPushRuleIdEncrypted = "keanu_push_rule.master.encrypted"
    
    enum NotificationCondition : String {
        case Always = "always"
        case Mentions = "mentions"
        case Individual = "individual"
    }
    
    /**
     Singleton instance.
     */
    public static let shared: NotificationSettings = {
        return NotificationSettings()
    }()
    
    private var defaults: UserDefaults?
    
    override init() {
        defaults = UserDefaults(suiteName: Config.appGroupId)
    }
    
    public var notifyCondition: NotificationCondition {
        get {
            if let defaults = defaults {
                if let val = defaults.string(forKey: "notify_condition") {
                    return NotificationCondition(rawValue: val) ?? NotificationCondition.Always
                }
            }
            return NotificationCondition.Always
        }
        set(value) {
            defaults?.set(value.rawValue, forKey: "notify_condition")
        }
    }

    public var soundAll: Bool {
        get {
            if let defaults = defaults {
                return defaults.object(forKey: "notify_sound_all") != nil ? defaults.bool(forKey: "notify_sound_all") : true
            }
            return true
        }
        set(value) {
            defaults?.set(value, forKey: "notify_sound_all")
        }
    }

    public var vibrateAll: Bool {
        get {
            if let defaults = defaults {
                return defaults.object(forKey: "notify_vibrate_all") != nil ? defaults.bool(forKey: "notify_vibrate_all") : false
            }
            return false
        }
        set(value) {
            defaults?.set(value, forKey: "notify_vibrate_all")
        }
    }

    public var ringtoneAll: String? {
        get {
            return defaults?.string(forKey: "notify_ringtone_all")
        }
        set(value) {
            defaults?.set(value, forKey: "notify_ringtone_all")
        }
    }
    
    public var soundMentions: Bool {
        get {
            if let defaults = defaults {
                return defaults.object(forKey: "notify_sound_mentions") != nil ? defaults.bool(forKey: "notify_sound_mentions") : true
            }
            return true
        }
        set(value) {
            defaults?.set(value, forKey: "notify_sound_mentions")
        }
    }
    
    public var vibrateMentions: Bool {
        get {
            if let defaults = defaults {
                return defaults.object(forKey: "notify_vibrate_mentions") != nil ? defaults.bool(forKey: "notify_vibrate_mentions") : true
            }
            return true
        }
        set(value) {
            defaults?.set(value, forKey: "notify_vibrate_mentions")
        }
    }
    
    public var ringtoneMentions: String? {
        get {
            return defaults?.string(forKey: "notify_ringtone_mentions")
        }
        set(value) {
            defaults?.set(value, forKey: "notify_ringtone_mentions")
        }
    }
}
