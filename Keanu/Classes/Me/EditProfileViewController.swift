//
//  EditProfileViewController.swift
//  Keanu
//
//  Created by N-Pex on 14.6.22.
//

import KeanuCore
import UIKit

public protocol EditProfileViewControllerDelegate: AnyObject {
    func updateProfile(name: String?, avatar: UIImage?, done: ((Bool) -> Void)?)
}

open class EditProfileViewController: BaseViewController, UITextFieldDelegate {

    weak var delegate : EditProfileViewControllerDelegate?

    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var avatarView: AvatarView!
    @IBOutlet weak var editName: UITextField!
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonSave: UIBarButtonItem!
    
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    public var account: MXKAccount?
    
    private var pickedAvatarImage: UIImage?
        
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItems = navItem.leftBarButtonItems
        self.navigationItem.rightBarButtonItems = navItem.rightBarButtonItems
        self.navigationItem.title = navItem.title
        self.navigationItem.backButtonTitle = navItem.backButtonTitle
        editName.delegate = self
        if let account = account {
            editName.text = account.userDisplayName
            avatarView.load(account: account)
        }
        updateSaveButton()
    }

    override open func keyboardWillShow(notification: Notification) {
        if let kbSize = getKeyboardSize(notification) {
            self.bottomLayoutConstraint.constant = kbSize.height
            animateDuringKeyboardMovement(notification)
        }
    }
    
    override open func keyboardWillBeHidden(notification: Notification) {
        bottomLayoutConstraint.constant = 0
        animateDuringKeyboardMovement(notification)
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 80
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func didChangeName(_ sender: Any) {
        updateSaveButton()
    }
    
    @IBAction func didTapCamera(_ sender: Any) {
        AvatarPicker.showPickerFor(avatarView: self.avatarView, inViewController: self) { image in
            self.pickedAvatarImage = image
            self.updateSaveButton()
        }
    }
    
    @IBAction func didTapAvatar(_ sender: Any) {
        AvatarPicker.showPickerFor(avatarView: self.avatarView, inViewController: self) { image in
            self.pickedAvatarImage = image
            self.updateSaveButton()
        }
    }

    func updatedName() -> String? {
        if let account = account {
            if let name = editName?.text, name.count > 0, name != account.userDisplayName {
                return name
            }
        }
        return nil
    }
    
    @IBAction func didTapSave() {
        guard let delegate = delegate else { return }
        workingOverlay.isHidden = false
        delegate.updateProfile(name: updatedName(), avatar: pickedAvatarImage, done: { success in
            self.workingOverlay.isHidden = true
            if success {
                self.navigationController?.popViewController(animated: true)
            }
        })
    }
    
    func updateSaveButton() {
        buttonSave.isEnabled = updatedName() != nil || pickedAvatarImage != nil
    }
}
