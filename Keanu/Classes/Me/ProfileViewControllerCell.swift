//
//  ProfileViewControllerCell.swift
//  Keanu
//
//  Created by N-Pex on 28.6.22.
//

import UIKit

/**
 A UITableViewCell with Value1 style, but with a specific font style etc.
 */
open class ProfileViewControllerCell: UITableViewCell {

    init() {
        super.init(style: .value1, reuseIdentifier: ProfileViewControllerCell.defaultReuseId)
        self.textLabel?.font = UIFont(name: "Inter-Regular", size: 17)
        self.accessoryType = .disclosureIndicator
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: ProfileViewControllerCell.defaultReuseId)
        self.textLabel?.font = UIFont(name: "Inter-Regular", size: 17)
        self.accessoryType = .disclosureIndicator
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public class var defaultReuseId: String {
        return String(describing: self)
    }

    /**
     Applies an image icon and an action label to this cell.

     - parameter icon: The icon image to use.
     - parameter action: The text label content.
     - returns: self for convenience.
     */
    @discardableResult
    public func apply(_ icon: UIImage?, _ action: String?, _ detail: String? = nil) -> ProfileViewControllerCell {
        self.imageView?.image = icon?.withRenderingMode(.alwaysTemplate)
        self.textLabel?.text = action
        self.detailTextLabel?.text = detail
        return self
    }
}
