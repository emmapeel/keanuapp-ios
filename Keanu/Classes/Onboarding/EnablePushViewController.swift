//
//  EnablePushViewController.swift
//  Keanu
//
//  Created by N-Pex on 16.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit
import KeanuCore

/**
 Delegate called by the EnablePushViewController when it needs to close.
 */
public protocol EnablePushViewControllerDelegate {
    func done()
}

open class EnablePushViewController: UIViewController {

    public var delegate: EnablePushViewControllerDelegate?
    private var authObserver: NSObjectProtocol?
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.setNavigationBarHidden(true, animated: false)

        authObserver = NotificationCenter.default.addObserver(
            forName: .didGetPushManagerAuthorizationResult, object: nil, queue: .main)
        { [weak self] notification in
            if notification.userInfo?["granted"] as? Bool ?? false {
                self?.close(nil)
            }
        }
    }
    
    override open func viewDidDisappear(_ animated: Bool) {
        if let observer = authObserver {
            NotificationCenter.default.removeObserver(observer)
            authObserver = nil
        }

        navigationController?.setNavigationBarHidden(false, animated: true)

        super.viewDidDisappear(animated)
    }


    // MARK: Actions

    @IBAction func close(_ sender: Any?) {
        if let delegate = delegate {
            delegate.done()
        }
        else {
            navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func enablePushPressed(_ sender: Any?) {
        EnablePushViewController.enablePush(self)
    }

    open class func enablePush(_ vc: UIViewController) {
        PushManager.shared.isPushAllowed { allowed, _ in
            if allowed {
                PushManager.shared.registerUserNotificationSettings()
            }
            else {
                // We have already asked user, so show a prompt to go to settings.

                AlertHelper.present(
                    vc,
                    message: "Please go to Settings and enable notifications for this app.".localize(),
                    title: "Notifications Disabled".localize(),
                    actions: [
                        AlertHelper.defaultAction("Settings".localize()) { _ in
                            if let url = URL(string: UIApplication.openSettingsURLString) {
                                UIApplication.shared.open(url)
                            }
                        },
                        AlertHelper.cancelAction()])
            }
        }
    }
}
