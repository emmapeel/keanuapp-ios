//
//  Router.swift
//  Keanu
//
//  Created by N-Pex on 06.02.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

/**
 Protocol used for indirecting view controller instantiation.

 This alleviates the problem of overriding view controllers in downstream apps. You don't need to override
 the calling view controller, if you want to change the way the called view controller looks and works.

 - Tag: Theme
 */
public protocol Router {


    // MARK: Onboarding

    /**
     Create a `WelcomeViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `WelcomeViewController`.
     */
    func welcome() -> WelcomeViewController

    /**
     Create a `AddAccountViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `AddAccountViewController`.
     */
    func addAccount() -> AddAccountViewController

    /**
     Create a `EnablePushViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `EnablePushViewController`.
     */
    func enablePush() -> EnablePushViewController


    // MARK: Main

    /**
     Create a `MainViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `MainViewController`.
     */
    func main() -> MainViewController

    /**
     Create a `ChatListViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `ChatListViewController`.
     */
    func chatList() -> ChatListViewController

    /**
     Create a `DiscoverViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `DiscoverViewController`.
     */
    func discover() -> DiscoverViewController

    /**
     Create a `MeViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `MeViewController`.
     */
    func me() -> MeViewController

    // MARK: Profile/settings view controllers
    
    /**
     Create a `MyProfileViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `MyProfileViewController`.
     */
    func myProfile() -> MyProfileViewController

    /**
     Create a `EditProfileViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `EditProfileViewController`.
     */
    func profileEdit() -> EditProfileViewController

    /**
     Create a `NotificationsViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `NotificationsViewController`.
     */
    func profileNotifications() -> NotificationsViewController

    
    /**
     Create a `PreferencesViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `PreferencesViewController`.
     */
    func profilePreferences() -> PreferencesViewController

    
    /**
     Create a `AccountViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `AccountViewController`.
     */
    func profileAccount() -> AccountViewController

    /**
     Create a `AboutViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `AboutViewController`.
     */
    func profileAbout() -> AboutViewController

    
    // MARK: Room

    /**
     Create a `RoomViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `RoomViewController`.
     */
    func room() -> RoomViewController

    /**
     Create a `RoomSettingsViewController`.

     - returns: A new instance of a `RoomSettingsViewController`.
     */
    func roomSettings() -> RoomSettingsViewController

    /**
     Create a `ProfileViewController`.

     - returns: A new instance of a `ProfileViewController`.
     */
    func profile() -> ProfileViewController

    /**
     Create a `StoryViewController`.

     - returns: A new instance of a `StoryViewController`.
     */
    func story() -> StoryViewController

    /**
     Create a `StoryAddMediaViewController`.

     - returns: A new instance of a `StoryAddMediaViewController`.
     */
    func storyAddMedia() -> StoryAddMediaViewController

    /**
     Create a `StoryGalleryViewController`.

     - returns: A new instance of a `StoryGalleryViewController`.
     */
    func storyGallery() -> StoryGalleryViewController

    /**
     Create a `StoryEditorViewController`.

     - returns: A new instance of a `StoryEditorViewController`.
     */
    func storyEditor() -> StoryEditorViewController

    /**
     Create a `StickerPackViewController`.

     - returns: A new instance of a `StickerPackViewController`.
     */
    func stickerPack() -> StickerPackViewController

    /**
     Create a `PickStickerViewController`.

     - returns: A new instance of a `PickStickerViewController`.
     */
    func pickSticker() -> PickStickerViewController


    // MARK: Friends

    /**
     Create a `ChooseFriendsViewController`.

     - returns: A new instance of a `ChooseFriendsViewController`.
     */
    func chooseFriends() -> ChooseFriendsViewController

    /**
     Create an `AddFriendViewController`.

     - returns: A new instance of an `AddFriendViewController`.
     */
    func addFriend() -> AddFriendViewController

    /**
     Create a `ShowQrViewController`.

     - returns: A new instance of a `ShowQrViewController`.
     */
    func showQr() -> ShowQrViewController

    /**
     Create a `QrScanViewController`.

     - returns: A new instance of a `QrScanViewController`.
     */
    func qrScan() -> QrScanViewController

    /**
     Create a `NewDeviceViewController`.

     - returns: A new instance of a `NewDeviceViewController`.
     */
    func newDevice() -> NewDeviceViewController

    /**
     Create a `ManualCompareViewController`.

     - returns: A new instance of a `ManualCompareViewController`.
     */
    func manualCompare() -> ManualCompareViewController

    /**
     Create an `IntrusionViewController`.

     - returns: A new instance of an `IntrusionViewController`.
     */
    func intrusion() -> IntrusionViewController

    /**
     Create a `VerificationViewController`.

     - returns: A new instance of a `VerificationViewController`.
     */
    func verification() -> VerificationViewController


    // MARK: Discover

    /**
     Create a `PhotoStreamViewController`.

     - returns: A new instance of a `PhotoStreamViewController`.
     */
    func photoStream() -> PhotoStreamViewController
    
    // MARK: Misc UI
    
    /**
     Create a chat message bubble of the given type, for the given rect.
     */
    func chatBubble(type: BubbleViewType, rect: CGRect) -> CGPath?
}
