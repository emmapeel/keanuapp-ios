//
//  MainViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 16.03.20.
//

import UIKit
import KeanuCore
import MatrixSDK
import PureLayout

open class MainViewController: UITabBarController {

    open lazy var syncingOverlay: UIView = {
        let label = UILabel(frame: .zero)

        label.text = "Syncing…".localize()
        label.textAlignment = .center

        label.backgroundColor = .keanuBackground

        label.layer.cornerRadius = 8
        label.layer.borderWidth = 2
        label.layer.borderColor = UIColor.systemGray.cgColor
        label.clipsToBounds = true

        label.autoSetDimension(.width, toSize: 160)
        label.autoSetDimension(.height, toSize: 32)

        return label
    }()

    override open func viewDidLoad() {
        super.viewDidLoad()

        let router = UIApplication.shared.router

        viewControllers = [
            item(router.chatList(), "ic_message_white", 1),
            item(router.chooseFriends(), "ic_group_white", 2),
            item(router.discover(), "ic_discover_white", 3),
            item(router.me(), "ic_face_white", 4)]

        let nc = NotificationCenter.default

        nc.addObserver(self, selector: #selector(syncStarts), name: .syncingStarts, object: nil)

        nc.addObserver(self, selector: #selector(syncStopped), name: .syncingStopped, object: nil)
        nc.addObserver(self, selector: #selector(syncStopped), name: .mxSessionDidSync, object: nil)
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        syncStarts()
    }

    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        let nc = NotificationCenter.default
        nc.removeObserver(self, name: .syncingStarts, object: nil)
        nc.removeObserver(self, name: .syncingStopped, object: nil)
        nc.removeObserver(self, name: .mxSessionDidSync, object: nil)
    }

    /**
     Create a `UINavigationController` with the given root view controller using the given named image
     as `UITabBarItem` and with the given tag.

     - parameter vc: The root view controller.
     - parameter image: The image name to use for the `UITabBarItem`.
     - parameter tag: The tag ID to use for the `UITabBarItem`. (Typically not important but it should be some unique number.
     - returns: a `UINavigationController` containing the `vc` as root view controller.
     */
    open func item(_ vc: UIViewController, _ image: String, _ tag: Int) -> UINavigationController {
        let navC = UINavigationController(rootViewController: vc)

        navC.tabBarItem = UITabBarItem(
            title: nil,
            image: UIImage(named: image, in: Bundle(for: type(of: self)), compatibleWith: nil),
            tag: tag)

        return navC
    }

    @discardableResult
    open func showChatList() -> UINavigationController? {
        return show(ChatListViewController.self)
    }

    @discardableResult
    open func showChooseFriends() -> UINavigationController? {
        return show(ChooseFriendsViewController.self)
    }

    @discardableResult
    open func showDiscover() -> UINavigationController? {
        return show(DiscoverViewController.self)
    }

    @discardableResult
    open func showMe() -> UINavigationController? {
        return show(MeViewController.self)
    }

    open func show(_ viewControllerType: UIViewController.Type) -> UINavigationController? {
        var index = 0

        for vc in viewControllers ?? [] {
            if type(of: vc) == viewControllerType {
                show(index)

                return nil
            }

            if let navC = vc as? UINavigationController {
                for vc in navC.viewControllers {
                    if type(of: vc) == viewControllerType {
                        show(index)
                        navC.popToViewController(vc, animated: false)

                        return navC
                    }
                }
            }

            index += 1
        }

        return nil
    }

    open func show(_ selectedIndex: Int) {
        // If we are presenting, close the presented VC first.
        if let presented = presentedViewController {
            presented.dismiss(animated: false)
        }

        self.selectedIndex = selectedIndex
    }

    @objc
    open func syncStarts() {
        view.addSubview(syncingOverlay)

        syncingOverlay.autoPinEdge(.bottom, to: .top, of: tabBar, withOffset: -8)
        syncingOverlay.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }

    @objc
    open func syncStopped() {
        syncingOverlay.removeFromSuperview()
    }
}
