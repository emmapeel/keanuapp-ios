//
//  UITableView+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 02.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

extension UITableView {
    
    /**
     Return true if the height of the content is equal or taller than the viewport, i.e. the table view is "filled".
     */
    @objc
    open var viewportIsFilled: Bool {
        return contentSize.height >= frame.height
    }
    
    /**
     Helper to return the first index path in a UITableView
     
     - Returns: the first valid IndexPath in the UITableView, or nil
     */
    @objc
    open var firstIndexPath: IndexPath? {
        for sectionIndex in (0 ..< numberOfSections) {
            if numberOfRows(inSection: sectionIndex) > 0 {
                return IndexPath(row: 0, section: sectionIndex)
            }
        }

        return nil
    }

    /**
     Helper to return the last index path in a UITableView
     
     - Returns: the last valid IndexPath in the UITableView, or nil
     */
    @objc
    open var lastIndexPath: IndexPath? {
        for sectionIndex in (0 ..< numberOfSections).reversed() {
            let numberOfRows = numberOfRows(inSection: sectionIndex)

            if numberOfRows > 0 {
                return IndexPath(row: numberOfRows - 1, section: sectionIndex)
            }
        }

        return nil
    }
    
    /**
     Check if the given IndexPath is valid, i.e. a row exists at that location.
     */
    func isValidIndexPath(indexPath: IndexPath) -> Bool {
        return indexPath.section < numberOfSections && indexPath.row < numberOfRows(inSection: indexPath.section)
    }
}
