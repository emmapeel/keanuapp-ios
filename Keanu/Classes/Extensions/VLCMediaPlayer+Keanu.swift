//
//  VLCMediaPlayer+Keanu.swift
//  Keanu
//
//  Created by Benjamin Erhart on 17.06.21.
//

import Foundation
import MobileVLCKit

extension VLCMediaPlayer {

    var duration: TimeInterval {
        return (media?.length.value?.doubleValue ?? 0) / 1000.0
    }

    var currentTime: TimeInterval {
        return (time.value?.doubleValue ?? 0) / 1000.0
    }
}
