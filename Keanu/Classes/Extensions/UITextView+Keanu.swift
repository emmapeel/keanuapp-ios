//
//  UITextView+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 30.12.19.
//

import KeanuCore

public extension UITextView {
    
    @IBInspectable
    var textInsets: String {
        get {
            let i = textContainerInset

            return "\(i.top),\(i.left),\(i.bottom),\(i.right)"
        }
        set {
            let values = newValue.split(separator: ",")

            let s2f = { (s: Substring?, d: Float) -> CGFloat in
                return CGFloat(Float((s ?? "").trimmingCharacters(in: .whitespacesAndNewlines)) ?? d)
            }

            textContainerInset = UIEdgeInsets(
                top: s2f(values[safe: 0], 8),
                left: s2f(values[safe: 1], 0),
                bottom: s2f(values[safe: 2], 8),
                right: s2f(values[safe: 3], 0))
        }
    }
}
