//
//  ChatListDataSource.swift
//  Keanu
//
//  Created by N-Pex on 28.09.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore

public protocol ChatListDataSourceDelegate: RoomSummariesDataSourceDelegate {

    func createCell(at indexPath: IndexPath, for device: ChatListDataSource.Device) -> UITableViewCell?
}

/**
 Data source to be used for chat list table view.
 
 Extends the RoomSummariesDataSource with the flag "displayArchive".
 By default, the data source filters all rooms having "isArchived" set in
 their "others" parameter.
 To invert this behavior (i.e. ONLY show rooms with isArchived), set the
 "displayArchive" property to true.
 */
open class ChatListDataSource: RoomSummariesDataSource {

    public typealias Device = (info: MXDeviceInfo, session: MXSession)

    open var newDevices = [Device]()

    open var hasNewDevices: Bool {
        return !newDevices.isEmpty
    }

    public override init() {
        super.init()

        let nc = NotificationCenter.default

        nc.addObserver(self, selector: #selector(newDevice),
                       name: .MXCrossSigningMyUserDidSignInOnNewDevice, object: nil)

        nc.addObserver(self, selector: #selector(verification),
                       name: .MXKeyVerificationTransactionDidChange, object: nil)
    }

    /**
     Return true if there are ANY archived rooms in any of the active accounts.
     */
    public var hasArchivedRooms: Bool {
        return MXKAccountManager.shared().activeAccounts.contains(where: { (account) -> Bool in
            return account.mxSession?.rooms.contains(where: { room in
                return account.mxSession?.roomSummary(withRoomId: room.roomId)?.isArchived ?? false
            }) ?? false
        })
    }
    
    var displayArchive: Bool = false {
        didSet(newValue) {
            reload()
        }
    }
    
    open override func shouldAddRoomSummary(_ roomSummary: MXRoomSummary) -> Bool {
        return displayArchive == roomSummary.isArchived
    }

    /**
     Callback for `MXCrossSigningMyUserDidSignInOnNewDevice` notification.

     Adds all new devices, where the user recently signed in on to a list at the top of the scene.

     - parameter notification: Should containing a list of device IDs.
     */
    @objc func newDevice(_ notification: Notification) {
        guard let deviceIds = (notification.userInfo?["deviceIds"] as? [String]) else {
            return
        }

        let originalCount = newDevices.count

        for deviceId in deviceIds {
            for account in MXKAccountManager.shared().activeAccounts {
                let session = account.mxSession
                let info = session?.crypto?.device(withDeviceId: deviceId, ofUser: account.matrixId)

                if let info = info, let session = session {
                    let device: Device = (info, session)

                    if !newDevices.contains(where: { $0.info.deviceId == device.info.deviceId }) {
                        newDevices.append(device)
                    }

                    break
                }
            }
        }

        if originalCount != newDevices.count {
            DispatchQueue.main.async {
                self.delegate?.didChangeAllRows()
            }
        }
    }

    /**
     Callback for `MXKeyVerificationTransactionDidChange` notification.

     Checks, if a device was verified and removes it from the new devices list.

     - parameter notification: Should contain a `MXSASTransaction`.
     */
    @objc func verification(_ notification: Notification) {
        guard let transaction = notification.object as? MXSASTransaction,
            transaction.state == MXSASTransactionStateVerified else {
                return
        }

        let originalCount = newDevices.count

        newDevices.removeAll { $0.info.deviceId == transaction.otherDeviceId }

        if originalCount != newDevices.count {
            DispatchQueue.main.async {
                self.delegate?.didChangeAllRows()
            }
        }
    }

    open override func numberOfSections(in tableView: UITableView) -> Int {
        return super.numberOfSections(in: tableView) + (hasNewDevices ? 1 : 0)
    }

    open override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if hasNewDevices && section == 0 {
            return newDevices.count
        }

        return super.tableView(tableView, numberOfRowsInSection: section)
    }

    open override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if hasNewDevices && indexPath.section == 0,
            let cell = (delegate as? ChatListDataSourceDelegate)?.createCell(at: indexPath, for: newDevices[indexPath.row])
        {
            return cell
        }

        return super.tableView(tableView, cellForRowAt: indexPath)
    }
}

