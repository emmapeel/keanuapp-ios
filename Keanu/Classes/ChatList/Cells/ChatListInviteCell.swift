//
//  ChatListInviteCell.swift
//  Keanu
//
//  Created by N-Pex on 01.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK
import KeanuCore

protocol ChatListInviteCellDelegate: AnyObject {

    func acceptInviteForRoom(roomId:String)

    func declineInviteForRoom(roomId:String)
}

open class ChatListInviteCell: RoomCell {

    weak var delegate: ChatListInviteCellDelegate?

    var roomId: String?
    
    override open func apply(_ roomSummary: MXRoomSummary) -> Self {
        // Store the id so we can fetch it from handlers.
        roomId = roomSummary.roomId

        return super.apply(roomSummary)
    }
    
    /**
     Action for accepting a room invitation
     */
    @IBAction func acceptInviteForRoom(_ sender: Any) {
        if let roomId = roomId {
            self.delegate?.acceptInviteForRoom(roomId: roomId)
        }
    }
    
    /**
     Action for declining a room invitation
     */
    @IBAction func declineInviteForRoom(_ sender: Any) {
        if let roomId = roomId {
            self.delegate?.declineInviteForRoom(roomId: roomId)
        }
    }
}
