//
//  NoFriendsDataSource.swift
//  Keanu
//
//  Created by N-Pex on 16.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import KeanuCore

/**
 Data source for when you have no friends yet. A single "Add friends" cell is shown.
 */
open class NoFriendsDataSource: NSObject, UITableViewDataSource {
    
    public var delegate: NoFriendsDataSourceDelegate?
    
    // Notification center listener objects
    var observers = [NSObjectProtocol]()
    
    override init() {
        super.init()
        
        observers.append(
            NotificationCenter.default.addObserver(
                forName: .friendsManagerChanged, object: nil, queue: .main)
            { notification in
                if FriendsManager.shared.friends.count > 0, let delegate = self.delegate {
                    self.observers.forEach({ NotificationCenter.default.removeObserver($0) })
                    self.observers.removeAll()
                    delegate.shouldCloseDataSource(self)
                }
            })
    }
    
    deinit {
        observers.forEach({ NotificationCenter.default.removeObserver($0) })
        observers.removeAll()
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = "Add friends".localize()
        cell.detailTextLabel?.text = "Tap here to add friends and start chatting".localize()
        return cell
    }
}
