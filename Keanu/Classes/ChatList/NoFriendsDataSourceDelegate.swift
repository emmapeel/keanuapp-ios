//
//  NoFriendsDataSourceDelegate.swift
//  Keanu
//
//  Created by N-Pex on 16.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

public protocol NoFriendsDataSourceDelegate {
    func shouldCloseDataSource(_ dataSource:NoFriendsDataSource)
}

