//
//  ChatListViewController.swift
//  Keanu
//
//  Created by N-Pex on 26.09.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore

open class ChatListViewController: UIViewController, ChatListProtocol, ChatListDataSourceDelegate,
UITableViewDelegate, ChooseFriendsDelegate {

    // MARK: Storyboard outlets
    @IBOutlet weak open var tableView: UITableView!

    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()

    // MARK: Properties
    private var dataSource: ChatListDataSource?
    open var archiveSegmentedControl: UISegmentedControl!

    open override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Chats".localize()

        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(barButtonSystemItem: .add,
                            target: self, action: #selector(chooseFriendsForNewRoom)),
            UIBarButtonItem(barButtonSystemItem: .search,
                            target: self, action: #selector(joinRoom))]

        // Install a segmented control as titleView. This controls whether we show "archived"
        // rooms or "non-archived" rooms. Basically archiving is a custom (i.e. not built in
        // to matrix sdk) way to hide rooms.
        archiveSegmentedControl = UISegmentedControl(items: [
            "Active".localize(), // Tab for active chats
            "Archive".localize() // Tab for archived chats
            ])
        archiveSegmentedControl.selectedSegmentIndex = 0
        archiveSegmentedControl.addTarget(self, action: #selector(archivedSegmentedControlValueChanged(_:)), for: .valueChanged)
        navigationItem.titleView = archiveSegmentedControl

        // Set ourselves as delegate, we want to provide row actions
        tableView.delegate = self

        // Set default row height. Actually just used when the table view is empty.
        tableView.rowHeight = RoomCell.height

        // Register cell types
        tableView.register(RoomCell.nib, forCellReuseIdentifier: RoomCell.defaultReuseId)
        tableView.register(ChatListInviteCell.nib, forCellReuseIdentifier: ChatListInviteCell.defaultReuseId)
        tableView.register(NewDeviceCell.nib, forCellReuseIdentifier: NewDeviceCell.defaultReuseId)
    }

    deinit {
        dataSource = nil
    }

    open func createDataSource() -> ChatListDataSource? {
        return ChatListDataSource()
    }

    open override func viewWillAppear(_ animated: Bool) {
        // Create the table view data source
        dataSource = createDataSource()
        dataSource?.delegate = self
        dataSource?.displayArchive = (archiveSegmentedControl.selectedSegmentIndex == 1)
        tableView.dataSource = dataSource
        super.viewWillAppear(animated)
    }

    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let ds = tableView.dataSource as? NoFriendsDataSource {
            ds.delegate = nil
        }
        tableView.dataSource = nil
        dataSource = nil
    }


    // MARK: ChatListDataSourceDelegate

    open func createCell(at indexPath: IndexPath, for roomSummary: MXRoomSummary) -> UITableViewCell?
    {
        // Is this an invite cell or a "normal" chat cell?
        if roomSummary.isInvite {
            let cell = tableView.dequeueReusableCell(
                withIdentifier: ChatListInviteCell.defaultReuseId, for: indexPath) as? ChatListInviteCell
            cell?.delegate = self

            return cell?.apply(roomSummary)
        }
        else {
            let cell = tableView.dequeueReusableCell(
                withIdentifier: RoomCell.defaultReuseId, for: indexPath) as? RoomCell

            return cell?.apply(roomSummary)
        }
    }

    open func createCell(at indexPath: IndexPath, for device: ChatListDataSource.Device) -> UITableViewCell?
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: NewDeviceCell.defaultReuseId, for: indexPath) as? NewDeviceCell

        return cell?.apply(device)
    }

    /**
     Check if we should show the active/archive segmented control. If we have no archived rooms we hide it. Make sure that if we hide it, we are on the first segment, i.e. "active".
     */
    private func checkShowArchived() {
        archiveSegmentedControl.isHidden = !(dataSource?.hasArchivedRooms ?? false)
        navigationItem.titleView = (archiveSegmentedControl.isHidden) ? nil : archiveSegmentedControl
        if archiveSegmentedControl.isHidden, archiveSegmentedControl.selectedSegmentIndex != 0 {
            DispatchQueue.main.async {
                self.archiveSegmentedControl.selectedSegmentIndex = 0
                self.archivedSegmentedControlValueChanged(self.archiveSegmentedControl)
            }
        }
    }

    public func didChangeAllRows() {
        tableView.reloadData()
        checkShowArchived()
        showNoFriendsIfNoItems()
    }

    public func didChangeRows(at indexPaths: [IndexPath]) {
        tableView.reloadData()
        checkShowArchived()
        showNoFriendsIfNoItems()
    }

    private func showNoFriendsIfNoItems() {
        guard let ds = dataSource else { return }
        if ds.tableView(tableView, numberOfRowsInSection: 0) == 0, FriendsManager.shared.friends.count == 0 {
            let dsNoItems = NoFriendsDataSource()
            dsNoItems.delegate = self
            tableView.dataSource = dsNoItems
        } else if tableView.dataSource == nil || tableView.dataSource is NoFriendsDataSource {
            tableView.dataSource = ds
        }
    }


    // MARK: UITableViewDelegate

    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && dataSource?.hasNewDevices ?? false {
            return NewDeviceCell.height
        }

        return UITableView.automaticDimension
    }

    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if indexPath.section == 0 && dataSource?.hasNewDevices ?? false {
            if let device = dataSource?.newDevices[indexPath.row] {
                VerificationManager.shared.interactiveVerifyOwn(device.session, device.info)
            }

            return
        }

        if tableView.dataSource is NoFriendsDataSource {
            UIApplication.shared.openAddFriendsViewController()
            return
        }
        
        if let roomSummary = dataSource?.roomSummary(at: indexPath), !roomSummary.isArchived, let room = roomSummary.room {
            
            // Only allow room opening if we have joined the room
            if roomSummary.membership == .join || roomSummary.membership == .invite {
                openRoom(room)
            }
        }
    }

    open func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if let roomSummary = dataSource?.roomSummary(at: indexPath) {
            let actions: [UIContextualAction]
            
            if roomSummary.isInvite {
                // Invites can be accepted or rejected.
                actions = [acceptInvite(indexPath), declineInvite(indexPath)]
            }
            else {
                // Normal chats can be deleted or archived/unarchived depending on state.
                if roomSummary.isArchived {
                    actions = [unarchiveRoom(indexPath)]
                }
                else {
                    actions = [archiveRoom(indexPath)]
                }
            }

            return UISwipeActionsConfiguration(actions: actions)
        }

        return nil
    }

    func didJoinRoom(_ roomSummary:MXRoomSummary) {
    }

    func didFailToJoinRoom(_ roomSummary:MXRoomSummary, with error:Error?) {
    }


    // MARK: ChooseFriendsDelegate

    /**
     User returned from `ChooseFriendsViewController` and selected a list of
     friends.

     List should contain *at least* one friend as per contract!

     Will use the session (the account), where the first friend is attached to.

     - parameter friends: List of `MXUser` objects the user selected.
    */
    public func friendsChosen(_ friends: [Friend]) {
        guard friends.count > 0 else {return}
        UIApplication.shared.openRoom(friends)
    }

    /**
     Create an empty, excrypted, room, to which you can later add members.
     */
    public func createEmptyRoom(session: MXSession) {
        let vc = UIApplication.shared.router.room()
        vc.createEmptyRoom(session: session)

        navigationController?.pushViewController(vc, animated: true)
    }


    // MARK: Storyboard bindable actions

    @IBAction func archivedSegmentedControlValueChanged(_ sender: UISegmentedControl) {
        dataSource?.displayArchive = (sender.selectedSegmentIndex == 1)
        tableView.reloadData()
    }


    // MARK: Actions
    
    /**
     Open the given room.
     */
    open func openRoom(_ room: MXRoom?) {
        let vc = UIApplication.shared.router.room()
        vc.room = room

        navigationController?.pushViewController(vc, animated: true)
    }
    
    /**
     Show the `ChooseFriendsViewController` to choose friends for a new room.
    */
    @IBAction func chooseFriendsForNewRoom(_ sender: Any) {
        let vc = UIApplication.shared.router.chooseFriends()
        vc.delegate = self
        vc.multiselect = true

        present(UINavigationController(rootViewController: vc), animated: true)
    }

    @IBAction func joinRoom(_ sender: Any) {
        let alert = AlertHelper.build(
            title: "Join Known Room".localize(),
            actions: [AlertHelper.cancelAction()])

        alert.addAction(AlertHelper.defaultAction("Join/Knock".localize(), handler: { [weak self] _ in
            guard let roomId = alert.textFields?.first?.text, !roomId.isEmpty else {
                return
            }

            self?.workingOverlay.isHidden = false
            UIApplication.shared.joinRoom(roomId, uninvited: true) { success, room, error in
                self?.workingOverlay.isHidden = true

                if success {
                    self?.openRoom(room)
                }
                else {
                    if let self = self {
                        AlertHelper.present(self, message: error?.localizedDescription ?? "Unable to join room.".localize())
                    }
                }
            }
        }))

        AlertHelper.addTextField(alert, placeholder: "#room-id:example.org")

        present(alert, animated: true)
    }


    // MARK: Private Methods

    /**
     Action for accepting an invite.
     */
    private func acceptInvite(_ indexPath: IndexPath) -> UIContextualAction {
        UIContextualAction(
            style: .normal,
            title: "Accept".localize()
        ) { [weak self] (_, _, completionHandler) in

            guard let self = self,
                let roomSummary = self.dataSource?.roomSummary(at: indexPath)
            else {
                return completionHandler(false)
            }

            self.acceptInviteForRoom(roomId: roomSummary.roomId)

            completionHandler(true)
        }
    }

    /**
     Action for declining an invite.
     */
    private func declineInvite(_ indexPath: IndexPath) -> UIContextualAction {
        UIContextualAction(
            style: .destructive,
            title: "Decline".localize()
        ) { [weak self] (_, _, completionHandler) in

            guard let self = self,
                  let roomSummary = self.dataSource?.roomSummary(at: indexPath)
            else {
                return completionHandler(false)
            }

            self.declineInviteForRoom(roomId: roomSummary.roomId)

            completionHandler(true)
        }
    }

    /**
     Action for unarchiving a room.
     */
    private func unarchiveRoom(_ indexPath: IndexPath) -> UIContextualAction {
        UIContextualAction(
            style: .normal,
            title: "Unarchive".localize()
        ) { [weak self] (_, _, completionHandler) in

            guard let self = self,
                let roomSummary = self.dataSource?.roomSummary(at: indexPath)
            else {
                return completionHandler(false)
            }

            roomSummary.isArchived = false
            self.dataSource?.reload()

            completionHandler(true)
        }
    }

    /**
     Action for archiving a room.
     */
    private func archiveRoom(_ indexPath: IndexPath) -> UIContextualAction {
        UIContextualAction(
            style: .normal,
            title: "Archive".localize()
        ) { [weak self] (_, _, completionHandler) in

            guard let self = self,
                  let roomSummary = self.dataSource?.roomSummary(at: indexPath)
            else {
                return completionHandler(false)
            }

            roomSummary.isArchived = true
            self.dataSource?.reload()

            completionHandler(true)
        }
    }
}

extension ChatListViewController: ChatListInviteCellDelegate {
    func acceptInviteForRoom(roomId: String) {
        workingOverlay.isHidden = false

        UIApplication.shared.joinRoom(roomId) { [weak self] success, room, error in
            self?.workingOverlay.isHidden = true

            if success {
                self?.openRoom(room)
            }
            else {
                if let self = self {
                    AlertHelper.present(self, message: error?.localizedDescription ?? "Unable to join room.".localize())
                }
            }
        }
    }

    func declineInviteForRoom(roomId: String) {
        workingOverlay.isHidden = false
        UIApplication.shared.declineRoomInvite(roomId: roomId) { (success) in
            self.workingOverlay.isHidden = true
        }
    }
}

extension ChatListViewController: NoFriendsDataSourceDelegate {
    public func shouldCloseDataSource(_ noFriendsDataSource: NoFriendsDataSource) {
        noFriendsDataSource.delegate = nil
        
        // Set the real DataSource on the tableView
        tableView.dataSource = self.dataSource
        tableView.reloadData()
    }
}
