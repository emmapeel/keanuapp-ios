//
//  RecaptchaTermsTokenViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 15.10.18.
//  Copyright © 2018 - 2019 Guardian Project. All rights reserved.
//

import MatrixSDK

/**
 Protocol to receive the secret from the solved Recaptcha.
*/
public protocol RecaptchaTermsTokenViewControllerDelegate {
    
    /**
     The delegate receives this, when the user successfully solved the Recaptcha and accepted all terms.
     
     The `RecaptchaTermsTokenViewController` will dismiss itself right after it.
     
     - parameter secret: The eventual secret from the solved Recaptcha.
     - parameter regToken: The eventual registration token entered by the user.
    */
    func success(_ secret: String?, _ regToken: String?)
}

/**
 `UIViewController` which only purpose it is to show a Recaptcha in a `WKWebView`.
 
 Will call `RecaptchaViewControllerDelegate#setSecret(String)` upon the user
 successfully solving the recaptcha and dismisses itself again.
 
 The user can click a "Cancel" button in a navigation bar to dismiss this
 view controller without solving the Recaptcha.
 */
open class RecaptchaTermsTokenViewController: UINavigationController {

    private let successDelegate: RecaptchaTermsTokenViewControllerDelegate?
    private var vcs = [UIViewController]()
    private var index = 0

    var secret: String?
    var regToken: String?

    public init(_ homeServer: String?, _ publicKey: String?, _ terms: MXLoginTerms?,
                _ tokenNeeded: Bool, delegate: RecaptchaTermsTokenViewControllerDelegate
    ) {
        successDelegate = delegate

        super.init(nibName: nil, bundle: nil)

        // Should go first. No need to mess with CAPTCHAs, if the user doesn't have a token.
        if tokenNeeded {
            let vc = PasswordViewController()
            vc.title = "Registration Token".localize()
            vc.message = "This server needs a token to be able to register an account.".localize()
                + "\n"
                + "You should have received this by the server operator.".localize()
            vc.passwordLabel = "Registration Token".localize()
            vc.popOnClose = true
            vc.completion = { [weak self] vc in
                self?.regToken = vc.password
            }

            vcs.append(vc)
        }

        if let homeServer = homeServer, let publicKey = publicKey {
            vcs.append(RecaptchaViewController(homeServer, publicKey))
        }

        let policies = terms?.policiesData(forLanguage: Locale.preferredLanguages.first, defaultLanguage: "en") ?? []

        for policy in policies {
            vcs.append(PolicyViewController(policy))
        }

        setViewController(vcs.first)
    }

    required public init?(coder: NSCoder) {
        successDelegate = coder.decodeObject(forKey: "successDelegate") as? RecaptchaTermsTokenViewControllerDelegate

        super.init(coder: coder)
    }

    open override func popViewController(animated: Bool) -> UIViewController? {
        if index < vcs.count - 1 {
            index += 1

            setViewController(vcs[index])

            return vcs[index - 1]
        }

        dismiss(animated: true) {
            self.successDelegate?.success(self.secret, self.regToken)
        }

        return vcs[index]
    }

    private func setViewController(_ viewController: UIViewController?) {
        var viewControllers = [UIViewController]()

        if let viewController = viewController {
            viewControllers.append(viewController)
        }

        setViewControllers(viewControllers, animated: true)

        viewControllers.first?.navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
    }

    @objc private func cancel() {
        dismiss(animated: true)
    }
}
