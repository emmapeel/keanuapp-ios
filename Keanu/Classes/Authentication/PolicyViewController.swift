//
//  PolicyViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 30.09.19.
//  Copyright © 2019 Guardian Project. All rights reserved.

import UIKit
import WebKit
import MatrixSDK

/**
`UIViewController` which only purpose it is to show a given `MXLoginPolicy` in a `WKWebView`,
 which is a link to a URL and a title.
*/
class PolicyViewController: UIViewController {

    let policy: MXLoginPolicyData

    init(_ policy: MXLoginPolicyData, isLast: Bool = true) {
        self.policy = policy

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        policy = coder.decodeObject(forKey: "policy") as! MXLoginPolicyData

        super.init(coder: coder)
    }

    override func encode(with coder: NSCoder) {
        coder.encode(policy, forKey: "policy")

        super.encode(with: coder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = policy.name
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: "Accept".localize(), style: .plain, target: self, action: #selector(accepted))

        let webView = WKWebView(frame: view.bounds)
        webView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(webView)
        webView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        webView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        webView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        if let url = URL(string: policy.url) {
            webView.load(URLRequest(url: url))
        }
    }

    @objc func accepted() {
        navigationController?.popViewController(animated: true)
    }
}
