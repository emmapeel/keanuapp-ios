//
//  QuickReactionPicker.swift
//  Keanu
//
//  Created by N-Pex on 08.05.20.
//

import UIKit
import ISEmojiView

open class QuickReactionPicker: UIView, ISEmojiView.EmojiViewDelegate, UIGestureRecognizerDelegate {

    var didPickEmojiCallback: ((String) -> Void)? = nil
    

    open var emojiPickerBottomConstraint: NSLayoutConstraint?

    open var closeTapRecognizer: UITapGestureRecognizer?

    open lazy var emojiPicker: ISEmojiView.EmojiView = {
        let view = ISEmojiView.EmojiView(keyboardSettings: KeyboardSettings(bottomType: .categories))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self

        return view
    }()


    @discardableResult
    open func showIn(_ view: UIView, didPickEmoji: @escaping (String) -> Void) -> Self {
        didPickEmojiCallback = didPickEmoji
        
        // First add ourselves to parent view.
        backgroundColor = UIColor(white: 0, alpha: 0.5)
        translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(self)
        autoPinEdgesToSuperviewEdges()

        closeTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
        closeTapRecognizer?.delegate = self
        addGestureRecognizer(closeTapRecognizer!)

        // Then add emoji view to ourselves.
        self.addSubview(emojiPicker)
        
        emojiPickerBottomConstraint = emojiPicker.autoPinEdge(toSuperviewEdge: .bottom, withInset: 0)
        emojiPicker.autoPinEdge(toSuperviewEdge: .leading)
        emojiPicker.autoPinEdge(toSuperviewEdge: .trailing)
        
        closeTapRecognizer?.isEnabled = true

        emojiPicker.layoutIfNeeded()

        emojiPickerBottomConstraint?.constant = emojiPicker.frame.height
        layoutIfNeeded()

        DispatchQueue.main.async {
            self.emojiPickerBottomConstraint?.constant = 0

            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                self?.layoutIfNeeded()
            })
        }

        return self
    }

    @objc public func close() {
        closeTapRecognizer?.isEnabled = false

        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.emojiPickerBottomConstraint?.constant = self?.emojiPicker.frame.height ?? 0
            self?.layoutIfNeeded()
        }) { [weak self] _ in
            self?.emojiPicker.removeFromSuperview()
            self?.removeFromSuperview()
        }
    }
    
    public func emojiViewDidSelectEmoji(_ emoji: String, emojiView: ISEmojiView.EmojiView) {
        didPickEmojiCallback?(emoji)

        close()
    }
    
    open override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == closeTapRecognizer {
            // Close only on background tap, not when tapping anything else.
            return hitTest(gestureRecognizer.location(in: self), with: nil) == self
        }

        return super.gestureRecognizerShouldBegin(gestureRecognizer)
    }
}
