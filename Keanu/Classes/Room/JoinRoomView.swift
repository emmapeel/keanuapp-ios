//
//  JoinRoomView.swift
//  ChatSecure
//
//  Created by N-Pex on 14.01.18.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK

open class JoinRoomView: UIView {
    
    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var membersTable: UITableView!

    open var room:MXRoom? {
        didSet {
            didChangeRoom(from: oldValue, to: room)
        }
    }
    open var roomMembersDataSource:RoomMembersDataSource?
    
    open var acceptButtonCallback:(() -> Void)?
    open var declineButtonCallback:(() -> Void)?
    
    open override func awakeFromNib() {
        super.awakeFromNib()

        // Register cell types
        membersTable.register(RoomMemberCell.nib, forCellReuseIdentifier: RoomMemberCell.defaultReuseId)
    }
    
    open override var canBecomeFirstResponder: Bool {
        return true
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        self.acceptButton.layer.cornerRadius = self.acceptButton.frame.height / 2
    }
    
    @IBAction func joinRoomAccept(sender: AnyObject) {
        if let callback = self.acceptButtonCallback {
            callback()
        }
    }
    
    @IBAction func joinRoomDecline(sender: AnyObject) {
        if let callback = self.declineButtonCallback {
            callback()
        }
    }
    
    open func didChangeRoom(from: MXRoom?, to: MXRoom?) {
        roomMembersDataSource?.delegate = nil
        roomMembersDataSource = nil
        membersTable.dataSource = nil
        if let room = to {
            roomMembersDataSource = RoomMembersDataSource(room: room)
            if let roomMembersDataSource = roomMembersDataSource {
                roomMembersDataSource.section = 0
                roomMembersDataSource.delegate = self
            }
            membersTable.dataSource = roomMembersDataSource
        }
    }
}

// MARK: - RoomMembersDataSourceDelegate
extension JoinRoomView: RoomMembersDataSourceDelegate {
    public func createCell(at indexPath: IndexPath, for roomMember: MXRoomMember) -> UITableViewCell? {
        guard let room = self.room, let cellData = roomMembersDataSource?.roomMember(at: indexPath) else {return nil}
        let cell = membersTable.dequeueReusableCell(withIdentifier: RoomMemberCell.defaultReuseId, for: indexPath)
        if let cell = cell as? RoomMemberCell {
            cell.render(member: cellData, room: room)
        }
        cell.backgroundColor = .clear
        return cell
    }
    
    public func didChangeAllRows() {
        self.membersTable?.reloadSections(IndexSet(arrayLiteral: 0), with: .automatic)
    }
    
    public func didChangeRows(at indexPaths: [IndexPath]) {
        self.membersTable?.reloadSections(IndexSet(arrayLiteral: 0), with: .automatic)
    }
}
