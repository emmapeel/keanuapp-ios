//
//  RoomManager.swift
//  Keanu
//
//  Created by Benjamin Erhart on 10.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import KeanuCore

open class RoomManager {

    /**
     Singleton instance. Will load the friends list from disk on first access.
     */
    public static let shared: RoomManager = {
        return RoomManager().setup()
    }()

    /**
     The id of the room that is currently displayed, or nil if the room view
     controller is not open.
     */
    open var currentlyViewedRoomId: String? {
        didSet {
            if currentlyViewedRoomId != nil {
                PushManager.shared.updateNotificationAndBadge(false)
            }
        }
    }

    /**
     An map of notification listeners. Key is the session, value is a listener object.
     */
    private var sessionNotificationListeners: [MXSession: Any] = [:]

    /**
     An map of notification observers. Key is the session, value is an array of observers.
     */
    private var sessionNotificationObservers: [MXSession: [NSObjectProtocol]?] = [:]

    /**
     A view controller used for asking the user to share the room keys when a
     new device is detected.
     */
    private var roomKeyViewController: UIAlertController?

    private var myDevicesVc: MyDevicesViewController?

    private func setup() -> RoomManager {

        for account in MXKAccountManager.shared().activeAccounts {
            if let session = account.mxSession,
               session.state.rawValue >= MXSessionState.storeDataReady.rawValue {
                // Already inited, make sure to add listener
                onSessionReady(session)
            }
        }
        
        // Install a listener for session state updates
        NotificationCenter.default.addObserver(
            forName: .mxSessionStateDidChange, object: nil,
            queue: OperationQueue.main, using: { notification in

                guard let session = notification.object as? MXSession else {
                    return
                }

                switch session.state {
                case .storeDataReady:
                    self.onSessionReady(session)

                case .closed:
                    // Remove listener.
                    if let listener = self.sessionNotificationListeners[session] {
                        session.notificationCenter?.removeListener(listener)
                        self.sessionNotificationListeners.removeValue(forKey: session)
                    }

                    self.stopObserversForSession(session)

                case .running:
                    self.checkPendingRoomKeyRequests()

                default:
                    break
                }
            })

        return self
    }

    private func onSessionReady(_ session: MXSession) {
        self.startObserversForSession(session)
        
        // Only create listener once.
        if !self.sessionNotificationListeners.keys.contains(session) {
            self.sessionNotificationListeners[session] = session.notificationCenter.listen(
                toNotifications: { [weak session](event, roomState, pushRule) in
                    
                    guard let session = session, let event = event,
                        let roomId = event.roomId else { return }
                    
                    // Is the event for the currently shown room?
                    if UIApplication.shared.applicationState == .active,
                        let currentlyViewedId = self.currentlyViewedRoomId,
                        currentlyViewedId == roomId {
                        // Current room, ignore this
                    } else {
                        PushManager.shared.showLocalNotificationFor(
                            event: event, session: session)
                    }
            })
        }
    }
    
    private func startObserversForSession(_ session: MXSession) {
        let roomKeyRequestObserver = NotificationCenter.default.addObserver(
            forName: .mxCryptoRoomKeyRequest, object: session.crypto, queue: OperationQueue.main)
        { notification in
            self.checkPendingRoomKeyRequestsIn(session)
        }

        let roomKeyRequestCancellationObserver = NotificationCenter.default.addObserver(
            forName: .mxCryptoRoomKeyRequestCancellation, object: session.crypto,
            queue: OperationQueue.main)
        { notification in
            self.checkPendingRoomKeyRequestsIn(session)
        }

        sessionNotificationObservers[session] = [roomKeyRequestObserver,
                                                 roomKeyRequestCancellationObserver]
    }

    private func stopObserversForSession(_ session: MXSession) {
        if let observers = sessionNotificationObservers[session] {
            observers?.forEach { NotificationCenter.default.removeObserver($0) }
            sessionNotificationObservers.removeValue(forKey: session)
        }
    }

    private func checkPendingRoomKeyRequests() {
        MXKAccountManager.shared().activeAccounts.forEach({ (account) in
            if let session = account.mxSession {
                checkPendingRoomKeyRequestsIn(session)
            }
        })
    }

    private func checkPendingRoomKeyRequestsIn(_ session: MXSession) {
        guard UIApplication.shared.applicationState == .active else {
            debugPrint("checkPendingRoomKeyRequestsIn called while app not active. Ignoring.")
            return
        }

        (session.crypto as? MXLegacyCrypto)?.pendingKeyRequests { [weak self] pendingKeyRequests in
            guard pendingKeyRequests?.count ?? 0 > 0,
                // Pick the first coming user/device pair.
                let userId = pendingKeyRequests?.userIds().first,
                let deviceId = pendingKeyRequests?.deviceIds(forUser: userId)?.first else {
                    return
            }

            // Give the client a chance to refresh the device list.
            session.crypto.downloadKeys(
                [userId], forceDownload: false,
                success: { [weak self] map, crossSigningInfo in
                    if let deviceInfo = map?.object(forDevice: deviceId, forUser: userId) {
                        if deviceInfo.trustLevel?.localVerificationStatus == .unknown {
                            session.crypto.setDeviceVerification(
                                .unverified, forDevice: deviceId, ofUser: userId,
                                success: { [weak self] in
                                    self?.showRoomKeyRequestDialog(
                                        userId, deviceInfo: deviceInfo, wasNewDevice: true, session)
                                },
                                failure: { [weak self] _ in
                                    self?.checkPendingRoomKeyRequests()
                                })
                        }
                        else {
                            self?.showRoomKeyRequestDialog(
                                userId, deviceInfo: deviceInfo, wasNewDevice: false, session)
                        }
                    }
                    else {
                        debugPrint("[\(String(describing: type(of: self)))] checkPendingRoomKeyRequestsInSession: No details found for device \(userId):\(deviceId)")

                        // Ignore this device to avoid to loop on it.
                        (session.crypto as? MXLegacyCrypto)?.ignoreAllPendingKeyRequests(
                            fromUser: userId, andDevice: deviceId) {
                                self?.checkPendingRoomKeyRequests()
                            }
                    }
                },
                failure: { [weak self] error in
                    // Retry later.
                    debugPrint("[\(String(describing: type(of: self)))] checkPendingRoomKeyRequestsInSession: Failed to download device keys. Retry")
                    self?.checkPendingRoomKeyRequests()
                })
        }
    }

    private func showRoomKeyRequestDialog(_ userId: String, deviceInfo: MXDeviceInfo, wasNewDevice: Bool, _ session: MXSession) {

        let startVerifyingAction = AlertHelper.defaultAction("Start verifying".localize()) { _ in
            debugPrint("Start verifying")

            guard userId == session.myUser.userId else {
                // TODO - can we get here for other users?
                return
            }

            VerificationManager.shared.interactiveVerifyOwn(session, deviceInfo)
        }

        let actions = [
            startVerifyingAction,
            AlertHelper.defaultAction("Share without verification".localize()) { action in
                    debugPrint("Share without verifying")

                    // Accept the received requests from this device
                    (session.crypto as? MXLegacyCrypto)?.acceptAllPendingKeyRequests(
                        fromUser: deviceInfo.userId, andDevice: deviceInfo.deviceId) {
                        self.checkPendingRoomKeyRequests()
                    }
            },
            AlertHelper.defaultAction("Ignore".localize(), handler: { action in
                debugPrint("Ignore device room key request")

                // Ignore the received requests from this device
                (session.crypto as? MXLegacyCrypto)?.ignoreAllPendingKeyRequests(
                    fromUser: deviceInfo.userId, andDevice: deviceInfo.deviceId) {
                    self.checkPendingRoomKeyRequests()
                }
            })
        ]

        // New device has logged in.
        let title = "New device".localize()

        let message =
            "Looks like you have logged in with a new device ('%'). Do you want to send this device your room keys?"
            .localize(value: deviceInfo.displayName ?? "Unknown")

        let showAlert = {
            self.roomKeyViewController = AlertHelper.build(message: message, title: title, style: .alert, actions: actions)
            if let vc = UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.rootViewController,
                let alert = self.roomKeyViewController
            {
                vc.present(alert, animated: true, completion: nil)
            }
        }
        
        if let roomKeyViewController = self.roomKeyViewController, roomKeyViewController.isBeingPresented {
            // Old alert open, close that first
            roomKeyViewController.dismiss(animated: false) {
                showAlert()
            }
        } else {
            showAlert()
        }
    }

    @objc private func done() {
        DispatchQueue.main.async {
            self.myDevicesVc?.navigationController?.dismiss(animated: true)

            self.checkPendingRoomKeyRequests()
        }
    }
}
