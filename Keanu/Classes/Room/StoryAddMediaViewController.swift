//
//  StoryAddMediaViewController
//  Keanu
//
//  Created by N-Pex on 31.05.19.
//

import KeanuCore
import AVKit
import Photos

/**
 A small class to keep track of media.
 */
public class MediaInfo: NSObject {
    public var url: URL?
    public var image: UIImage?
    public var uti: String?

    init(url: URL? = nil, image: UIImage? = nil) {
        super.init()

        self.url = url
        self.image = image

        if let url = url {
            self.uti = UtiHelper.uti(for: url)
        }
    }
    
    public var mime: String {
        if image != nil { // If there is an image, force PNG.
            return UtiHelper.mimePng
        }

        if let url = url {
            return UtiHelper.mimeType(for: url)
        }

        return UtiHelper.mimeDefault
    }

    public var isImage: Bool {
        return image != nil || UtiHelper.conforms(uti, to: UtiHelper.typeImage)
    }
    
    public var isAudio: Bool {
        return image == nil && UtiHelper.conforms(uti, to: UtiHelper.typeAudio)
    }
    
    public var isVideo: Bool {
        return image == nil && UtiHelper.conforms(uti, to: UtiHelper.typeMovie)
    }

}

public protocol StoryAddMediaViewControllerDelegate: AnyObject {
    /**
     The user has selected one or more media items. Return "true" to close the view controller, false to keep it open.
     */
    func didSelectMedia(_ media:[MediaInfo]) -> Bool
}

open class StoryAddMediaViewController: UIViewController, AVCapturePhotoCaptureDelegate,
AVAudioRecorderDelegate, AVCaptureFileOutputRecordingDelegate, StoryGalleryViewControllerDelegate {
    
    public weak var delegate: StoryAddMediaViewControllerDelegate?
    
    @IBOutlet weak var cameraPreviewView: UIView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var cameraPulseButton: PulseButtonView!
    @IBOutlet open weak var cameraButtonLayout: UIView?
    @IBOutlet open weak var micButton: UIButton!
    @IBOutlet open weak var micPulseButton: PulseButtonView!
    @IBOutlet open weak var recordingTimeLabel: UILabel?
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var flipCameraButton: UIButton!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var audioPreview: UIView!
    @IBOutlet weak var audioPreviewVisualizer: AudioVisualizerView!
    @IBOutlet weak var audioPreviewPlayer: AudioPlayer!
    @IBOutlet weak var audioRecordingVisualizer: AudioVisualizerView!
    @IBOutlet weak var videoPreview: UIView!
    @IBOutlet weak var lockButtonLayout: UIView!
    @IBOutlet weak var lockButton: UIView!
    @IBOutlet weak var stopLockedRecordingButton: UIButton!
    @IBOutlet weak var cancelRecordButton: UIButton!
    @IBOutlet weak var galleryItems: UIView?
    @IBOutlet weak var galleryItemsStack: UIStackView!
    @IBOutlet weak var galleryBar: UIView?
    
    private lazy var galleryImageManager = PHCachingImageManager()
    
    var isRecordLocked: Bool = false
    var lockButtonRange: CGFloat = 0.0
    
    var videoPreviewPlayer: AVPlayer?
    var videoPreviewPlayerController: AVPlayerViewController?

    var cameraCaptureSession: AVCaptureSession?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    var audioRecorder: AVAudioRecorder?
    
    var currentImage: MediaInfo?
    var currentAudio: MediaInfo?
    var currentVideo: MediaInfo?
    
    var timerStartVideoRecording: Timer? = nil {
        willSet {
            timerStartVideoRecording?.invalidate()
        }
    }
    
    var timerAudioRecordingDecibel: Timer? = nil {
        willSet {
            timerAudioRecordingDecibel?.invalidate()
        }
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()

        imagePreview.isHidden = true
        audioPreview.isHidden = true
        videoPreview.isHidden = true
        cameraButton.isEnabled = false
        cameraPreviewView.isHidden = true
        cameraPulseButton.isHidden = true
        micPulseButton.isHidden = true
        audioRecordingVisualizer.isHidden = true
        cancelRecordButton.isHidden = true
        lockButtonLayout.isHidden = true
        lockButtonRange = lockButtonLayout.bounds.height - lockButton.frame.maxY - lockButton.frame.minY
        setViewerMode(false)
        requestShowCamera()
        
        if galleryItems != nil {
            populateGalleryItems()
        }
    }

    open override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        cameraPreviewLayer?.frame = cameraPreviewView.bounds

        if cameraPreviewLayer?.connection?.isVideoOrientationSupported ?? false {
            var orientation: AVCaptureVideoOrientation?

            switch UIDevice.current.orientation {

            case .portrait:
                orientation = .portrait

            case .portraitUpsideDown:
                orientation = .portraitUpsideDown

            case .landscapeLeft:
                orientation = .landscapeRight // Strangly, this is reversed.

            case .landscapeRight:
                orientation = .landscapeLeft // Strangly, this is reversed.

            default:
                orientation = nil
            }

            if let orientation = orientation {
                cameraPreviewLayer?.connection?.videoOrientation = orientation
            }
        }

        videoPreviewPlayerController?.view.frame = videoPreview.frame
    }

    private lazy var frontVideoDevice: AVCaptureDevice? = {
        return AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)
    }()
    
    private lazy var backVideoDevice: AVCaptureDevice? = {
        return AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
    }()

    private lazy var audioDevice: AVCaptureDevice? = {
        return AVCaptureDevice.default(.builtInMicrophone, for: .audio, position: .unspecified)
    }()

    func startCameraPreview() {
        let videoDevice = frontVideoDevice ?? backVideoDevice
        let videoInput = try? videoDevice != nil ? AVCaptureDeviceInput(device: videoDevice!) : nil
        let audioInput = try? audioDevice != nil ? AVCaptureDeviceInput(device: audioDevice!) : nil

        let output: AVCaptureOutput

        let photoOutput = AVCapturePhotoOutput()
        photoOutput.isHighResolutionCaptureEnabled = true
        photoOutput.isLivePhotoCaptureEnabled = false // photoOutput.isLivePhotoCaptureSupported

        output = photoOutput

        cameraCaptureSession = AVCaptureSession()

        if let videoInput = videoInput {
            cameraCaptureSession?.addInput(videoInput)
        }

        if let audioInput = audioInput {
            cameraCaptureSession?.addInput(audioInput)
        }

        cameraCaptureSession?.addOutput(output)

        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: cameraCaptureSession!)
        cameraPreviewLayer?.videoGravity = .resizeAspectFill
        cameraPreviewLayer?.frame = cameraPreviewView.bounds

        cameraPreviewView.layer.addSublayer(cameraPreviewLayer!)

        cameraCaptureSession?.startRunning()

        cameraButton.isEnabled = true
        cameraPreviewView.isHidden = false
    }
    
    open func requestShowCamera() {
        self.checkCameraAuthorization { authorized in
            if authorized {
                DispatchQueue.main.async {
                    self.startCameraPreview()
                }
            } else {
                print("Permission to use camera denied.")
                // TODO
            }
        }
    }
    
    func checkCameraAuthorization(_ completionHandler: @escaping ((_ authorized: Bool) -> Void)) {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            //The user has previously granted access to the camera.
            completionHandler(true)

        case .notDetermined:
            // The user has not yet been presented with the option to grant video access so request access.
            AVCaptureDevice.requestAccess(for: .video) { success in
                completionHandler(success)
            }

        // .denied: The user has previously denied access.
        // .restricted: The user doesn't have the authority to request access e.g. parental restriction.
        default:
            completionHandler(false)
        }
    }
    
    func takeSnapshot() {
        guard let capturePhotoOutput = cameraCaptureSession?.outputs.first(where: { $0 is AVCapturePhotoOutput }) as? AVCapturePhotoOutput,
            let videoPreviewLayerOrientation = cameraPreviewLayer?.connection?.videoOrientation
            else {
                return
        }

        DispatchQueue.main.async {
            // Update the photo output's connection to match the video orientation of the video preview layer.
            capturePhotoOutput.connection(with: .video)?.videoOrientation = videoPreviewLayerOrientation

            // JPEG should always be available, but just to be sure, we do it very right here.
            var type: AVVideoCodecType? = .jpeg
            if !capturePhotoOutput.availablePhotoCodecTypes.contains(.jpeg) {
                type = capturePhotoOutput.availablePhotoCodecTypes.first
            }

            var format: [String: Any]?
            if let type = type {
                format = [AVVideoCodecKey: type]
            }

            let photoSettings = AVCapturePhotoSettings(format: format)
            photoSettings.isHighResolutionPhotoEnabled = true

            capturePhotoOutput.capturePhoto(with: photoSettings, delegate: self)
        }
    }
    
    @IBAction func didPressSendButton(_ sender: AnyObject) {
        var addedMedia = [MediaInfo]()
        addedMedia.appendOptional(currentImage)
        addedMedia.appendOptional(currentAudio)
        addedMedia.appendOptional(currentVideo)

        /*
         If the delegate returns true, close this view controller
         */
        if delegate?.didSelectMedia(addedMedia) ?? true {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func didPressSwitchCameraButton(_ sender: AnyObject) {
        guard let input = cameraCaptureSession?.inputs.first(where: { ($0 as? AVCaptureDeviceInput)?.device.hasMediaType(.video) ?? false }) as? AVCaptureDeviceInput,
            let newCaptureDevice = input.device.position == .back ? frontVideoDevice : backVideoDevice else
        {
            return
        }

        do {
            let newInput = try AVCaptureDeviceInput(device: newCaptureDevice)
            cameraCaptureSession?.removeInput(input)
            cameraCaptureSession?.addInput(newInput)
        }
        catch {
            print(error)
        }
    }
    
    private var usingFrontCamera: Bool {
        return (cameraCaptureSession?.inputs.first as? AVCaptureDeviceInput)?.device.position == .front
    }

    public func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?)
    {
        if let error = error {
            print(error.localizedDescription)
        }

        if let dataImage = photo.fileDataRepresentation() {
            processCapturedImage(image: UIImage(data: dataImage, scale: 1.0), flipX: usingFrontCamera)
        }
    }
    
    func processCapturedImage(image: UIImage?, flipX: Bool) {
        guard let image = image?.fixOrientation() else {
            //hideCamera()
            return
        }

        cameraCaptureSession?.stopRunning()
        currentImage = MediaInfo(image: image)
        setViewerMode(true)
    }
    
    @IBAction func cameraButtonTouchDown(_ sender: Any) {
        timerStartVideoRecording =  Timer.scheduledTimer(
            timeInterval: TimeInterval(0.5), target: self,
            selector: #selector(timerStartVideoRecordingElapsed),
            userInfo: nil, repeats: false)
    }
    
    @IBAction func cameraButtonTouchUp(_ sender: Any) {
        cameraPulseButton.isHidden = true

        if timerStartVideoRecording == nil {
            if let videoOutput = cameraCaptureSession?.outputs[1] as? AVCaptureMovieFileOutput {
                videoOutput.stopRecording()
            }
            else {
                setViewerMode(false)
            }
        }
        else {
            timerStartVideoRecording = nil
            takeSnapshot()
        }
    }
    
    @objc func timerStartVideoRecordingElapsed() {
        guard let videoFile = videoFile else {
            return
        }

        timerStartVideoRecording = nil
        cameraPulseButton.isHidden = false

        var videoOutput = cameraCaptureSession?.outputs.first(where: { $0 is AVCaptureMovieFileOutput }) as? AVCaptureMovieFileOutput

        if videoOutput == nil {
            videoOutput = AVCaptureMovieFileOutput()
            cameraCaptureSession?.addOutput(videoOutput!)
        }

        videoOutput?.startRecording(to: videoFile, recordingDelegate: self)
    }
    
    @IBAction func cameraButtonTouchUpOutside(_ sender: Any) {
        timerStartVideoRecording = nil
    }
    
    @IBAction func didPressCancelRecord(sender:Any) {
        finishRecording(success: false)

        videoPreviewPlayerController?.willMove(toParent: nil)
        videoPreviewPlayerController?.view.removeFromSuperview()
        videoPreviewPlayerController?.removeFromParent()
        videoPreviewPlayerController = nil

        // Reset
        setViewerMode(false);
    }
    
    @IBAction func didPressStopLockedRecording(sender:Any) {
        finishRecording(success: true)
    }
    
    @IBAction open func micButtonTouchDown(_ sender: Any) {
        startAudioRecordingIfPermitted()
    }
    
    @IBAction open func micButtonTouchUp(_ sender: Any) {
        // If recording is locked it is safe to let the button go!
        guard !isRecordLocked else {return}
        
        micPulseButton.isHidden = true
        // Stop audio recording
        finishRecording(success: true)
    }
    
    @IBAction open func micButtonTouchUpOutside(_ sender: Any) {
        // If recording is locked it is safe to let the button go!
        guard !isRecordLocked else {return}
        finishRecording(success: false)
    }
    
    @IBAction func micButtonTouchDragInside(_ sender: Any, forEvent event: UIEvent) {
        if let touch = event.allTouches?.first {
            didDrag(touch)
        }
    }
    
    @IBAction func micButtonTouchDragOutside(_ sender: Any, forEvent event: UIEvent) {
        if let touch = event.allTouches?.first {
            didDrag(touch)
        }
    }
    
    @objc func didClickCancelButton() {
        setViewerMode(false)
    }
    
    func showCancelButton(_ show: Bool) {
        DispatchQueue.main.async {
            if show {
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(
                    barButtonSystemItem: .cancel, target: self, action: #selector(self.didClickCancelButton))
            }
            else {
                self.navigationItem.rightBarButtonItem = nil
            }
        }
    }
    
    public func setViewerMode(_ viewerMode: Bool) {
        if viewerMode {
            imagePreview.image = currentImage?.image
            imagePreview.isHidden = false

            if currentVideo != nil {
                imagePreview.isHidden = true
                videoPreview.isHidden = false
                micButton.isHidden = true
                audioPreview.isHidden = true
                showVideoPreview(url: currentVideo?.url)
            }
            else {
                videoPreview.isHidden = true
                stopVideoPreview()

                if currentAudio != nil {
                    micButton.isHidden = true
                    audioPreview.isHidden = false
                }
                else {
                    micButton.isHidden = false
                    audioPreview.isHidden = true
                }
            }

            cameraButtonLayout?.isHidden = currentVideo != nil || currentImage != nil
            sendButton.isHidden = false
            flipCameraButton.isHidden = true
            showCancelButton(true)
            galleryBar?.isHidden = true
            galleryItems?.isHidden = true
        }
        else {
            currentAudio = nil
            currentImage = nil
            imagePreview.image = nil
            imagePreview.isHidden = true
            audioPreview.isHidden = true
            videoPreview.isHidden = true
            micButton.isHidden = false
            cameraButtonLayout?.isHidden = false
            sendButton.isHidden = true
            flipCameraButton.isHidden = false
            showCancelButton(false)
            cameraCaptureSession?.startRunning()
            galleryBar?.isHidden = false
            galleryItems?.isHidden = false
        }

        didSetViewerMode(viewerMode)
    }
    
    public func didPick(image: UIImage) {
        currentImage = MediaInfo(image: image)
        setViewerMode(true)
    }


    // MARK: Audio recording

    private lazy var audioFile: URL? = {
         // Hardcoded extension, because kUTTypeMPEG4Audio returns "mp4" as extension, which conforms to kUTTypeMovie.
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            .first?.appendingPathComponent("recording.m4a")
    }()

    private lazy var videoFile: URL? = {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            .first?.appendingPathComponent("recording").appendingPathExtension(UtiHelper.ext(for: UtiHelper.typeQuickTimeMovie))
    }()

    public func startAudioRecordingIfPermitted() {
        let recordingSession = AVAudioSession.sharedInstance()

        do {
            try recordingSession.setCategory(
                .playAndRecord, mode: .default,
                options: [.allowAirPlay, .allowBluetooth, .allowBluetoothA2DP,
                          .defaultToSpeaker, .duckOthers])

            try recordingSession.setActive(true)

            recordingSession.requestRecordPermission() { [weak self] allowed in
                DispatchQueue.main.async {
                    if !allowed {
                        self?.micButton.isEnabled = false
                    } else {
                        self?.startAudioRecording()
                    }
                }
            }
        } catch {
            // failed to record!
            micButton.isEnabled = false
        }
    }
    
    func startAudioRecording() {
        guard let audioFile = audioFile else {
            return
        }

        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 16000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFile, settings: settings)
            audioRecorder?.delegate = self
            audioRecorder?.isMeteringEnabled = true
            timerAudioRecordingDecibel = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(decibelTimerUpdate(_:)), userInfo: nil, repeats: true)
            audioRecorder?.record()
            micPulseButton.isHidden = false
            audioRecordingVisualizer.reset()
            audioRecordingVisualizer.isHidden = false
            didStartRecording()
        }
        catch {
            finishRecording(success: false)
        }
    }
    
    public func finishRecording(success: Bool) {
        audioRecordingVisualizer.isHidden = true
        micPulseButton.isHidden = true
        timerAudioRecordingDecibel = nil
        
        cameraButtonLayout?.isHidden = false
        
        let recordingTime = audioRecorder?.currentTime ?? TimeInterval(0)
        audioRecorder?.stop()
        audioRecorder = nil

        didFinishRecording(success: success)

        if success && recordingTime > 0.5,
            let audioFile = audioFile {

            currentAudio = MediaInfo(url: audioFile)
            audioPreviewVisualizer.loadAudioFile(url: audioFile)
            audioPreviewPlayer.loadAudioFile(url: audioFile)
            setViewerMode(true)
        }
        else {
            if currentAudio == nil, currentImage == nil, currentVideo == nil {
                setViewerMode(false)
            }
        }
    }

    public func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    @objc func decibelTimerUpdate(_ sender: Any) {
        guard let audioRecorder = audioRecorder else {
            return
        }

        audioRecorder.updateMeters()
        let decibel = audioRecorder.averagePower(forChannel: 0)

        var scale: Float = 0

        //Values for human speech range quiet to loud
        let mindB: Float = -80
        let maxdB: Float = -10

        if decibel >= maxdB {
            //too loud
            scale = 1
        }
        else if decibel >= mindB && decibel <= maxdB {
            //normal voice
            let powerFactor: Float = 20
            let mindBScale = pow(10, mindB / powerFactor)
            let maxdBScale = pow(10, maxdB / powerFactor)
            let linearScale = pow (10, decibel / powerFactor)
            let scaleMin: Float = 0
            let scaleMax: Float = 1
            //Get a value between 0 and 1 for mindB & maxdB values
            scale = (((scaleMax - scaleMin) * (linearScale - mindBScale)) / (maxdBScale - mindBScale)) + scaleMin
        }

        audioRecordingVisualizer.addSingleDataValue(scale)

        //let color = UIColor(red: 1, green: CGFloat(1-scale), blue: CGFloat(1-scale), alpha: 1)
        //view.backgroundColor = color

        let duration = audioRecorder.currentTime
        recordingTimeLabel?.text = duration.isFinite && !duration.isNaN ? Formatters.format(duration: duration) : ""
    }
    
    public func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        if error == nil {
            currentVideo = MediaInfo(url: outputFileURL)
        }

        setViewerMode(true)
    }
    
    func showVideoPreview(url: URL?) {
        videoPreviewPlayer?.pause()

        guard let url = url else {
            return
        }

        videoPreviewPlayer = AVPlayer(url: url)

        if videoPreviewPlayerController == nil {
            let controller = AVPlayerViewController()
            videoPreviewPlayerController = controller

            self.addChild(controller)
            controller.view.frame = videoPreview.frame
            videoPreview.addSubview(controller.view)
        }

        videoPreviewPlayerController?.player = videoPreviewPlayer
        videoPreviewPlayer?.play()
    }
    
    func stopVideoPreview() {
        videoPreviewPlayer?.pause()
    }
    
    private func distanceBetween(point1:CGPoint, point2:CGPoint) -> CGFloat {
        return sqrt(pow(point2.x-point1.x,2)+pow(point2.y-point1.y,2));
    }
    
    private func didDrag(_ touch: UITouch) {
        guard !isRecordLocked else {return}
        
        let pointInView = touch.location(in: self.view)
        
        let cancelRecordButtonCenter = cancelRecordButton.superview!.convert(cancelRecordButton.center, to: self.view)
        let lockButtonCenter = lockButton.superview!.convert(lockButton.center, to: self.view)
        let micButtonCenter = micButton.superview!.convert(micButton.center, to: self.view)
        let lockButtonLayoutRect = lockButtonLayout.convert(lockButtonLayout.bounds, to: self.view)
        
        let distanceFromMicX = max(0, micButtonCenter.x - pointInView.x)
        let distanceFromMicY = min(max(0, micButtonCenter.y - pointInView.y), micButtonCenter.y - lockButtonLayoutRect.maxY + lockButtonRange)
        if distanceFromMicX > distanceFromMicY {
            micButton.transform = CGAffineTransform(translationX: -distanceFromMicX, y: 0)
            micPulseButton.transform = micButton.transform
        } else {
            micButton.transform = CGAffineTransform(translationX: 0, y: -distanceFromMicY)
            micPulseButton.transform = micButton.transform
        }
        
        let normalDistanceCancel = distanceBetween(point1: cancelRecordButtonCenter, point2: micButtonCenter)
        let distanceCancel = distanceBetween(point1: cancelRecordButtonCenter, point2: pointInView)
        let fracDistanceCancel = (normalDistanceCancel - distanceCancel)/normalDistanceCancel
        
        let normalDistanceLock = distanceBetween(point1: lockButtonCenter, point2: micButtonCenter)
        let distanceLock = distanceBetween(point1: lockButtonCenter, point2: pointInView)
        let fracDistanceLock = (normalDistanceLock - distanceLock)/normalDistanceLock

        setLockOffset(fraction: getLockOffsetFraction(pointInView: pointInView))
        
        if fracDistanceLock > 0.8 {
            isRecordLocked = true
            stopLockedRecordingButton.isHidden = false
            micButton.isHidden = true
            micButton.transform = .identity
            micPulseButton.transform = .identity
        } else if fracDistanceCancel > 0.8 {
            setLockOffset(fraction: 1.0)
            finishRecording(success: false)
            setViewerMode(false)
        }
    }
        
    private func getLockOffsetFraction(pointInView: CGPoint) -> CGFloat {
        let rect = lockButtonLayout.convert(lockButtonLayout.bounds, to: self.view)
        if pointInView.y <= rect.maxY {
            let offset = rect.maxY - pointInView.y
            return min(1, max(0, offset/lockButtonRange))
        } else {
            return 0.0
        }
    }
    
    private func setLockOffset(fraction: CGFloat) {
        print("Set lock \(fraction)")
        lockButton.transform = CGAffineTransform(translationX: 0, y: CGFloat((1.0 - fraction) * lockButtonRange))
    }
    
    @IBAction open func galleryBarTouchUp(_ sender: Any) {
        endDragGalleryBar()
    }
    
    @IBAction open func galleryBarTouchUpOutside(_ sender: Any) {
        endDragGalleryBar()
    }
    
    @IBAction func galleryBarTouchDragInside(_ sender: Any, forEvent event: UIEvent) {
        if let touch = event.allTouches?.first {
            didDragGalleryBar(touch)
        }
    }
    
    @IBAction func galleryBarTouchDragOutside(_ sender: Any, forEvent event: UIEvent) {
        if let touch = event.allTouches?.first {
            didDragGalleryBar(touch)
        }
    }

    private func endDragGalleryBar() {
        guard let galleryBar = galleryBar else { return }
        let galleryBarCenter = galleryBar.superview!.convert(galleryBar.center, to: self.view)
        
        let distanceY = -galleryBar.transform.ty
        if distanceY > 40 {
            // Open gallery
            UIView.animate(
                withDuration: 0.4,
                animations: {
                    self.galleryBar?.transform = CGAffineTransform(translationX: 0, y: -galleryBarCenter.y)
                    self.galleryItems?.transform = CGAffineTransform(translationX: 0, y: -galleryBarCenter.y)
                },
                completion: { [weak self] _ in
                    guard let self = self else { return }
                    self.galleryBar?.transform = .identity
                    self.galleryItems?.transform = .identity

                    // Open the gallery view
                    let vc = UIApplication.shared.router.storyGallery()
                    vc.delegate = self
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            )
        } else {
            // Go back
            UIView.animate(
                withDuration: 0.2,
                animations: {
                    self.galleryBar?.transform = CGAffineTransform(translationX: 0, y: 0)
                    self.galleryItems?.transform = CGAffineTransform(translationX: 0, y: 0)
                },
                completion: { [weak self] _ in
                    guard let self = self else { return }
                    self.galleryBar?.transform = .identity
                    self.galleryItems?.transform = .identity
                }
            )

        }
    }
    
    private func didDragGalleryBar(_ touch: UITouch) {
        guard let galleryBar = galleryBar else { return }
        let pointInView = touch.location(in: self.view)
        
        let galleryBarCenter = galleryBar.superview!.convert(galleryBar.center, to: self.view)
        
        let distanceY = max(0, galleryBarCenter.y - pointInView.y)
        galleryBar.transform = CGAffineTransform(translationX: 0, y: -distanceY)
        galleryItems?.transform = CGAffineTransform(translationX: 0, y: -distanceY)
    }
    
    open func populateGalleryItems() {
        if PHPhotoLibrary.authorizationStatus() != .authorized {
            PHPhotoLibrary.requestAuthorization { status in
                DispatchQueue.main.async {
                    if status != .authorized {
                        self.onNoGalleryPermission()
                    }
                    else {
                        self.onGalleryPermission()
                    }
                }
            }
        }
        else {
            self.onGalleryPermission()
        }
    }
    
    open func onNoGalleryPermission() {
        galleryItems?.removeFromSuperview()
    }
    
    open func onGalleryPermission() {
        let allowedTypes:[PHAssetMediaType] = [.image] // Only images for now

        let options = PHFetchOptions()
        options.includeAssetSourceTypes = [.typeUserLibrary, .typeCloudShared, .typeiTunesSynced]
        options.fetchLimit = 20

        let assets = PHAsset.fetchAssets(with: options)
        if assets.count > 0 {
            assets.enumerateObjects { asset, index, stop in
                if allowedTypes.contains(asset.mediaType) {
                    let v = UIImageView(forAutoLayout: ())
                    v.autoMatch(.height, to: .width, of: v)
                    self.galleryItemsStack.addArrangedSubview(v)
                    
                    self.galleryImageManager.requestImage(for: asset,
                                              targetSize: v.frame.size,
                                              contentMode: .aspectFit, options: nil)
                    { [weak self, weak v] image, options in
                        guard let self = self, let v = v else {return}
                        v.image = image
                        v.addGestureRecognizer(AssetTapGestureRecognizer(asset: asset, target: self, action: #selector(self.didTapGalleryItem(_:))))
                        v.isUserInteractionEnabled = true
                    }
                }
            }
        }
    }
    
    @objc open func didTapGalleryItem(_ sender: Any) {
        guard let assetTapGestureRecognizer = sender as? AssetTapGestureRecognizer else {
            return
        }

        galleryImageManager.requestImageDataAndOrientation(
            for: assetTapGestureRecognizer.asset,
            options: nil
        ) { [weak self] data, string, orientation, options in
            if let self = self,
               let image = data.flatMap({ UIImage(data: $0) })
            {
                self.didPick(image: image)
            }
        }
    }
    
    // MARK: Events that easily can be overridden in subclasses

    open func didSetViewerMode(_ viewerMode: Bool) {
    }
    
    open func didStartRecording() {
        cameraButtonLayout?.isHidden = true
        cameraPulseButton.isHidden = true
        flipCameraButton.isHidden = true
        isRecordLocked = false
        cancelRecordButton.isHidden = false
        setLockOffset(fraction: 0.0)
        lockButtonLayout.isHidden = false
        stopLockedRecordingButton.isHidden = true
        recordingTimeLabel?.text = ""
        recordingTimeLabel?.isHidden = false
        galleryBar?.isHidden = true
        galleryItems?.isHidden = true
    }
    
    open func didFinishRecording(success: Bool) {
        cancelRecordButton.isHidden = true
        isRecordLocked = false
        lockButtonLayout.isHidden = true
        micButton.transform = .identity
        micPulseButton.transform = .identity
        stopLockedRecordingButton.isHidden = true
        recordingTimeLabel?.isHidden = true
        galleryBar?.isHidden = false
        galleryItems?.isHidden = false
    }
    
    open func importAudioFile(url: URL) {
        currentAudio = MediaInfo(url: url, image: nil)
        audioPreviewVisualizer.loadAudioFile(url: url)
        audioPreviewPlayer.loadAudioFile(url: url)
        setViewerMode(true)
    }
    
    class AssetTapGestureRecognizer: UITapGestureRecognizer {
        let asset:PHAsset
        init(asset: PHAsset, target: Any?, action: Selector?) {
            self.asset = asset
            super.init(target: target, action: action)
        }
    }
}



fileprivate extension UIImage {
    func fixOrientation() -> UIImage {
        
        // No-op if the orientation is already correct
        if imageOrientation == .up {
            return self
        }
        
        // We need to calculate the proper transformation to make the image upright.
        // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
        var transform = CGAffineTransform.identity
        
        if imageOrientation == .down || imageOrientation == .downMirrored {
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: .pi)
        }
        
        if imageOrientation == .left || imageOrientation == .leftMirrored {
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: .pi / 2)
        }
        
        if imageOrientation == .right || imageOrientation == .rightMirrored {
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: -.pi / 2)
        }
        
        if imageOrientation == .upMirrored || imageOrientation == .downMirrored {
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        }
        
        if imageOrientation == .leftMirrored || imageOrientation == .rightMirrored {
            transform = transform.translatedBy(x: size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        }

        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        guard let cgImage = cgImage, let colorSpace = cgImage.colorSpace,
            let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height),
                                bitsPerComponent: cgImage.bitsPerComponent,
                                bytesPerRow: 0,
                                space: colorSpace,
                                bitmapInfo: cgImage.bitmapInfo.rawValue)
            else {
                return self
        }


        ctx.concatenate(transform)
        
        if imageOrientation == .left || imageOrientation == .leftMirrored ||
            imageOrientation == .right || imageOrientation == .rightMirrored
        {
            ctx.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        }
        else {
            ctx.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        }

        // And now we just create a new UIImage from the drawing context and return it
        if let newCgImage = ctx.makeImage() {
            return UIImage(cgImage: newCgImage)
        }
        
        return self
    }
}
