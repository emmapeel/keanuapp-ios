//
//  ReplyEventView.swift
//  Keanu
//
//  Created by Benjamin Erhart on 27.01.22.
//

import UIKit
import MatrixSDK

public protocol ReplyEventViewDelegate: AnyObject {

    func closed(view: ReplyEventView)
}

open class ReplyEventView: UIView {

    open var event: MXEvent?

    open weak var delegate: ReplyEventViewDelegate?

    @discardableResult
    open func showIn(_ view: UIView, _ roomBubbleData: RoomBubbleData, toTopOf: UIView? = nil) -> Self {
        event = roomBubbleData.event

        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .keanuBackground

        let closeBt = UIButton()
        closeBt.setAttributedTitle(NSAttributedString(string: .close, attributes: [.font : UIFont.materialIcons(ofSize: 15)]))
        closeBt.translatesAutoresizingMaskIntoConstraints = false
        closeBt.addTarget(self, action: #selector(close), for: .touchUpInside)

        addSubview(closeBt)
        closeBt.autoPinEdge(toSuperviewEdge: .top, withInset: 4)
        closeBt.autoPinEdge(toSuperviewEdge: .trailing, withInset: 4)

        let content = UILabel()
        content.attributedText = roomBubbleData.formattedText
        content.numberOfLines = 0
        content.translatesAutoresizingMaskIntoConstraints = false

        addSubview(content)
        content.autoPinEdge(toSuperviewEdge: .top, withInset: 8)
        content.autoPinEdge(toSuperviewEdge: .leading, withInset: 8)
        content.autoPinEdge(.trailing, to: .leading, of: closeBt, withOffset: 8)
        content.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8)

        layer.opacity = 0

        view.addSubview(self)
        self.autoPinEdge(toSuperviewEdge: .leading)
        self.autoPinEdge(toSuperviewEdge: .trailing)

        if let viewBelow = toTopOf {
            autoPinEdge(.bottom, to: .top, of: viewBelow)
        }
        else {
            autoPinEdge(toSuperviewEdge: .bottom)
        }

        layoutIfNeeded()

        DispatchQueue.main.async { [weak self] in
            UIView.animate(withDuration: 0.3, animations: {
                self?.layer.opacity = 1
                self?.layoutIfNeeded()
            })
        }

        return self
    }

    @objc open func close() {
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.layer.opacity = 0
            self?.layoutIfNeeded()
        }) { [weak self] _ in
            self?.event = nil
            self?.removeFromSuperview()
            self?.delegate?.closed(view: self!)
        }
    }
}
