//
//  StoryDataSource.swift
//  Keanu
//
//  Created by N-Pex 28.05.19.
//

import KeanuCore

public protocol StoryDataSourceDelegate: AnyObject {
    func audioDataDidChange() // TODO - maybe send some more info?
}
    
open class StoryDataSource: RoomDataSource {
    
    open var roomAudioData:[RoomBubbleData] = []
    public weak var storyDelegate: StoryDataSourceDelegate?
    
    override open func invalidate() {
        super.invalidate()
        roomAudioData.removeAll()
    }
    
    open override func doInitialPagination() {
        // For story mode (if unencrypted), we read in the whole story in one go!
        //if let summary = room.summary { //, !summary.isEncrypted {
        //    delegate?.pagingStarted(direction: .backwards)
        //    pageBackwardsInitial()
        //} else {
            super.doInitialPagination()
        //}
    }
    
    func pageBackwardsInitial() {
        if let timeline = self.timeline, timeline.canPaginate(.backwards) {
            pagingDirection = .backwards
            let completion = { (response:MXResponse<Void>) in
                if response.isSuccess, timeline.canPaginate(.backwards) {
                    self.queue.async {
                        self.pageBackwardsInitial()
                    }
                } else {
                    // Done
                    DispatchQueue.main.async {
                        self.pagingDone(.backwards, success: true, scrollToEventId: nil)
                        self.updateReadReceipts(eventIds: nil)
                    }
                }
            }
            DispatchQueue.global().async {
                timeline.paginate(self.pagingSize, direction: .backwards, onlyFromStore: false, completion: completion)
            }
        }
    }
    
    /**
     Override coalesceBubbles to only include events that are media events!
     */
    open override func coalesceBubbles(existing: RoomBubbleData?, new: RoomBubbleData, direction: MXTimelineDirection) -> Bool {
        
        let storyEvents = new.events.filter { (event) -> Bool in
            return event.isMediaAttachment() && !event.isAudioAttachment()
        }

        let audioEvents = new.events.filter { (event) -> Bool in
            return event.isMediaAttachment() && event.isAudioAttachment()
        }

        // If everything has been filtered out, return true to make this bubble go away!
        if storyEvents.count == 0 {
            if audioEvents.count > 0 {
                // We have audio! Save that.
                new.events = audioEvents
                if direction == .backwards {
                    roomAudioData.insert(new, at: 0)
                } else {
                    roomAudioData.append(new)
                }
                if let delegate = storyDelegate {
                    delegate.audioDataDidChange()
                }
            }
            return true
        }
        new.events = storyEvents
        return super.coalesceBubbles(existing: existing, new: new, direction: direction)
    }
}
