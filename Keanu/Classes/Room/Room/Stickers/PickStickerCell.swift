//
//  PickStickerCell.swift
//  Keanu
//
//  Created by N-Pex on 13.02.20.
//
//

import UIKit

open class PickStickerCell: UICollectionViewCell {
    
    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    public class var defaultReuseId: String {
        return String(describing: self)
    }


    // MARK: Views

    @IBOutlet var imageView: UIImageView!
}
