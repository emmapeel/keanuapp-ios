//
//  EmojiView.swift
//  Keanu
//
//  Created by N-Pex on 07.05.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import UIKit
import KeanuCore

public protocol EmojiViewDelegate: AnyObject {
    func didSelectEmoji(emoji: String)
}

@IBDesignable
public class EmojiView: UIView {
    
    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    public weak var delegate: EmojiViewDelegate?
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var view: UIView?
    
    convenience init(emoji: String, delegate: EmojiViewDelegate?) {
        self.init(frame: CGRect.zero)
        self.delegate = delegate
        label.text = emoji
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override open func prepareForInterfaceBuilder() {
        commonInit()
        view?.prepareForInterfaceBuilder()
    }
    
    private func commonInit() {
        self.view = EmojiView.nib.instantiate(withOwner: self, options: nil).first as? UIView
        guard let view = view else {return}
        addSubview(view)
        view.frame = bounds
        view.translatesAutoresizingMaskIntoConstraints = false
        view.autoPinEdgesToSuperviewEdges()
    }
    
    @IBAction func didSelectQuickReaction(sender:Any) {
        delegate?.didSelectEmoji(emoji: label.text ?? "")
    }
}
