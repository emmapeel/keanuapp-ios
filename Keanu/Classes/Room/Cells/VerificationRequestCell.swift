//
//  VerificationRequestCell.swift
//  Keanu
//
//  Created by Benjamin Erhart on 30.07.20.
//

import KeanuCore

class VerificationRequestCell: UITableViewCell, RoomBubbleDataRenderer {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    public class var defaultReuseId: String {
        return String(describing: self)
    }

    @IBOutlet weak var titleLb: UILabel!

    @IBOutlet weak var matrixIdLb: UILabel!

    @IBOutlet weak var statusLb: UILabel!

    @IBOutlet weak var acceptBt: UIButton! {
        didSet {
            acceptBt.setTitle("Accept".localize())
        }
    }

    @IBOutlet weak var declineBt: UIButton! {
        didSet {
            declineBt.setTitle("Decline".localize())
        }
    }

    private var data: RoomBubbleData?
    private var delegate: RoomBubbleDataRendererDelegate?
    private var verification: MXKeyVerification?


    // MARK: Actions

    @IBAction func accept() {
        if let session = data?.dataSource.room.mxSession,
           let request = verification?.request
        {
            VerificationManager.shared.interactiveVerifyFromEvent(session, request)
        }
        else {
            verification?.state = .requestExpired
            evaluateState()
        }
    }

    @IBAction func decline() {
        verification?.request?.cancel(with: .user(), success: {
            self.verification?.state = .requestCancelledByMe
            self.evaluateState()
        }, failure: { error in
            self.verification?.state = .requestExpired
            self.evaluateState()
        })
    }


    // MARK: RoomBubbleDataRenderer

    func render(roomBubbleData: RoomBubbleData, delegate: RoomBubbleDataRendererDelegate?) {
        data = roomBubbleData
        self.delegate = delegate

        guard let event = data?.events.first,
            let senderId = event.sender else {
                // Hide this row, if everything broken!
                hide()

                return
        }

        NotificationCenter.default.addObserver(self, selector: #selector(verificationChange),
                                               name: .MXKeyVerificationRequestDidChange, object: nil)


        titleLb.text = "% wants to verify".localize(value: roomBubbleData.getName(senderId))
        matrixIdLb.text = senderId

        data?.dataSource.room.mxSession?.crypto.keyVerificationManager.keyVerification(
            fromKeyVerificationEvent: event, roomId: event.roomId, success:
            { verification in

                self.verification = verification

                self.evaluateState()

        }, failure: { error in
            self.hide()
        })
    }


    // MARK: Observers

    @objc private func verificationChange(_ notification: Notification) {
        guard let request = notification.object as? MXLegacyKeyVerificationRequest,
            data?.events.contains(request.event) ?? false
        else {
            return
        }

        data?.dataSource.room.mxSession?.crypto.keyVerificationManager.keyVerification(
            fromKeyVerificationEvent: request.event, roomId: request.event.roomId, success:
            { verification in

                self.verification = verification

                self.evaluateState()

        }, failure: { error in
            self.hide()
        })
    }


    // MARK: Private Methods

    private func hide() {
        contentView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        titleLb.text = nil
        titleLb.isHidden = true
        matrixIdLb.text = nil
        matrixIdLb.isHidden = true
        acceptBt.isHidden = true
        declineBt.isHidden = true
    }

    private func evaluateState() {
        if verification?.state == .requestPending {
            acceptBt.isHidden = false
            declineBt.isHidden = false
            statusLb.isHidden = true
        }
        else {
            acceptBt.isHidden = true
            declineBt.isHidden = true
            statusLb.isHidden = false

            switch verification?.state {

            case .requestCancelled, .requestCancelledByMe, .transactionCancelled, .transactionCancelledByMe:
                statusLb.text = "cancelled".localize()

            case .requestExpired:
                statusLb.text = "expired".localize()

            case .verified:
                statusLb.text = "successful".localize()

            default:
                statusLb.text = "errored".localize()
            }
        }
    }
}
