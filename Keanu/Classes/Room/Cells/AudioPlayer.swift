//
//  AudioPlayer.swift
//  Keanu
//
//  Created by N-Pex on 06.12.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore
import MobileVLCKit


public protocol AudioPlayerDelegate: AnyObject {

    func onPrevious()

    func onNext()

    func onPlayerFinished()
}

/**
 A view coupled with an audio player, for presenting playback options for a media attachment.
 */
@IBDesignable
public class AudioPlayer: UIView, AVAudioPlayerDelegate {
    
    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    weak var delegate: AudioPlayerDelegate?

    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var sliderView: UISlider!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var previousButton: UIButton?
    @IBOutlet weak var nextButton: UIButton?
    @IBOutlet weak var errorButton: UIButton!
    @IBOutlet weak var labelCurrent: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var downloadButton: UIButton?
    @IBOutlet weak var activityIndicatorLabel: UILabel!
    @IBOutlet weak var view: UIView?


    private var avPlayer: AVAudioPlayer?
    private var vlcPlayer: VLCMediaPlayer?
    private var attachment: MXKAttachment?
    private var loadCalled = false
    private var mediaLoader: MXMediaLoader?


    public var onShareCallback: ((_ sourceView: UIView?) -> ())? {
        didSet {
            shareButton.isHidden = onShareCallback == nil
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override open func prepareForInterfaceBuilder() {
        commonInit()
        view?.prepareForInterfaceBuilder()
    }

    deinit {
        close()
    }
    
    /**
     Set bubble data to load. It is assumed that the first event of this bubble contains a media attachment event.
     */
    public func setRoomBubbleData(roomBubbleData: RoomBubbleData) {
        guard let event = roomBubbleData.events.first,
            let attachment = MXKAttachment(event: event, andMediaManager: roomBubbleData.dataSource.room.mxSession.mediaManager) else {
                return}
        setAttachment(attachment: attachment)
    }
    
    public func setAttachment(attachment: MXKAttachment) {
        self.attachment = attachment
        
        // If it's cached, load immediately.
        if let cacheFilePath = attachment.cacheFilePath,
            FileManager.default.fileExists(atPath: cacheFilePath) {
            
            loadAttachment()
        }
    }

    public func loadAttachment() {
        guard let attachment = attachment, !loadCalled else { return }
        loadCalled = true
        
        downloadButton?.isHidden = true
        activityIndicator.startAnimating()
        activityIndicatorLabel.text = ""
        activityIndicatorLabel.isHidden = false
        
        let _ = attachment.downloadData(success: { (data) in
            self.activityIndicator.stopAnimating()
            self.activityIndicatorLabel.isHidden = true
            self.setData(data, url: nil)
        }, failure: { (error) in
            self.activityIndicator.stopAnimating()
            self.activityIndicatorLabel.isHidden = true
            self.showLoadingError()
        }) { (percentage) in
            self.activityIndicatorLabel.text = "Downloading: :percent%".localize(dictionary: ["percent":"\(percentage)"])
        }
    }
    
    public func loadAudioFile(url: URL) {
        reset()
        downloadButton?.isHidden = true
        setData(nil, url: url)
    }
    
    public func reset() {
        close()

        loadCalled = false
        mediaLoader = nil

        resetViews()
    }


    // MARK: AVAudioPlayerDelegate
    
    public func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        playButton.isHidden = false
        pauseButton.isHidden = true
        sliderView.setValue(0, animated: false)
        labelCurrent.text = ""
    }


    // MARK: Actions
    
    @IBAction func didPressDownload(sender: Any) {
        loadAttachment()
    }
    
    @IBAction func didPressPlay(sender: Any) {
        if avPlayer?.isPlaying == false {
            avPlayer?.play()
        }
        else if vlcPlayer?.isPlaying == false {
            vlcPlayer?.play()
        }

        // Delay a little, otherwise vlcPlayer won't be in play mode, yet.
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
            self.updateUi()
        }
    }
    
    @IBAction func didPressPause(sender: Any) {
        if avPlayer?.isPlaying == true {
            avPlayer?.pause()
        }
        else if vlcPlayer?.isPlaying == true {
            vlcPlayer?.pause()
        }
    }

    @IBAction func didChangeSlider(_ sender: Any) {
        if let player = avPlayer {
            player.currentTime = TimeInterval(sliderView.value)
        }
        else if let player = vlcPlayer {
            player.time = VLCTime(number: NSNumber(value: sliderView.value))
        }
    }

    @IBAction func didPressPrevious(sender: Any) {
        delegate?.onPrevious()
    }

    @IBAction func didPressNext(sender: Any) {
        delegate?.onNext()
    }

    @IBAction func didPressShare() {
        onShareCallback?(shareButton)
    }


    // MARK: Private Methods

    private func commonInit() {
        view = AudioPlayer.nib.instantiate(withOwner: self, options: nil).first as? UIView

        guard let view = view else {
            return
        }

        addSubview(view)

        view.frame = bounds
        view.translatesAutoresizingMaskIntoConstraints = false
        view.autoPinEdgesToSuperviewEdges()

        resetViews()
    }

    private func resetViews() {
        shareButton.isHidden = true

        activityIndicator.hidesWhenStopped = true
        activityIndicator.stopAnimating()

        sliderView.isHidden = true
        sliderView.value = 0

        labelCurrent.text = ""
        labelTotal.text = ""

        playButton.isHidden = true
        pauseButton.isHidden = true
        errorButton.isHidden = true

        //NOTE: Visibility of these are controlled by the view owner.
        //previousButton?.isHidden = true
        //nextButton?.isHidden = true

        downloadButton?.isHidden = false
        activityIndicatorLabel.isHidden = true
    }

    private func showLoadingError() {
        errorButton.isHidden = false
    }

    private func setData(_ data: Data?, url: URL?) {
        close()

        var duration: TimeInterval?

        do {
            if let data = data {
                avPlayer = try AVAudioPlayer(data: data)
                avPlayer?.delegate = self
                duration = avPlayer?.duration
            }
            else if let url = url {
                avPlayer = try AVAudioPlayer(contentsOf: url)
                avPlayer?.delegate = self
                duration = avPlayer?.duration
            }
            else {
                showLoadingError()
            }
        }
        catch {
            // Fall back to VLC.

            // TODO: This needs to be randomly created and deleted again!
            let url = URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
                .appendingPathComponent(UUID().uuidString)

            do {
                try data?.write(to: url)

                vlcPlayer = VLCMediaPlayer()
                vlcPlayer?.media = VLCMedia(url: url)

                duration = vlcPlayer?.duration
            }
            catch {
                close()

                showLoadingError()
            }
        }

        if let duration = duration {
            do {
                try AVAudioSession.sharedInstance().setCategory(
                    .playAndRecord, mode: .default,
                    options: [.allowAirPlay, .allowBluetooth, .allowBluetoothA2DP,
                              .defaultToSpeaker, .duckOthers])
            } catch {}

            labelTotal.text = Formatters.format(duration: duration)

            sliderView.minimumValue = 0
            sliderView.maximumValue = Float(duration)
            sliderView.value = 0
            sliderView.isHidden = false

            playButton.isHidden = false
            pauseButton.isHidden = true
        }
    }

    private func close() {
        avPlayer?.stop()
        avPlayer = nil

        vlcPlayer?.stop()

        if let url = vlcPlayer?.media?.url {
            try? FileManager.default.removeItem(at: url)
        }

        vlcPlayer = nil
    }
    
    private func updateUi() {
        if avPlayer?.isPlaying ?? vlcPlayer?.isPlaying ?? false {
            pauseButton.isHidden = false
            playButton.isHidden = true

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.updateUi()
            }
        }
        else {
            pauseButton.isHidden = true
            playButton.isHidden = false

            // Resets play position to 0 for next play button hit.
            // Otherwise, can't be played again.
            // Note, typically, currentTime is at 0 after a full play, but it
            // needs to be stopped anyway, before it can be played again. D'oh!
            if vlcPlayer?.currentTime == 0 || vlcPlayer?.currentTime ?? 0 >= vlcPlayer?.duration ?? 0 {
                vlcPlayer?.stop()
            }
        }

        let duration = avPlayer?.duration ?? vlcPlayer?.duration ?? 0
        let time = avPlayer?.currentTime ?? vlcPlayer?.currentTime ?? 0

        labelCurrent.text = Formatters.format(duration: time)

        sliderView.value = Float(time)
        sliderView.maximumValue = Float(duration)

        labelTotal.text = Formatters.format(duration: duration)
    }
}
