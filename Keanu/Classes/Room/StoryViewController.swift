//
//  StoryViewController.swift
//  Keanu
//
//  Created by N-Pex 27.05.19.
//

import KeanuCore
import AVKit

/**
 View controller to show a story. Items are shown as full screen collection view items.
 */
open class StoryViewController: UIViewController, UICollectionViewDelegateFlowLayout, RoomDataSourceDelegate, StoryToolbarDelegate {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var audioPlayer: AudioPlayer!

    public var dataSource: RoomDataSource?
    public var renderDelegate: RoomBubbleDataRendererDelegate?
    private var playerViewController: AVPlayerViewController?
    private var cellWithMediaFocus: StoryBaseCell?
    private var currentBubble: RoomBubbleData?
    private var nowPlayingAudio: RoomBubbleData?
    
    final let IMAGE_TIMEOUT = 5
    
    var timerAutoAdvance: Timer? = nil {
        willSet {
            timerAutoAdvance?.invalidate()
        }
    }

    /**
     * Set to true to automatically advance to next media item. For images this is after a set time, for video and audio when they are played.
     */
    private var autoAdvance = true
    private var waitingForMoreData = false
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(StoryImageCell.nib, forCellWithReuseIdentifier: StoryImageCell.defaultReuseId)
        collectionView.register(StoryVideoCell.nib, forCellWithReuseIdentifier: StoryVideoCell.defaultReuseId)
        collectionView.register(StoryFileCell.nib, forCellWithReuseIdentifier: StoryFileCell.defaultReuseId)
        collectionView.delegate = self
        collectionView.dataSource = self.dataSource
        collectionView.alwaysBounceHorizontal = true
        updateCurrentItem()
        audioPlayer.delegate = self
        audioPlayer.alpha = 0
    }
    
    public func didScroll(offset: CGFloat) {
    }
    
    public func reloadData() {
        self.collectionView.reloadData()
        updateCurrentItem()
    }
    
    public func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if cell == cellWithMediaFocus {
            cellWithMediaFocus = nil
            if let storyBaseCell = cell as? StoryBaseCell {
                storyBaseCell.cleanup()
            }
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    // Make sure to cache a few bubbles forward.
    public func needsForwardsPaging() -> Bool {
        if !waitingForMoreData, let ds = dataSource, let current = cellWithMediaFocus, let index = collectionView.indexPath(for: current) {
            let numItems = collectionView.numberOfItems(inSection: ds.sectionMessages)
            let thisIndex = index.item
            if thisIndex > (numItems - 2) {
                return true
            }
        }
        return false
    }
    
    //MARK: RoomDataSourceDelegate
    public func didChange(deleted: [IndexPath]?, added: [IndexPath]?, changed: [IndexPath]?, live: Bool, disableScroll: Bool) {
        
        // Before collection view is updated, check if we are on the last item and should auto advance
        // to the next item when done.
        var advanceToNextItem = false
        if autoAdvance, waitingForMoreData, let ds = dataSource, let current = cellWithMediaFocus, let index = collectionView.indexPath(for: current) {
            let numItems = collectionView.numberOfItems(inSection: ds.sectionMessages)
            let thisIndex = index.item
            if thisIndex == (numItems - 1) {
                advanceToNextItem = true
            }
        }

        UICollectionView.setAnimationsEnabled(false)
        collectionView.performBatchUpdates({
            if let deleted = deleted?.filter({$0.section == dataSource?.sectionMessages ?? 1}), deleted.count > 0 {
                collectionView.deleteItems(at: deleted)
            }
            if let changed = changed?.filter({$0.section == dataSource?.sectionMessages ?? 1}), changed.count > 0 {
                collectionView.reloadItems(at: changed)
            }
            if let added = added?.filter({$0.section == dataSource?.sectionMessages ?? 1}), added.count > 0 {
                collectionView.insertItems(at: added)
            }
        }) { (success) in
            if advanceToNextItem {
                self.waitingForMoreData = false
                self.timerAutoAdvanceElapsed()
            } else if let
                ds = self.dataSource as? StoryDataSource,
                let currentBubbleData = self.currentBubble,
                let indexPath = ds.indexPath(for: currentBubbleData) {
                print("Scroll to item \(indexPath.item)")
                self.collectionView.scrollToItem(at: indexPath, at: .left, animated: false)
            } else {
                self.updateCurrentItem()
            }

            // Do we have audio? If not already playing, load up the first piece of audio we have, if any.
            if
                self.nowPlayingAudio == nil,
                let audioBubbles = (self.dataSource as? StoryDataSource)?.roomAudioData,
                let firstAudio = audioBubbles.first
                {
                self.nowPlayingAudio = firstAudio
                self.audioPlayer.setRoomBubbleData(roomBubbleData: firstAudio)
            }
        }
        UICollectionView.setAnimationsEnabled(true)
    }
    
    public func onTypingUsersChange(_ room: MXRoom) {
    }
    
    public func pagingStarted(direction: MXTimelineDirection) {
    }
    
    public func pagingDone(direction: MXTimelineDirection, newIndexPaths: [IndexPath], changedIndexPaths: [IndexPath], success: Bool, scrollToIndexPath: IndexPath?) {
    }
    
    public func createCell(at indexPath: IndexPath, for roomBubbleData: RoomBubbleData) -> UITableViewCell? {
        return nil
    }
    
    public func createCell(type: RoomBubbleDataType, at indexPath: IndexPath) -> UITableViewCell? {
        return nil
    }
    
    public func createCollectionViewCell(at indexPath: IndexPath, for roomBubbleData: RoomBubbleData) -> UICollectionViewCell? {
        var reuseId = StoryImageCell.defaultReuseId
        if roomBubbleData.isVideoStoryItem() {
            reuseId = StoryVideoCell.defaultReuseId
        } else if roomBubbleData.isFileStoryItem() {
            reuseId = StoryFileCell.defaultReuseId
        }

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath)
        if let renderer = cell as? RoomBubbleDataRenderer {
            renderer.render(roomBubbleData: roomBubbleData, delegate: renderDelegate)
        }
        return cell
    }
    
    public func unknownDevicesUpdate(unknownDevices: Int, unknownDeviceUsers: Int) {
    }
    
    public func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        self.updateCurrentItem()
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.updateCurrentItem()
    }
    
    public func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            self.updateCurrentItem()
        }
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x < 0, let ds = self.dataSource, !ds.isPaging, ds.canPageBackwards {
            ds.pageBackwards()
        }
    }
    
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        autoAdvance = false
        timerAutoAdvance = nil
    }
    
    public func getCurrentCellIndexPath() -> IndexPath? {
        guard let ds = self.dataSource else {return nil}
        return self.collectionView.indexPathsForVisibleItems.first ?? IndexPath(item: 0, section: ds.sectionMessages)
    }
    
    private func updateCurrentItem() {
        guard let ds = self.dataSource else {return}
        
        DispatchQueue.main.async {
            let currentCellIndexPath = self.collectionView.indexPathsForVisibleItems.first ?? IndexPath(item: 0, section: ds.sectionMessages)
            print("Update current item \(currentCellIndexPath.item)")

            let total = self.collectionView.numberOfItems(inSection: ds.sectionMessages)
            if total == 0 {
                self.progressView.progress = 0
            } else {
                self.progressView.progress = Float(1 + currentCellIndexPath.row) / Float(total)
            }
            
            // Current item
            if let currentCell = self.collectionView.cellForItem(at: currentCellIndexPath) as? StoryBaseCell {
                self.currentBubble = ds.roomBubbleData(at: currentCellIndexPath)
                self.autoAdvance = true
                if let videoCell = currentCell as? StoryVideoCell {
                    self.createAVPlayer()
                    videoCell.playerController = self.playerViewController
                    
                    // TODO advance when played!
                } else {
                    self.startAutoAdvanceTimer(self.IMAGE_TIMEOUT)
                }
                if currentCell != self.cellWithMediaFocus {
                    self.cellWithMediaFocus?.hasMediaFocus = false
                    currentCell.hasMediaFocus = true
                    self.cellWithMediaFocus = currentCell
                }
            }
        }
    }
    
    private func createAVPlayer() {
        guard playerViewController == nil else {return}
        playerViewController = AVPlayerViewController()
        if let playerViewController = playerViewController {
            addChild(playerViewController)
            playerViewController.didMove(toParent: self)
        }
    }
    
    func startAutoAdvanceTimer(_ timeout:Int) {
        timerAutoAdvance =  Timer.scheduledTimer(
            timeInterval: TimeInterval(timeout),
            target      : self,
            selector    : #selector(StoryViewController.timerAutoAdvanceElapsed),
            userInfo    : nil,
            repeats     : false)
    }
    
    @objc func timerAutoAdvanceElapsed() {
        timerAutoAdvance = nil
        if autoAdvance,
            let ds = dataSource,
            let current = cellWithMediaFocus,
            let index = collectionView.indexPath(for: current) {
            // At end of data?
            if index.item < (collectionView.numberOfItems(inSection: ds.sectionMessages) - 1) {
                waitingForMoreData = false
                collectionView.scrollToItem(at: IndexPath(item: index.item + 1, section: index.section), at: .centeredHorizontally, animated: true)
            } else {
                waitingForMoreData = true
            }
        }
    }
    
    public func didPressAudioButton() {
        UIView.animate(withDuration: 0.5) {
            self.audioPlayer.alpha = (self.audioPlayer.alpha > 0) ? 0 : 1
        }
    }
    
}

extension StoryViewController: StoryDataSourceDelegate {
    public func audioDataDidChange() {
        guard let ds = dataSource as? StoryDataSource else {return}
        if ds.roomAudioData.count > 1 {
            audioPlayer.previousButton?.isHidden = false
            audioPlayer.nextButton?.isHidden = false
        } else {
            audioPlayer.previousButton?.isHidden = true
            audioPlayer.nextButton?.isHidden = true
        }
        if let audio = nowPlayingAudio, let index = ds.roomAudioData.firstIndex(of: audio) {
            audioPlayer.previousButton?.isEnabled = (index > 0)
            audioPlayer.nextButton?.isEnabled = (index < (ds.roomAudioData.count - 1))
        } else {
            audioPlayer.previousButton?.isEnabled = false
            audioPlayer.nextButton?.isEnabled = false
        }
    }
}

extension StoryViewController: AudioPlayerDelegate {
    public func onPrevious() {
        guard let ds = dataSource as? StoryDataSource else {return}
        audioPlayer.reset()
        if let audio = nowPlayingAudio, let index = ds.roomAudioData.firstIndex(of: audio), index > 0 {
            nowPlayingAudio = ds.roomAudioData[index - 1]
            audioPlayer.setRoomBubbleData(roomBubbleData: ds.roomAudioData[index - 1])
            audioDataDidChange()
        }
    }
    
    public func onNext() {
        guard let ds = dataSource as? StoryDataSource else {return}
        audioPlayer.reset()
        if let audio = nowPlayingAudio, let index = ds.roomAudioData.firstIndex(of: audio), index < (ds.roomAudioData.count - 1) {
            nowPlayingAudio = ds.roomAudioData[index + 1]
            audioPlayer.setRoomBubbleData(roomBubbleData: ds.roomAudioData[index + 1])
            audioDataDidChange()
        }
    }
    
    public func onPlayerFinished() {
        // TODO
    }
}

extension RoomBubbleData {
    func isVideoStoryItem() -> Bool {
        guard let event = events.first,
              event.isMediaAttachment(),
              event.decryptionError == nil,
              let attachment = MXKAttachment(event: event, andMediaManager: dataSource.room.mxSession.mediaManager),
              attachment.contentURL != nil,
              attachment.type == .video
        else {
            return false
        }

        return true
    }
    
    func isFileStoryItem() -> Bool {
        guard let event = events.first,
              event.isMediaAttachment(),
              event.decryptionError == nil,
              let attachment = MXKAttachment(event: event, andMediaManager: dataSource.room.mxSession.mediaManager),
              attachment.contentURL != nil,
              attachment.type == .file
        else {
            return false
        }

        return true
    }
}
