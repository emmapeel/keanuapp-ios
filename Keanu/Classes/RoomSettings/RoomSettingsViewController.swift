//
//  RoomSettingsViewController.swift
//  Keanu
//
//  Created by N-Pex on 18.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK
import KeanuCore

public protocol RoomSettingsViewControllerDelegate {

    func didLeaveRoom(_ roomSettingsViewController: RoomSettingsViewController) -> Void

    func didArchiveRoom(_ roomSettingsViewController: RoomSettingsViewController) -> Void
}

/// Cell identifiers from the Rooms.storyboard
public enum StoryboardCellIdentifier: String {
    case desc = "RoomSettingsCell"
    case alias = "RoomAliasCell"
    case encryption = "cellRoomEncryption"
    case addFriends = "cellRoomAddFriends"
    case share = "cellRoomShare"
    case mute = "cellRoomMute"
    case membersLabel = "cellRoomMembers"
    case leave = "cellRoomLeave"
}

open class RoomSettingsViewController: UIViewController {

    private static let shareLink = Config.inviteLinkFormat

    public var delegate: RoomSettingsViewControllerDelegate? = nil
    
    @IBOutlet open weak var tableView: UITableView!

    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    let disabledCellAlphaValue: CGFloat = 0.5
    
    // For matching navigation bar and avatar
    var navigationBarShadow: UIImage?
    var navigationBarBackground: UIImage?
    var topBounceView: UIView?
    
    var notificationToken: NSObjectProtocol? = nil
    
    open var allHeaderRows: [StoryboardCellIdentifier] = [
        .desc, .alias, .encryption, .addFriends,
        .share, .mute, .membersLabel]
    open var headerRows = [StoryboardCellIdentifier]()
    open var footerRows = [StoryboardCellIdentifier]()
    
    open var crownImage: UIImage?
    
    open var room: MXRoom? {
        didSet {
            didChangeRoom(from: oldValue, to: room)
        }
    }
    open var roomMembersDataSource: RoomMembersDataSource?
    open var observers = [NSObjectProtocol]()
    
    deinit {
        roomMembersDataSource = nil
        observers.forEach({ NotificationCenter.default.removeObserver($0) })
        observers.removeAll()
    }

    override open func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Settings".localize()
        
        headerRows = allHeaderRows
        
        footerRows = [.leave]
        
        // Add observers
        observers.append(NotificationCenter.default.addObserver(forName: .mxRoomSummaryDidChange, object: nil, queue: .main)
        { [weak self] notification in
            if let updatedSummary = notification.object as? MXRoomSummary,
                let thisRoom = self?.room,
                updatedSummary.roomId == thisRoom.roomId {

                self?.updateUIBasedOnOwnRole()
            }
        })
        
        // Register cell types
        tableView.register(RoomAliasCell.nib, forCellReuseIdentifier: RoomAliasCell.defaultReuseId)
        tableView.register(RoomSettingsCell.nib, forCellReuseIdentifier: RoomSettingsCell.defaultReuseId)
        tableView.register(RoomMemberCell.nib, forCellReuseIdentifier: RoomMemberCell.defaultReuseId)
        
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = RoomSettingsCell.height
        tableView.estimatedSectionHeaderHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.dataSource = self
        tableView.delegate = self
        
        didChangeRoom(from: nil, to: room)
        updateUIBasedOnOwnRole()

        room?.summary?.updateCanonicalAlias { [weak self] _ in
            if !(self?.room?.summary?.canonicalAlias?.isEmpty ?? true)
                && !(self?.headerRows.contains(.alias) ?? false) {

                self?.headerRows.insert(.alias, at: 1)

                if self?.room?.canSendInvites() ?? false
                    && !(self?.headerRows.contains(.share) ?? false) {

                    self?.headerRows.insert(.share, at: 3)
                }
            }

            self?.tableView.reloadSections(IndexSet(arrayLiteral: 0), with: .none)
        }
    }
    
    func updateUIBasedOnOwnRole() {
        guard let room = room else {
            return
        }
    
        configureHeaderRows(room: room)
        
        // Update the header section
        tableView.reloadSections(IndexSet(arrayLiteral: 0), with: .none)
    }
    
    /**
     Configure which header rows are visible. Override this to hide rows you don't want.
     */
    open func configureHeaderRows(room: MXRoom) {
        headerRows = allHeaderRows

        if room.summary?.canonicalAlias?.isEmpty ?? true {
            headerRows.removeAll { $0 == .alias }
        }
        
        if !room.canSendInvites() {
            headerRows.removeAll { $0 == .addFriends || $0 == .share }
        }
        else if !room.canEditCanonicalAlias() && room.summary?.canonicalAlias?.isEmpty ?? true {
            headerRows.removeAll { $0 == .share }
        }
        
        // If encrypted already, remove the toggle.
        if room.summary?.isEncrypted ?? true {
            headerRows.removeAll { $0 == .encryption }
        }
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tableView {
            // Adjust the frame of the overscroll view.
            if let topBounceView = topBounceView {
                topBounceView.frame = CGRect(x: 0, y: 0,
                                             width: tableView.frame.size.width,
                                             height: tableView.contentOffset.y)
            }
        }
    }
    
    open func didChangeRoom(from: MXRoom?, to: MXRoom?) {
        guard self.isViewLoaded else {
            return
        }
        
        roomMembersDataSource?.delegate = nil
        roomMembersDataSource = nil

        if let room = to {
            roomMembersDataSource = RoomMembersDataSource(room: room)
            roomMembersDataSource?.section = 1
            roomMembersDataSource?.delegate = self
        }

        updateUIBasedOnOwnRole()
    }
    
    open func didUpdateState() {
        tableView?.reloadData()
    }
    
    open func createHeaderCell(type: StoryboardCellIdentifier, at indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: type.rawValue) ?? UITableViewCell(style: .default, reuseIdentifier: type.rawValue)

        let hasMyUser = room?.mxSession?.myUser != nil
        let isOnline = hasMyUser && room?.mxSession?.state == .running

        switch type {
        case .desc:
            guard let cell = cell as? RoomSettingsCell else {
                break
            }

            cell.avatar.load(for: room?.summary)

            cell.nameLb.text = room?.summary?.displayName

            if room?.summary?.topic?.isEmpty ?? true {
                cell.topicLb.text = "Room Topic".localize()
                cell.topicLb.textColor = .systemGray
            }
            else {
                cell.topicLb.text = room?.summary?.topic
                cell.topicLb.textColor = .keanuLabel
            }

            if hasMyUser && room?.canModifyName() ?? false {
                cell.editNameBt.addTarget(self, action: #selector(didPressEditName(_:)), for: .touchUpInside)
            }
            else {
                cell.editNameBt.isHidden = true
                cell.editNameBtWidth.constant = 0
            }

            if hasMyUser && room?.canModifyTopic() ?? false {
                cell.editTopicBt.addTarget(self, action: #selector(didPressEditTopic(_:)), for: .touchUpInside)
            }
            else {
                cell.editTopicBt.isHidden = true
                cell.editTopicBtWidth.constant = 0
            }

        case .alias:
            guard let cell = cell as? RoomAliasCell else {
                break
            }

            if hasMyUser && room?.canEditCanonicalAlias() ?? false {
                cell.editAliasBt.addTarget(self, action: #selector(didPressEditAlias(_:)), for: .touchUpInside)
            }
            else {
                cell.editAliasBt.isHidden = true
                cell.editAliasBtWidth.constant = 0
            }

            cell.aliasLb.text = room?.summary?.canonicalAlias

        case .encryption:
            cell.textLabel?.setIconAndText(.lock, "Encryption".localize())

            let encSwitch = UISwitch()
            encSwitch.isOn = room?.summary?.isEncrypted ?? false

            if isOnline && room?.canEnableEncryption() ?? false && !encSwitch.isOn {
                encSwitch.addTarget(self, action: #selector(didEnableEncryption(_:)), for: .valueChanged)
                encSwitch.isEnabled = true
            }
            else {
                encSwitch.isEnabled = false
            }

            cell.accessoryView = encSwitch

            cell.isUserInteractionEnabled = true

            cell.selectionStyle = .none

        case .addFriends:
            cell.textLabel?.setIconAndText(.person_add, "Add Friends".localize())

            cell.isUserInteractionEnabled = hasMyUser && room?.canSendInvites() ?? false

            cell.contentView.alpha = cell.isUserInteractionEnabled ? 1 : disabledCellAlphaValue

        case .share:
            cell.textLabel?.setIconAndText(.language, "Share".localize())

            cell.isUserInteractionEnabled = hasMyUser && room?.canSendInvites() ?? false

            cell.contentView.alpha = cell.isUserInteractionEnabled ? 1 : disabledCellAlphaValue

        case .mute:
            cell.textLabel?.setIconAndText(.notifications_active, "Notifications".localize())

            let muteSwitch = UISwitch()
            muteSwitch.isOn = room?.notificationLevel != .None
            muteSwitch.addTarget(self, action: #selector(didChangeNotificationSwitch(_:)), for: .valueChanged)

            cell.accessoryView = muteSwitch
            cell.isUserInteractionEnabled = NotificationSettings.shared.notifyCondition == .Individual
            cell.contentView.alpha = cell.isUserInteractionEnabled ? 1 : disabledCellAlphaValue
            cell.selectionStyle = .none

        case .membersLabel:
            cell.textLabel?.text = "Members".localize()
            cell.textLabel?.font = .systemFont(ofSize: 12)

        default:
            break
        }

        return cell
    }
    
    open func createFooterCell(type: StoryboardCellIdentifier, at indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: type.rawValue) ?? UITableViewCell(style: .default, reuseIdentifier: type.rawValue)

        switch type {
        case .leave:
            cell.textLabel?.setIconAndText(.exit_to_app, "Leave".localize())
            cell.textLabel?.textColor = UIColor(hexString: "#fff59d21")

        default:
            break
        }

        return cell
    }
    
    open func didSelectHeaderCell(type: StoryboardCellIdentifier) {
        switch type {
        case .addFriends:
            addMoreFriends()

            break

        case .share:
            if let index = headerRows.firstIndex(of: .share),
                let cell = tableView.cellForRow(at: IndexPath(item: index, section: 0)) {
                shareInviteLink(sourceView: cell)
            }

            break

        default:
            break
        }
    }
    
    open func didSelectFooterCell(type: StoryboardCellIdentifier) {
        switch type {
        case .leave:
            if self.room != nil {
                AlertHelper.present(
                    self,
                    message: "Your room chat history will be wiped away. To keep these chats, archive the room instead.".localize(),
                    title: "Leave room?".localize(),
                    actions: [
                        AlertHelper.defaultAction("Archive".localize()) { action in
                            self.archiveRoom()
                        },
                        AlertHelper.defaultAction("Leave".localize()) { action in
                            self.leaveRoom()
                        },
                        AlertHelper.cancelAction()
                    ])

                return
            }
            break

        default:
            break
        }
    }
    
    @IBAction func didPressEditName(_ sender: Any) {
        guard let room = room else {
            return
        }

        let alert = AlertHelper.build(title: "Change room name".localize(),
                                      actions: [AlertHelper.cancelAction()])

        alert.addAction(AlertHelper.defaultAction() { [weak self] _ in
            if let newName = alert.textFields?.first?.text {
                room.setName(newName) { response in
                    if let error = response.error {
                        self?.handleError(error)
                    }

                    // Success will be picked up by the "updated room summary" event.
                }
            }
        })

        AlertHelper.addTextField(alert, placeholder: room.summary?.displayName, text: room.summary?.displayName)

        present(alert, animated: true)
    }
    
    @IBAction func didPressEditTopic(_ sender: Any) {
        guard let room = room else {
            return
        }

        let alert = AlertHelper.build(title: "Change room topic".localize(),
                                      actions: [AlertHelper.cancelAction()])

        alert.addAction(AlertHelper.defaultAction() { [weak self] _ in
            if let newTopic = alert.textFields?.first?.text {
                room.setTopic(newTopic, completion: { response in
                    if let error = response.error {
                        self?.handleError(error)
                    }

                    // Success will be picked up by the "updated room summary" event.
                })
            }
        })

        AlertHelper.addTextField(alert, placeholder: room.summary?.topic, text: room.summary?.topic)

        present(alert, animated: true)
    }

    @objc func didPressEditAlias(_ sender: Any) {
        guard let room = room,
            let server = MXTools.serverName(inMatrixIdentifier: room.roomId) else {
            return
        }

        let oldAlias = room.summary?.canonicalAlias ?? ""

        let title = oldAlias.isEmpty
            ? "Create Published Address".localize()
            : "Change Published Address".localize()

        let message = oldAlias.isEmpty
            ? "To be able to share a room, it needs a published address.\n\nPlease create one here.".localize()
            : nil

        let alert = AlertHelper.build(message: message, title: title, actions: [AlertHelper.cancelAction()])

        alert.addAction(AlertHelper.defaultAction() { [weak self] _ in
            if var alias = alert.textFields?.first?.text {
                self?.workingOverlay.message = title
                self?.workingOverlay.isHidden = false

                alias = "#" + alias + ":" + server

                room.addAlias(alias) { response in

                    // Ignore errors. If it already exists, we just try to make
                    // it the canoncial one. If it's not ours, an error will be
                    // shown.

                    room.setCanonicalAlias(alias) { response in
                        self?.workingOverlay.isHidden = true

                        if let error = response.error {
                            self?.handleError(error)
                            return
                        }

                        room.summary.canonicalAlias = alias
                        if !(self?.headerRows.contains(.alias) ?? false) {
                            self?.headerRows.insert(.alias, at: 1)
                        }

                        self?.tableView.reloadSections(IndexSet(integer: 0), with: .none)

                        // Finally, try to delete old alias.
                        if !oldAlias.isEmpty {
                            room.removeAlias(oldAlias) { _ in
                                // Ignore. Probably wasn't the users alias.
                            }
                        }
                        else {
                            self?.configRoom(alias, sender as? UIView ?? UIView())
                        }
                    }
                }
            }
        })

        if !oldAlias.isEmpty {
            alert.addAction(AlertHelper.destructiveAction("Delete".localize()) { [weak self] _ in
                room.setCanonicalAlias("") { response in
                    self?.workingOverlay.message = nil
                    self?.workingOverlay.isHidden = false

                    if let error = response.error {
                        self?.handleError(error)
                        return
                    }

                    room.summary?.canonicalAlias = nil
                    self?.headerRows.removeAll { $0 == .alias }

                    room.removeAlias(oldAlias) { response in
                        // Ignore. Probably wasn't the users alias.

                        self?.workingOverlay.isHidden = true
                        self?.tableView.reloadSections(IndexSet(integer: 0), with: .none)
                    }
                }
            })
        }

        let oldName = "^#([A-Z0-9._%#@+-]+):".r?.findFirst(in: oldAlias)?.group(at: 1)

        AlertHelper.addTextField(alert, placeholder: oldName, text: oldName)

        present(alert, animated: true)
    }
    
    @objc func didEnableEncryption(_ sender: UIControl!) {
        guard let encryptionSwitch = sender as? UISwitch, let room = room else {
            return
        }

        if encryptionSwitch.isOn {
            room.enableEncryption(withAlgorithm: kMXCryptoMegolmAlgorithm) { response in
                // TODO - handle failure. Success will be picked up by the "updated room summary" event.
            }
        }
    }
    
    @objc func didChangeNotificationSwitch(_ sender: UISwitch!) {
        room?.notificationLevel = sender.isOn ? .All : .None
    }
    
    /*
     Invite some more friends to this room.
     */
    private func addMoreFriends() {
        let vc = UIApplication.shared.router.chooseFriends()
        vc.delegate = self
        vc.multiselect = true
        vc.selectNewlyAddedFriends = true

        present(UINavigationController(rootViewController: vc), animated: true)
    }

    private func handleError(_ error: Error) {
        workingOverlay.isHidden = true

        AlertHelper.present(self, message: error.localizedDescription)
    }


    /**
     Step 1 of setting everything up to actually be able to share an invite link.

     Retrieves the room's canonical alias. If not available, the user needs to create one.

     If successful, moves to step 2: `#configRoom`.

     - parameter sourceView: The view, which triggered this. Needed for iPad.
    */
    private func shareInviteLink(sourceView: UIView) {
        if let alias = room?.summary?.canonicalAlias,
            !alias.isEmpty {
            configRoom(alias, sourceView)
        }
        else {
            didPressEditAlias(sourceView)
        }
    }

    /**
     Step 2 of setting everything up to actually be able to share an invite link.

     Checks, if the join rule of the room is set to public. If not, tries to set
     it to.

     Also checks, if guest access is disabled, and tries to set it so, if not,
     but continues in both cases, because not important for functionality, just
     a security feature.

     If successful, moves to step 3: `#showShareSheet`.

     - parameter alias: The canonical found or created for the current room.
     - parameter sourceView: The view, which triggered this. Needed for iPad.
    */
    private func configRoom(_ alias: String, _ sourceView: UIView) {
        guard let room = room, let roomId = room.roomId else {
            return
        }

        workingOverlay.message = "Preparing public invitation..."
        workingOverlay.isHidden = false

        let id = alias.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? roomId

        guard let shareUrl = URL(string: String(format: RoomSettingsViewController.shareLink, id)) else {
            // We're a bit lazy here, because this actually shouldn't happen.
            // It would be an app config issue, when then URL would be broken.
            workingOverlay.isHidden = true

            return
        }

        room.mxSession.matrixRestClient.joinRule(ofRoomWithId: roomId) { response in
            if let error = response.error {
                return self.handleError(error)
            }

            if response.value?.joinRule == MXRoomJoinRule.public.identifier {
                // Great! No need to change anything, where we might not have
                // the permission to.
                return self.showShareSheet(shareUrl, sourceView)
            }

            // Set room to public, so users with link can actually join...
            room.setJoinRule(.public) { response in

                // This might fail, when we don't have enough permission.
                // Of course it would be nicer to check before we even show
                // the option, but, alas, that's complicated.

                if let error = response.error {
                    return self.handleError(error)
                }

                // ...but not guests!
                room.setGuestAccess(.forbidden) { response in

                    // We ignore errors here. Guest access is forbidden by
                    // default anyway, so it's quite likely, that it's already
                    // the way we want it. And if not, and we can't change it,
                    // then that doesn't have an impact on this feature, also.

                    self.showShareSheet(shareUrl, sourceView)
                }
            }
        }
    }

    /**
     Step 3 of setting everything up to actually be able to share an invite link.

     Shows the share sheet with the created share URL.

     - parameter shareUrl: The URL to join the room, which should be shared.
     - parameter sourceView: The view, which triggered this. Needed for iPad.
    */
    private func showShareSheet(_ shareUrl: URL, _ sourceView: UIView) {
        workingOverlay.isHidden = true

        let vc = UIActivityViewController(activityItems: [shareUrl], applicationActivities: [ShowQrActivity()])

        vc.popoverPresentationController?.sourceView = sourceView
        vc.popoverPresentationController?.sourceRect = sourceView.bounds

        self.present(vc, animated: true)
    }
}

// MARK: RoomMembersDataSourceDelegate
extension RoomSettingsViewController: RoomMembersDataSourceDelegate {

    public func createCell(at indexPath: IndexPath, for roomMember: MXRoomMember) -> UITableViewCell? {
        guard let room = room, let cellData = roomMembersDataSource?.roomMember(at: indexPath) else {
            return nil
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: RoomMemberCell.defaultReuseId, for: indexPath)

        if let cell = cell as? RoomMemberCell {
            cell.render(member: cellData, room: room)
        }

        return cell
    }
    
    public func didChangeAllRows() {
        tableView?.reloadSections(IndexSet(arrayLiteral: 1), with: .automatic)
    }
    
    public func didChangeRows(at indexPaths: [IndexPath]) {
        tableView?.reloadSections(IndexSet(arrayLiteral: 1), with: .automatic)
    }
}

// MARK: UITableViewDataSource
extension RoomSettingsViewController: UITableViewDataSource {
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return headerRows.count

        case 2:
            return footerRows.count

        default:
            return roomMembersDataSource?.tableView(tableView, numberOfRowsInSection:section) ?? 0
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return createHeaderCell(type: headerRows[indexPath.row], at: indexPath)

        case 2:
            return createFooterCell(type: footerRows[indexPath.row], at: indexPath)

        default:
            return roomMembersDataSource?.tableView(tableView, cellForRowAt: indexPath) ?? UITableViewCell()
        }
    }
}

// MARK: UITableViewDelegate
extension RoomSettingsViewController: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 0 {
            return didSelectHeaderCell(type: headerRows[indexPath.row])
        }
        else if indexPath.section == 2 {
            return didSelectFooterCell(type: footerRows[indexPath.row])
        }
        
        guard let room = room,
            let myUser = room.myUser,
            let dataSource = roomMembersDataSource,
            let member = dataSource.roomMember(at: indexPath),
            // Tapping ourselves is a no-op.
            member.userId != myUser.userId else {

            return
        }
        
        let alert = AlertHelper.build(title: nil, style: .actionSheet, actions: nil)

        if room.canMakeUserAdmin(userId: member.userId) {
            // Action to make someone an admin for a room.
            alert.addAction(AlertHelper.defaultAction("Make admin".localize())
            { action in
                self.makeAdmin(member: member)
            })
        }
        
        if room.canMakeUserModerator(userId: member.userId) {
            // Action to make someone a moderator for a room.
            alert.addAction(AlertHelper.defaultAction("Make moderator".localize())
            { action in
                self.makeModerator(member: member)
            })
        }

        // Action to view profile.
        alert.addAction(AlertHelper.defaultAction("View profile".localize())
        { action in
            self.viewMemberProfile(member: member)
        })

        if room.canUnmakeUserModerator(userId: member.userId) {
            // Action to unmake someone a moderator for a room.
            alert.addAction(AlertHelper.defaultAction("Remove as moderator".localize())
            { action in
                self.unmakeModerator(member: member)
            })
        }
        
        if room.canKickUser(userId: member.userId) {
            // Action to kick someone from a room.
            alert.addAction(AlertHelper.destructiveAction("Kick".localize())
            { action in
                self.kickMember(member: member)
            })
        }
        
        if alert.actions.count == 1 {
            viewMemberProfile(member: member)
        }
        else {
            alert.addAction(AlertHelper.cancelAction())

            if let popoverController = alert.popoverPresentationController,
                let cell = tableView.cellForRow(at: indexPath) {

                popoverController.sourceView = cell
                popoverController.sourceRect = cell.bounds
            }

            present(alert, animated: true)
        }
    }
}

// MARK: Room actions
extension RoomSettingsViewController {

    @objc
    open func leaveRoom() {
        guard let room = room else {
            return
        }

        // Activity indicator when leaving room.
        workingOverlay.message = "Leaving room".localize()
        workingOverlay.isHidden = false

        UIApplication.shared.leaveRoom(room: room) { room, success in
            self.workingOverlay.isHidden = true

            if success,
                let delegate = self.delegate {

                delegate.didLeaveRoom(self)
            }
        }
    }
    
    @objc
    open func archiveRoom() {
        delegate?.didArchiveRoom(self)
    }
    
    @objc
    open func makeAdmin(member: MXRoomMember) {
        room?.setPowerLevel(ofUser: member.userId, powerLevel: 100) { response in
        }
    }
    
    @objc
    open func makeModerator(member: MXRoomMember) {
        room?.setPowerLevel(ofUser: member.userId, powerLevel: 50) { response in
        }
    }
    
    @objc
    open func viewMemberProfile(member: MXRoomMember) {
        let profileVc = UIApplication.shared.router.profile()
        profileVc.friend = FriendsManager.shared.getOrCreate(member, room?.mxSession)

        navigationController?.pushViewController(profileVc, animated: true)
    }
    
    @objc
    open func unmakeModerator(member: MXRoomMember) {
        room?.setPowerLevel(ofUser: member.userId, powerLevel: 0) { response in
        }
    }
    
    @objc
    open func kickMember(member: MXRoomMember) {
        room?.kickUser(member.userId, reason: "") { response in
            // TODO - Handle error and maybe allow editing of a reason?
        }
    }
}

// MARK: ChooseFriendsDelegate
extension RoomSettingsViewController: ChooseFriendsDelegate {

    public func friendsChosen(_ friends: [Friend]) {
        // Invite!
        if let room = room, room.canSendInvites() {
            for friend in friends {
                let invitee = MXRoomInvitee.userId(friend.matrixId)
                room.invite(invitee) { reponse in
                }
            }
        }
    }
}

