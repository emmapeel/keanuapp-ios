//
//  KeyCell.swift
//  Keanu
//
//  Created by Benjamin Erhart on 19.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore
import MatrixSDK

open class DeviceCell: UITableViewCell {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    open class var defaultReuseId: String {
        return String(describing: self)
    }

    public static var minimalHeight: CGFloat = 140

    @IBOutlet weak var badgeLb: UILabel!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var idLb: UILabel!
    @IBOutlet weak var verifiedLb: UILabel!
    @IBOutlet weak var fingerprintLb: UILabel!
    @IBOutlet weak var verifiedSw: UISwitch!


    weak var device: MXDeviceInfo?
    weak var session: MXSession?

    /**
     Applies a device info to this cell.

     - parameter device: The `MXDeviceInfo` for a device.
     - parameter session: The session where the device belongs to.
     - parameter isThisDevice: true, if that is the device info of the device we're running on.
     - returns: self for convenience.
     */
    open func apply(device: MXDeviceInfo?, session: MXSession?, isThisDevice: Bool = false) -> DeviceCell {
        self.device = device
        self.session = session

        let verified = device?.trustLevel?.isVerified ?? false

        DevicesViewController.setBadge(badgeLb, verified)

        if isThisDevice {
            let text = NSMutableAttributedString()

            if let name = device?.displayName {
                text.append(NSAttributedString(string: "\(name) – "))
            }

            text.append(NSAttributedString(
                string: "This Device".localize().localizedUppercase,
                attributes: [.font: UIFont.boldSystemFont(ofSize: nameLb.font.pointSize)]))

            nameLb.attributedText = text
        }
        else {
            nameLb.text = device?.displayName
        }
        idLb.text = device?.deviceId

        if verified {
            verifiedLb.text = "verified".localize()
        }
        else {
            switch device?.trustLevel?.localVerificationStatus {
            case .unknown:
                verifiedLb.text = "new".localize()
            case .unverified:
                verifiedLb.text = "unverified".localize()
            default:
                verifiedLb.text = "blocked".localize()
            }
        }

        fingerprintLb.attributedText = NSAttributedString.fingerprint(
            device?.fingerprint, size: fingerprintLb.font.pointSize)

        verifiedSw.isOn = verified
        verifiedSw.isEnabled = !isThisDevice && !(device?.trustLevel?.isCrossSigningVerified ?? false)

        return self
    }


    // MARK: Actions

    @IBAction func changeVerification() {
        guard let session = session, let device = device else {
            return
        }

        // Switch to blocked can be done right away.
        if !verifiedSw.isOn {
            VerificationManager.shared.verify(session, device, .blocked) { [weak self] error in
                if error != nil {
                    self?.resetSwitch()
                }
            }

            return
        }

        VerificationManager.shared.manualVerify(session, device)

        // Need to reset prophylacticaly, otherwise there will be situations where that
        // switch is on, but the actual values is still off.
        // Instead, if verification was successful, there's going to be a
        // MXDeviceListDidUpdateUsersDevicesNotification or MXDeviceInfoTrustLevelDidChangeNotification
        // which will trigger an update of the list and the switch will be correct.
        resetSwitch()
    }

    private func resetSwitch() {
        verifiedSw.setOn(device?.trustLevel?.isVerified ?? false, animated: true)
    }
}
