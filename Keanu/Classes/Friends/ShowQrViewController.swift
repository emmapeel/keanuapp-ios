//
//  ShowQrViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 06.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

/**
 Scene to show a QR code image and its contents underneath.
 */
open class ShowQrViewController: UIViewController {

    @IBOutlet weak var qrCodeIv: UIImageView!
    @IBOutlet weak var qrCodeTv: UITextView!
    
    public var qrCode = ""
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "QR Code".localize()
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .done, target: self, action: #selector(done))

        qrCodeTv.text = qrCode
    }

    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        qrCodeIv.image = UIImage.qrCode(qrCode, qrCodeIv.bounds.size)
    }


    // MARK: Actions

    @objc func done() {
        if let navC = navigationController {
            navC.dismiss(animated: true)
        }
        else {
            dismiss(animated: true)
        }
    }
}
