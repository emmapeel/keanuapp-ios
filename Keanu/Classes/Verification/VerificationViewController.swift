//
//  VerificationManager.swift
//  Keanu
//
//  Created by Benjamin Erhart on 01.07.19.
//  Copyright © 2019 Guardian Project. All rights reserved.

import KeanuCore
import MatrixSDK

open class VerificationViewController: UIViewController, QrScanViewControllerDelegate {

    public enum State {

        case incomingRequest(_ otherUserId: String, _ otherDeviceId: String)

        case waitingOnAccept

        case chooseMethod(canSas: Bool, canScan: Bool, _ qrCode: Data?)

        case waitingOnChallenge

        case showSas(_ emojis: String?)

        case qrScannedByOther

        case waitingOnConfirm

        case verified(_ otherUserId: String, _ otherDeviceId: String)

        case cancelled(_ reason: String?, byMe: Bool = false)

        case error(_ error: Error?)
    }

    public enum ActionStyle {
        case positive
        case negative
        case neutral
        case laidBack
    }

    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var subtitleLb: UILabel!
    @IBOutlet weak var messageTv: UITextView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var actionBt1: UIButton!
    @IBOutlet weak var actionBt2: UIButton!

    private var transaction: MXKeyVerificationTransaction?
    private var keyVerificationRequest: MXKeyVerificationRequest?

    private var state: State?

    private var token: NSKeyValueObservation?

    private var originalMessageTvTopInset: CGFloat?

    private weak var _session: MXSession?
    private var session: MXSession? {
        get {
            return _session ?? MXKAccountManager.shared().activeAccounts.first?.mxSession
        }
        set {
            _session = newValue
        }
    }

    private var matrixId: String? {
        return session?.myUserId
    }

    private var keyVerificationManager: MXKeyVerificationManager? {
        return session?.crypto?.keyVerificationManager
    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        navigationItem.title = "Verify Device".localize()

        // Finally render state which was given before the view was loaded.
        render()
    }

    open func render(_ session: MXSession?, _ transaction: MXKeyVerificationTransaction) {
        print("[\(String(describing: type(of: self)))] transaction=\(transaction)")

        if let session = session {
            self.session = session
        }

        self.transaction = transaction
        keyVerificationRequest = nil
        token = nil

        if let transaction = transaction as? MXSASTransaction {
            switch transaction.state {
            case MXSASTransactionStateIncomingShowAccept:
                state = .incomingRequest(transaction.otherUserId, transaction.otherDeviceId)

            case MXSASTransactionStateOutgoingWaitForPartnerToAccept:
                state = .waitingOnAccept

            case MXSASTransactionStateWaitForPartnerKey:
                state = .waitingOnChallenge

            case MXSASTransactionStateShowSAS:
                state = .showSas(
                    transaction.sasEmoji?.map { e -> String in "\(e.emoji) \(e.name.capitalized)" }.joined(separator: "\n\n"))

            case MXSASTransactionStateWaitForPartnerToConfirm:
                state = .waitingOnConfirm

            case MXSASTransactionStateVerified:
                state = .verified(transaction.otherUserId, transaction.otherDeviceId)

            case MXSASTransactionStateCancelled:
                state = .cancelled(transaction.reasonCancelCode?.humanReadable)

            case MXSASTransactionStateError:
                state = .error(transaction.error)

            default:
                break
            }
        }
        else if let transaction = transaction as? MXQRCodeTransaction {
            switch transaction.state {
            case .waitingOtherConfirm:
                state = .waitingOnConfirm

            case .verified:
                state = .verified(transaction.otherUserId, transaction.otherDeviceId)

            case .error:
                state = .error(transaction.error)

            case .cancelled, .cancelledByMe:
                state = .cancelled(transaction.reasonCancelCode?.humanReadable, byMe: transaction.state == .cancelledByMe)

            case .qrScannedByOther:
                state = .qrScannedByOther

            default:
                break
            }
        }

        render()
    }

    open func render(_ session: MXSession?, _ request: MXKeyVerificationRequest) {
        print("[\(String(describing: type(of: self)))] request=\(request), state=\(request.state)")

        if let session = session {
            self.session = session
        }

        transaction = nil
        keyVerificationRequest = request
        let defaultRequest = request as? MXLegacyKeyVerificationRequest
        token = defaultRequest?.observe(\.state, changeHandler: requestStateChangeHandler)

        switch request.state {

        case MXKeyVerificationRequestStatePending:
            if request.isFromMyDevice {
                state = .waitingOnAccept
            }
            else {
                if let defaultRequest = defaultRequest {
                    state = .incomingRequest(defaultRequest.otherUser, defaultRequest.fromDevice)
                }
                else {
                    state = .error(nil)
                }
            }

        case MXKeyVerificationRequestStateExpired:
            state = .error(nil)

        case MXKeyVerificationRequestStateCancelled:
            state = .cancelled(nil, byMe: false)

        case MXKeyVerificationRequestStateCancelledByMe:
            state = .cancelled(nil, byMe: true)

        case MXKeyVerificationRequestStateReady:
            state = .chooseMethod(
                canSas: request.methods.contains(MXKeyVerificationMethodSAS),
                canScan: request.methods.contains(MXKeyVerificationMethodQRCodeShow),
                nil)

        default:
            state = .waitingOnChallenge
        }

        render()
    }

    public func render(_ state: State? = nil) {
        if let state = state {
            self.state = state
        }

        print("[\(String(describing: type(of: self)))]#render state=\(String(describing: state))")

        DispatchQueue.main.async { [weak self] in
            guard self?.isViewLoaded ?? false else {
                return
            }

            var title: String?
            var subtitle: String?
            var message: String?
            var image: UIImage?
            var action1: String?
            var actionStyle1 = ActionStyle.laidBack
            var action2: String?
            var actionStyle2 = ActionStyle.laidBack

            switch self?.state {
            case .incomingRequest(let otherUserId, _):
                title = "% wants to verify".localize(value: FriendsManager.shared.getOrCreate(otherUserId).name)
                subtitle = otherUserId
                action1 = "Accept".localize()
                actionStyle1 = .positive
                action2 = "Decline".localize()
                actionStyle2 = .negative

            case .waitingOnAccept:
                title = "Waiting".localize()
                subtitle = "Waiting on other party to accept…".localize()

            case .chooseMethod(let canSas, let canScan, let qrCode):
                title = "Choose Method".localize()

                switch (canSas, canScan, qrCode) {
                case (true, true, .some(qrCode)):
                    subtitle = "Show the other party this QR code, scan theirs or compare emoji instead.".localize()

                case (true, true, .none):
                    subtitle = "Scan the other party's QR code or compare emoji instead.".localize()

                case (false, true, .some(qrCode)):
                    subtitle = "Show the other party this QR code or scan theirs.".localize()

                case (true, false, .some(qrCode)):
                    subtitle = "Show the other party this QR code or compare emoji instead.".localize()

                default:
                    break // These should never happen.
                }

                if let qrCode = qrCode {
                    image = UIImage.qrCode(qrCode, self?.image.bounds.size ?? .zero)
                }

                if canScan {
                    action1 = "Scan QR Code".localize()
                }

                if canSas {
                    action2 = "Compare Emoji".localize()
                }

            case .waitingOnChallenge:
                title = "Waiting".localize()
                subtitle = "Waiting on challenge negotiation…".localize()

            case .showSas(let emojis):
                title = "Compare Emoji".localize()
                subtitle = "Confirm the emoji below are displayed on both sessions, in the same order:".localize()
                message = emojis
                action1 = "They Don't Match".localize()
                actionStyle1 = .negative
                action2 = "They Match".localize()
                actionStyle2 = .positive

            case .qrScannedByOther:
                title = "Scanned".localize()
                subtitle = "Is the other device showing the same shield?".localize()
                message = .verified_user // Material icon; see below.
                action1 = "No".localize()
                actionStyle1 = .negative
                action2 = "Yes".localize()
                actionStyle2 = .positive

            case .waitingOnConfirm:
                title = "Verification Successful!".localize()
                subtitle = "Waiting on confirmation from other device…".localize()
                message = .verified_user // Material icon; see below.

            case .verified(let otherUserId, let otherDeviceId):
                title = "Verification Successful!".localize()

                let deviceName = self?.session?.crypto?.device(
                    withDeviceId: otherDeviceId, ofUser: otherUserId)?.displayName
                    ?? otherDeviceId

                if otherUserId == self?.matrixId {
                    subtitle = "% is now a trusted device.".localize(value: deviceName)
                }
                else {
                    let userName = FriendsManager.shared.getOrCreate(otherUserId).name

                    subtitle = "% of % is now a trusted device.".localize(values:
                        deviceName, userName)
                }

                message = .verified_user // Material icon; see below.
                action1 = "Done".localize()
                actionStyle1 = .neutral

            case .cancelled(let reason, let byMe):
                title = "Cancelled".localize()
                subtitle = byMe ? "Request was cancelled".localize() : "The other party cancelled the request".localize()
                message = "Reason: %".localize(value: reason ?? "No reason available.".localize())
                action1 = "OK".localize()
                actionStyle1 = .neutral

            case .error(let error):
                title = "Error".localize()
                message = error?.localizedDescription ?? "No description available".localize()
                action1 = "OK".localize()
                actionStyle1 = .neutral

            default:
                break
            }

            self?.titleLb.text = title
            self?.titleLb.isHidden = title == nil

            self?.subtitleLb.text = subtitle
            self?.subtitleLb.isHidden = subtitle == nil

            self?.messageTv.text = message
            self?.messageTv.isHidden = message == nil

            // Special handling - shows Material Icon in this case.
            switch self?.state {
            case .qrScannedByOther, .waitingOnConfirm, .verified:
                self?.messageTv.font = .materialIcons(ofSize: 96)
                self?.messageTv.textColor = .systemGreen
                self?.messageTv.textAlignment = .center

                if var inset = self?.messageTv.textContainerInset {
                    if self?.originalMessageTvTopInset == nil {
                        self?.originalMessageTvTopInset = inset.top
                    }
                    inset.top = 100
                    self?.messageTv.textContainerInset = inset
                }

            default:
                if !(message?.isEmpty ?? true) {
                    self?.messageTv.font = .systemFont(ofSize: 17)
                    self?.messageTv.textColor = .keanuLabel
                    self?.messageTv.textAlignment = .natural

                    if var inset = self?.messageTv.textContainerInset {
                        inset.top = self?.originalMessageTvTopInset ?? 0
                        self?.messageTv.textContainerInset = inset
                    }
                }
            }

            self?.image.image = image
            self?.image.isHidden = image == nil

            if let self = self {
                self.setAction(self.actionBt1, action1, actionStyle1)

                self.setAction(self.actionBt2, action2, actionStyle2)
            }
        }
    }


    // MARK: Actions

    @objc func cancel() {
        keyVerificationRequest?.cancel(with: .user(), success: nil)
        transaction?.cancel(with: .user())

        done()
    }

    @IBAction func action1() {
        switch state {
        case .incomingRequest:
            if let keyVerificationRequest = keyVerificationRequest {
                render(.waitingOnAccept)

                self.keyVerificationRequest = nil // Avoid repetition on next tap.

                keyVerificationRequest.accept(
                    withMethods: [MXKeyVerificationMethodQRCodeScan, MXKeyVerificationMethodQRCodeShow, MXKeyVerificationMethodReciprocate, MXKeyVerificationMethodSAS],
                    success: { [weak self] in
                        print("[\(String(describing: type(of: self)))]#keyVerificationRequest.accept success")
                    },
                    failure: { [weak self] (error) in
                        print("[\(String(describing: type(of: self)))]#keyVerificationRequest.accept error=\(error)")

                        self?.render(.error(error))
                })
            }
            else {
                (transaction as? MXIncomingSASTransaction)?.accept()
            }

        case .chooseMethod(_, let canScan, _):
            if canScan {
                let vc = UIApplication.shared.router.qrScan()
                vc.delegate = self

                navigationController?.pushViewController(vc, animated: true)
            }

        case .showSas:
            if keyVerificationRequest?.isFromMyUser ?? false || transaction?.otherUserId == matrixId {
                let vc = UIApplication.shared.router.intrusion()
                vc.matrixId = matrixId

                keyVerificationRequest?.cancel(with: .user(), success: nil)
                transaction?.cancel(with: .user())

                transaction = nil
                keyVerificationRequest = nil
                token = nil

                navigationController?.viewControllers = [vc]
            }
            else {
                cancel()
            }

        case .qrScannedByOther:
            (transaction as? MXQRCodeTransaction)?.otherUserScannedMyQrCode(false)

        case .verified:
            done()

        case .cancelled, .error:
            cancel()

        default:
            break
        }
    }

    @IBAction func action2() {
        switch state {
        case .incomingRequest:
            cancel()

        case .chooseMethod(let canSas, _, _):
                if canSas,
                   let request = keyVerificationRequest ?? keyVerificationManager?.pendingRequests.first
                {
                    startSas(request)
                }

        case .showSas:
            (transaction as? MXSASTransaction)?.confirmSASMatch()

        case .qrScannedByOther:
            (transaction as? MXQRCodeTransaction)?.otherUserScannedMyQrCode(true)

        default:
            break
        }
    }


    // MARK: QrScanViewControllerDelegate

    public func qrCodeFound(data: Data, controller: QrScanViewController) {
        if let transaction = self.transaction as? MXQRCodeTransaction,
            let otherQrCodeData = MXQRCodeDataCoder().decode(data)
        {
            transaction.userHasScannedOtherQrCodeData(otherQrCodeData)
            controller.navigationController?.popViewController(animated: true)
        }
        else {
            controller.showError("That QR code doesn't contain a valid verification code!".localize())
        }
    }


    // MARK: Private Methods

    private func requestStateChangeHandler(_ request: MXLegacyKeyVerificationRequest, _ change: NSKeyValueObservedChange<MXKeyVerificationRequestState>) {
        print("[\(String(describing: type(of: self)))] state changed to \(request.state), change=\(change)")

        switch request.state {
        case MXKeyVerificationRequestStateCancelled,
             MXKeyVerificationRequestStateExpired:
            render(.cancelled(request.reasonCancelCode?.humanReadable))

        case MXKeyVerificationRequestStateAccepted:
            render(.waitingOnChallenge)

        case MXKeyVerificationRequestStateReady:
            if let methods = request.acceptedMethods, !methods.isEmpty {
                if methods.count == 1 && methods.first == MXKeyVerificationMethodSAS {
                    render(.waitingOnChallenge)

                    if request.isFromMyDevice {
                        startSas(request)
                    }
                }
                else {
                    let t = keyVerificationManager?.qrCodeTransaction(withTransactionId: request.requestId)
                    transaction = t

                    var qrCode: Data? = nil

                    if let qrCodeData = t?.qrCodeData {
                        qrCode = MXQRCodeDataCoder().encode(qrCodeData)
                    }

                    render(.chooseMethod(
                        canSas: methods.contains(MXKeyVerificationMethodSAS),
                        // Logic looks reversed, but it seems the methods are the answers of the other device
                        // at this point.
                        canScan: (request.isFromMyDevice && methods.contains(MXKeyVerificationMethodQRCodeShow))
                            || (!request.isFromMyDevice && methods.contains(MXKeyVerificationMethodQRCodeScan)),
                        qrCode))
                }
            }
            else {
                let error = NSError(
                    domain: MXKeyVerificationErrorDomain,
                    code: Int(MXKeyVerificationUnsupportedMethodCode.rawValue),
                    userInfo: [NSLocalizedDescriptionKey: "No mutually supported verification methods.".localize()])

                render(.error(error))
            }

        case MXKeyVerificationRequestStateCancelledByMe:
            cancel()

        default:
            break
        }
    }

    private func done() {
        DispatchQueue.main.async {
            if let navC = self.navigationController {
                navC.dismiss(animated: true)
            }
            else {
                self.dismiss(animated: true)
            }
        }

        transaction = nil
        keyVerificationRequest = nil
        token = nil
    }

    private func startSas(_ request: MXKeyVerificationRequest) {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            if let manager = self?.keyVerificationManager {
                manager.beginKeyVerification(
                    from: request, method: MXKeyVerificationMethodSAS,
                    success: { transaction in
                        // Nothing to do here. Will continue with the MXKeyVerificationTransaction flow.
                    },
                    failure: { [weak self] error in
                        self?.render(.error(error))
                    })
            }
            else {
                self?.render(.error(NSError(domain: "Internal", code: -1, userInfo: [NSLocalizedDescriptionKey: "Internal Error".localize()])))
            }
        }
    }

    private func setAction(_ button: UIButton, _ text: String?, _ style: ActionStyle) {
        guard let text = text else {
            button.isHidden = true
            return
        }

        switch style {
        case .positive:
            button.backgroundColor = .systemGreen
            button.setTitleColor(.white)

        case .negative:
            button.backgroundColor = .systemRed
            button.setTitleColor(.white)

        case .neutral:
            button.backgroundColor = .systemBlue
            button.setTitleColor(.white)

        default:
            button.backgroundColor = .clear
            button.setTitleColor(.systemBlue)
        }

        button.setTitle(text)
        button.isHidden = false
    }
}
