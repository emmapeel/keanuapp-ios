//
//  VerificationManager.swift
//  Keanu
//
//  Created by Benjamin Erhart on 24.06.19.
//  Copyright © 2019 Guardian Project. All rights reserved.

import UIKit
import KeanuCore
import MatrixSDK
import UserNotifications

/**
 https://docs.google.com/document/d/1SXmyjyNqClJ5bTHtwvp8tT1Db4pjlGVxfPQNdlQILqU/edit#
 https://github.com/matrix-org/matrix-doc/pull/2072
 https://github.com/uhoreg/matrix-doc/blob/qr_key_verification/proposals/1543-qr_code_key_verification.md
 */
open class VerificationManager {

    public enum VmError: LocalizedError {

        public var errorDescription: String? {
            switch self {
            case .noBackupFound:
                return "No backup could be found to restore from!".localize()
            default:
                return "Internal Error".localize()
            }
        }

        case internalError
        case noBackupFound
    }
    
    /**
     Singleton instance.
     */
    public static let shared = VerificationManager()

    public static let backupRecoveryKeyDescription = "Matrix Key Backup Recovery Key"

    public lazy var navC: UINavigationController = {
        let navC = UINavigationController(rootViewController: verificationVc)
        navC.modalPresentationStyle = .formSheet

        return navC
    }()

    public lazy var verificationVc: VerificationViewController = {
        return UIApplication.shared.router.verification()
    }()


    public init() {
        let nc = NotificationCenter.default

        nc.addObserver(self, selector: #selector(verification),
                       name: .MXKeyVerificationTransactionDidChange, object: nil)

        nc.addObserver(self, selector: #selector(newRequest),
                       name: .MXKeyVerificationManagerNewRequest, object: nil)

        nc.addObserver(self, selector: #selector(newDevice),
                       name: .MXCrossSigningMyUserDidSignInOnNewDevice, object: nil)
    }

    /**
     Set another device's verified state just like that.

     The user should have acknowledged that they compared fingerprints before calling this to verify a device!
     Better actually: Let the user very through interactive means! (See `#interactiveVerify`!)

     - parameter session: The account session.
     - parameter device: The `MXDeviceInfo` of the device to verify.
     - parameter state: The verification state to set.
     - parameter completion: Completion callback. In case of an error, the error parameter will be non-nil. Optional.
     - parameter error: An error, if one happened.
     */
    open func verify(_ session: MXSession, _ device: MXDeviceInfo, _ state: MXDeviceVerification, _ completion: ((_ error: Error?) -> Void)? = nil) {
        session.crypto?.setDeviceVerification(
            state, forDevice: device.deviceId, ofUser: device.userId,
            success: {
                print("[\(String(describing: type(of: self)))] setDeviceVerification to \(state), success!")

                completion?(nil)
            },
            failure: { [weak self] error in
                print("[\(String(describing: type(of: self)))] setDeviceVerification to \(state), error=\(String(describing: error))")

                completion?(error)
            })
    }

    /**
     Trigger interactive verification of another device.

     Keanu currently supports verification through Emoji compare (method "SAS"), through scanning a QR code,
     through displaying a QR code and through "reciprocate". (The network acknowledgement of the other device,
     that a QR code was scanned and it is legit.)

     - parameter session: The account session.
     - parameter device: The `MXDeviceInfo` of the device to verify.
     - parameter failure: Error callback, in case of an error. Optional.
     - parameter error: An error, if one happened.
     */
    open func interactiveVerify(_ session: MXSession, _ device: MXDeviceInfo, _ failure: ((_ error: Error) -> Void)? = nil) {
        let startVerification = {
            self.presentViewController().render(.waitingOnAccept)

            var methods = [MXKeyVerificationMethodQRCodeShow, MXKeyVerificationMethodReciprocate, MXKeyVerificationMethodSAS]

            if QrScanViewController.canScan {
                methods.append(MXKeyVerificationMethodQRCodeScan)
            }

            session.crypto?.keyVerificationManager.requestVerificationByToDevice(
                withUserId: device.userId, deviceIds: nil,
                methods: methods,
                success: { [weak self] request in
                    print("[\(String(describing: type(of: self)))] requestVerification success, request=\(request)")

                    DispatchQueue.main.async {
                        self?.presentViewController().render(session, request)
                    }
                },
                failure: { [weak self] error in
                    print("[\(String(describing: type(of: self)))] requestVerification, error=\(String(describing: error))")

                    self?.presentViewController().render(.error(error))

                    failure?(error)
                })
        }

        guard !(session.crypto?.crossSigning.canCrossSign ?? true),
              let topVc = UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.rootViewController?.top
        else {
            startVerification()
            return
        }

        topVc.present(PasswordViewController.instantiate(
            title: "Enable Cross Signing".localize(),
            message: "Cross signing on this device is not enabled, yet.\n\nIt's highly recommended to do that, in order to improve your and your peers security!\n\nPlease provide your account password!".localize(),
            prefilledPassword: AuthenticationManager.storedCredentials(for: session.myUserId)?.password
        ) { [weak self] vc in
            guard let password = vc.password else {
                return
            }

            self?.enableCrossSigning(session, password) { error in
                DispatchQueue.main.async {
                    if let error = error {
                        failure?(error)
                    }
                    else {
                        startVerification()
                    }
                }
            }
        }, animated: true)
    }

    open func interactiveVerifyFromEvent(_ session: MXSession, _ request: MXKeyVerificationRequest) {
        presentViewController().render(session, request)

        var methods = [MXKeyVerificationMethodQRCodeShow, MXKeyVerificationMethodReciprocate, MXKeyVerificationMethodSAS]

        if QrScanViewController.canScan {
            methods.append(MXKeyVerificationMethodQRCodeScan)
        }

        request.accept(withMethods: methods, success: { [weak self] in
            print("[\(String(describing: type(of: self)))] requestVerification success, request=\(request)")

            // TODO: We would need the MXQRCodeTransaction here, to be able to show
            // a QR code, but `MXKeyVerificationRequest#accept`
            // is swallowing that, currently. Patch MatrixSDK, send pull request
            // (or wait for changes to happen) and adapt here accordingly!
            DispatchQueue.main.async {
                self?.presentViewController().render(session, request)
            }
        }, failure: { [weak self] error in
            self?.presentViewController().render(.error(error))
        })

    }

    open func interactiveVerifyOwn(_ session: MXSession, _ device: MXDeviceInfo) {
        let vc = UIApplication.shared.router.newDevice()
        vc.session = session
        vc.device = device

        navC.viewControllers = [vc]

        if navC.presentingViewController == nil,
            let rootVc = UIApplication.shared.delegate?.window??.rootViewController {

            rootVc.present(navC, animated: true)
        }
    }

    open func manualVerify(_ session: MXSession, _ device: MXDeviceInfo) {
        let vc = UIApplication.shared.router.manualCompare()
        vc.session = session
        vc.device = device

        navC.viewControllers = [vc]

        if navC.presentingViewController == nil,
            let rootVc = UIApplication.shared.delegate?.window??.rootViewController {

            rootVc.present(navC, animated: true)
        }
    }

    /**
     Enables cross-signing on this device, in case it isn't, already.

     - parameter session: The account session.
     - parameter password: The user's account password.
     - parameter completion: Callback when everything finished. In case of an error happens, contains that. Optional.
     - parameter error: An error, if one happened.
     */
    open func enableCrossSigning(_ session: MXSession, _ password: String, completion: ((_ error: Error?) -> Void)? = nil) {
        guard let csManager = session.crypto?.crossSigning,
            !csManager.canCrossSign else
        {
            completion?(nil)
            return
        }

        csManager.setup(withPassword: password, success: {
            completion?(nil)
        }, failure: { [weak self] error in
            print("[\(String(describing: type(of: self)))]#enableCrossSigning error=\(String(describing: error))")

            completion?(error)
        })
    }

    /**
     Check, if a backup is already ongoing.
     */
    open func checkBackup(_ session: MXSession, completion: @escaping (_ isBackingUp: Bool, _ error: Error?) -> Void) {
        guard let backup = session.crypto?.backup else {
            completion(false, VmError.internalError)
            return
        }

        let check = {
            switch backup.state {
            case MXKeyBackupStateEnabling,
                 MXKeyBackupStateReadyToBackUp,
                 MXKeyBackupStateWillBackUp,
                 MXKeyBackupStateBackingUp:
                completion(true, nil)
            default:
                completion(false, nil)
            }
        }

        if let version = backup.keyBackupVersion
        {
            backup.trust(for: version) { trust in
                check()
            }
        }
        else {
            check()
        }
    }

    /**
     Adds a new room key backup to the server, if there's not already one enabled.

     Tries to store the recovery key to the user's keychain and displays an alert showing the recovery key to the user, if a `vc` is given..

     - parameter session: The account session.
     - parameter password: The user's account password for convenience or a different, special key backup password.
     - parameter vc: A `UIViewController` to display the alert on.
     - parameter completion: Callback when everything finished. In case of an error happens, contains that. Optional.
     - parameter error: An error, if one happened.
     */
    open func addNewBackup(_ session: MXSession, _ password: String,  vc: UIViewController? = nil,
                           completion: ((_ error: Error?) -> Void)? = nil)
    {
        checkBackup(session) { [weak self] isBackingUp, error in
            if let error = error {
                completion?(error)
                return
            }

            // Already backing up, leave alone.
            if isBackingUp {
                completion?(nil)
                return
            }

            guard let backup = session.crypto?.backup else {
                completion?(VmError.internalError)
                return
            }

            backup.prepareKeyBackupVersion(withPassword: password, algorithm: nil, success: { info in
                backup.createKeyBackupVersion(info, success: { _ in

                    let message: String

                    if (try? KeanuKeychain.store(KeanuKeychain.Credentials(matrixId: session.myUserId, password: info.recoveryKey, description: Self.backupRecoveryKeyDescription))) != nil
                    {
                        message = "This is your recovery key:\n\n%\n\nThe recovery key can be used if you forget your backup password. It was also stored to your iOS keychain."
                            .localize(value: info.recoveryKey)
                    }
                    else {
                        message = "Please note down your recovery key:\n\n%\n\nThe recovery key can be used if you forget your backup password and is revealed only once now."
                            .localize(value: info.recoveryKey)
                    }

                    if let vc = vc {
                        AlertHelper.present(vc, message: message, title: "Recovery Key".localize())
                    }

                    completion?(nil)
                }, failure: { error in
                    print("[\(String(describing: type(of: self)))]#addNewBackup error=\(String(describing: error))")

                    completion?(error)
                })
            }, failure: { error in
                print("[\(String(describing: type(of: self)))]#addNewBackup error=\(String(describing: error))")

                completion?(error)
            })
        }
    }

    /**
     Restore the latest room key backup from the server, if there's one available.

     If the provided password fails, this method will try to find a stored key backup recovery key in the keychain and try to use that.

     - parameter session: The account session.
     - parameter password: The key backup password or the recovery key. Might be the same as the account password.
     - parameter completion: Callback when everything finished. In case of an error happens, contains that. Optional.
     - parameter error: An error, if one happened.
     - parameter foundKeys: The number of found keys, if successful.
     - parameter importedKeys: The number of imported keys, if successful. (Should be the same or less than `foundKeys`.)
     */
    open func restoreBackup(_ session: MXSession, _ password: String, completion: ((_ error: Error?, _ foundKeys: UInt?, _ importedKeys: UInt?) -> Void)? = nil) {
        guard let backup = session.crypto?.backup else {
            completion?(VmError.internalError, nil, nil)
            return
        }

        backup.version(nil, success: { version in
            guard let version = version else {
                print("[\(String(describing: type(of: self)))]#restoreBackup error=\(String(describing: VmError.noBackupFound))")

                completion?(VmError.noBackupFound, nil, nil)
                return
            }


            let callback = { [weak self] (foundKeys: UInt, importedKeys: UInt) in
                print("[\(String(describing: type(of: self)))]#restore foundKeys=\(foundKeys), importedKeys=\(importedKeys)")

                completion?(nil, foundKeys, importedKeys)
            }

            backup.restore(version, withPassword: password, room: nil, session: nil,
                               success: callback)
            { [weak self] error in
                backup.restore(
                    version, withRecoveryKey: password, room: nil, session: nil,
                    success: callback) { error in

                        if let recoveryKey = (try? KeanuKeychain.read(KeanuKeychain.Credentials(matrixId: session.myUserId, description: Self.backupRecoveryKeyDescription)))?.password
                        {
                            print("[\(String(describing: type(of: self)))]#restoreBackup with recovery key from keychain.")

                            backup.restore(
                                version, withRecoveryKey: recoveryKey, room: nil, session: nil, success: callback) { error in
                                print("[\(String(describing: type(of: self)))]#restoreBackup error=\(String(describing: error))")

                                completion?(error, nil, nil)

                            }
                        }
                        else {
                            print("[\(String(describing: type(of: self)))]#restoreBackup error=\(String(describing: error))")

                            completion?(error, nil, nil)
                        }
                    }
            }

        }) { [weak self] error in
            print("[\(String(describing: type(of: self)))]#restoreBackup error=\(String(describing: error))")

            completion?(error, nil, nil)
        }
    }

    /**
     Restores the latest room key backup from the server, if there's one available and the password/recovery key is correct,
     or adds a new backup, if no backup found.

     - parameter session: The account session.
     - parameter password: The key backup password or the recovery key. Might be the same as the account password.
     - parameter completion: Callback when everything finished. In case of an error happens, contains that. Optional.
     - parameter error: An error, if one happened.
     */
    open func restoreOrAddNewBackup(_ session: MXSession, _ password: String, completion: ((_ error: Error?) -> Void)? = nil) {
        restoreBackup(session, password) { [weak self] error, foundKeys, importedKeys in
            if let error = error as? VmError, error == .noBackupFound {
                self?.addNewBackup(session, password) { error in
                    completion?(error)
                }
            }
            else {
                completion?(error)
            }
        }
    }

    /**
     Callback for `MXDeviceVerificationTransactionDidChange` notification.

     Handles all SAS and QR code interactive verification states.

     - parameter notification: The notification object.
     */
    @objc open func verification(_ notification: Notification) {
        print("[MXKeyVerification][\(String(describing: type(of: self)))]#verification notification=\(notification)")

        // Don't show again, if it was just cancelled by the user.
        guard let transaction = notification.object as? MXKeyVerificationTransaction,
            (transaction as? MXSASTransaction)?.state != MXSASTransactionStateCancelledByMe else {
                return
        }

        // Remove notifications about now verified devices.
        if (transaction as? MXSASTransaction)?.state == MXSASTransactionStateVerified {
            UNUserNotificationCenter.current().removeDeliveredNotifications(
                withIdentifiers: ["\(PushManager.newDeviceToVerifyId)-\(transaction.otherDeviceId)"])
        }

        presentViewController().render(nil, transaction)
    }

    /**
     Callback for `MXKeyVerificationManagerNewRequest` notification.

     Handles all verification requests from other devices of same user.

     - parameter notification: The notification object.
     */
    @objc open func newRequest(_ notification: Notification) {
        print("[MXKeyVerification][\(String(describing: type(of: self)))]#newRequest notification=\(notification)")

        guard let request = notification.userInfo?[MXKeyVerificationManagerNotificationRequestKey] as? MXKeyVerificationRequest,
            request.state == MXKeyVerificationRequestStatePending else {
                return
        }

        if !request.methods.contains(MXKeyVerificationMethodQRCodeShow)
            && !request.methods.contains(MXKeyVerificationMethodQRCodeShow)
            && !request.methods.contains(MXKeyVerificationMethodSAS)
        {
            request.cancel(with: .unknownMethod(), success: nil)

            return
        }

        presentViewController().render(nil, request)
    }

    /**
     Callback for `MXCrossSigningMyUserDidSignInOnNewDevice` notification.

     Handles new devices of this user.

     - parameter notification: The notification object.
     */
    @objc open func newDevice(_ notification: Notification) {
        guard let deviceIds = (notification.userInfo?["deviceIds"] as? [String]) else {
            return
        }

        // Fixed issue #227: If there's more than one new device, a user signed in on,
        // than, most probably, *this device here* is the actual new device, and
        // all others are older. Don't notify x times about the user's other devices
        // but instead rely on these devices notifying the user about this new device.
        guard deviceIds.count < 2 else {
            return
        }

        for deviceId in deviceIds {
            var device: MXDeviceInfo? = nil
            var matrixId: String? = nil

            for account in MXKAccountManager.shared().activeAccounts {
                matrixId = account.matrixId
                device = account.mxSession?.crypto?.device(withDeviceId: deviceId, ofUser: matrixId!)

                if device != nil {
                    break
                }
            }

            guard matrixId != nil && device != nil else {
                continue
            }

            let body = "% logged into your account. Please confirm it was you.".localize(value: device?.displayName ?? deviceId)
            let userInfo: [AnyHashable : Any] = [PushManager.verifyDeviceIdKey: deviceId,
                            PushManager.verifyDeviceAccountKey: matrixId!]

            let content = UNMutableNotificationContent()
            content.body = body
            content.threadIdentifier = PushManager.newDeviceToVerifyId // Otherwise PushManager will filter this.
            content.userInfo = userInfo

            let request = UNNotificationRequest(identifier: "\(PushManager.newDeviceToVerifyId)-\(deviceId)",
                                                content: content, trigger: nil)

            UNUserNotificationCenter.current().add(request)
        }
    }


    // MARK: Private Methods

    /**
     Presents the `VerificationViewController` modally, if not already presenting
     */
    private func presentViewController() -> VerificationViewController {
        navC.viewControllers = [verificationVc]

        if navC.presentingViewController == nil,
            let rootVc = UIApplication.shared.delegate?.window??.rootViewController {

            rootVc.present(navC, animated: true)
        }

        return verificationVc
    }
}
