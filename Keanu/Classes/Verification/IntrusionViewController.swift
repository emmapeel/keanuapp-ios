//
//  IntrusionViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 29.07.20.
//

import KeanuCore

open class IntrusionViewController: UIViewController {

    open var matrixId: String?

    @IBOutlet weak var headerLb: UILabel! {
        didSet {
            headerLb.text = "Your account is not secure".localize()
        }
    }

    @IBOutlet weak var messageLb: UILabel! {
        didSet {
            messageLb.text = "Your password, server or the internet connection might be accessed without your knowledge."
                .localize()
        }
    }

    @IBOutlet weak var actionLb: UILabel! {
        didSet {
            actionLb.text = "We recommend changing your password and remove the new device from your account immediately."
                .localize()
        }
    }

    @IBOutlet weak var profileBt: UIButton! {
        didSet {
            profileBt.setTitle("Go to Profile".localize())
        }
    }

    @IBOutlet weak var ignoreBt: UIButton! {
        didSet {
            ignoreBt.setTitle("Ignore".localize())
        }
    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel, target: self, action: #selector(ignore))
        navigationItem.title = "New Log In".localize()
    }

    @IBAction func goToProfile() {
        ignore()

        let navC = UIApplication.shared.mainVc?.showMe()

        guard let meVc = navC?.viewControllers.first as? MeViewController,
            let account = MXKAccountManager.shared().account(forUserId: matrixId) else {
                return
        }

        navC?.pushViewController(meVc.createProfileViewController(account), animated: true)
    }

    @IBAction func ignore() {
        if let navC = navigationController {
            navC.dismiss(animated: true)
        }
        else {
            dismiss(animated: true)
        }
    }
}
