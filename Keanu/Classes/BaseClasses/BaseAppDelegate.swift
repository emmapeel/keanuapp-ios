//
//  BaseAppDelegate.swift
//  Keanu
//
//  Created by N-Pex on 28.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import KeanuCore
import UserNotifications

open class BaseAppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: UIApplicationDelegate
    
    // Delegate commands this to be optional, but we instantiate always, because
    // we need to for flow switching in `application:didFinishLaunchingWithOptions`.
    public lazy var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
    
    /**
     Override this for theming support, i.e. change view controller classes etc. See [Router](x-source-tag://Router) for more info.
     */
    open var router: Router {
        get {
            return BaseRouter.shared
        }
    }

    /**
     Override this to inject your configuration *before* calling super!
    */
    open func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        KeanuCore.setUpMatrix()
        KeanuCore.loadFonts()
        
        // Initialize *before* MatrixSDK, so it registers early and
        // can catch all the necessary notifications.
        _ = FriendsManager.shared

        // Initialize *before* MatrixSDK, so it registers early and
        // can catch all the necessary notifications.
        _ = VerificationManager.shared

        // Check, if there's already a Matrix account. If not, show onboarding
        // flow with sign-in/register scenes.
        // Else show main flow.
        
        window?.makeKeyAndVisible()
        
        if MXKAccountManager.shared().accounts.isEmpty {
            UIApplication.shared.startOnboardingFlow()
        }
        else {
            UIApplication.shared.startMainFlow()
        }

        return true
    }
    
    open func applicationDidEnterBackground(_ application: UIApplication) {
        FriendsManager.shared.store()
        MXKAccountManager.shared().activeAccounts.forEach({ (account) in
            account.pauseInBackgroundTask()
        })
    }
    
    open func applicationDidBecomeActive(_ application: UIApplication) {
        // Make sure push is set up.
        PushManager.shared.setupPush()
        
        // Restart any tasks that were paused (or not yet started) while the
        // application was inactive. If the application was previously in the
        //background, optionally refresh the user interface.
        MXKAccountManager.shared().activeAccounts.forEach({ (account) in
            account.resume()
        })
    }
    
    open func applicationWillTerminate(_ application: UIApplication) {
        FriendsManager.shared.store()
    }
    
    open func application(_ app: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return UrlHandler.shared.handle(url)
    }
    
    public func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb,
            let url = userActivity.webpageURL {
            if !UrlHandler.shared.handle(url) {
                UIApplication.shared.open(url)
            }
        }
        return true
    }
    
    
    //MARK: Push support

    open func application(_ application: UIApplication,
                          didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

        PushManager.shared.didRegister(token: deviceToken)
    }
    
    open func application(_ application: UIApplication,
                          didFailToRegisterForRemoteNotificationsWithError error: Error) {

        PushManager.shared.didFailToRegister(error: error)
    }
    
    /**
     Override this to inject your configuration *before* calling super!
     */
    open func application(_ application: UIApplication, didReceiveRemoteNotification
        userInfo: [AnyHashable : Any], fetchCompletionHandler
        completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        PushManager.shared.handleRemoteNotification(userInfo, completionHandler)
    }

    open func application(_ application: UIApplication, performFetchWithCompletionHandler
        completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        print("[\(String(describing: type(of: self)))] Background fetch initiated")
        
        // App entry point. Setup needs to be done.
        KeanuCore.setUpMatrix()
        
        MXKAccountManager.shared().activeAccounts.forEach({ (account) in
            if let state = account.mxSession?.state, state == .paused {
                account.backgroundSync(20000, success: {
                    print("[\(String(describing: type(of: self)))] Background sync succeeded")
                    completionHandler(.newData)
                }, failure: { (error) in
                    print("[\(String(describing: type(of: self)))] Background sync failed")
                    completionHandler(.noData)
                })
            }
        })
    }

    /**
     Callback for when an initial view controller is created from a storyboard.

     - parameter storyboard: The storyboard loaded from.
     - parameter viewController: The view controller that was created, or nil if creation failed.
     */
    open func storyboard(_ storyboard: UIStoryboard,
                         instantiatedInitialViewController viewController: UIViewController?) {
        // Base implementation does nothing.
    }
    
    /**
     Callback for when a view controller is created from a storyboard using the given identifier.

     - parameter storyboard: The storyboard loaded from.
     - parameter viewController: The view controller that was created, or nil if creation failed.
     - parameter identifier: The identifier used when creating.
     */
    open func storyboard(_ storyboard: UIStoryboard,
                         instantiatedViewController viewController: UIViewController?,
                         identifier: String) {
        // Base implementation does nothing.
    }
}
