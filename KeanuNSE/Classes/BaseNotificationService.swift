//
//  BaseNotificationService.swift
//  KeanuNSE
//
//  Created by N-Pex on 17.6.22.
//

import UserNotifications
import MatrixSDK
import KeanuCore
import AudioToolbox

open class BaseNotificationService: UNNotificationServiceExtension {
    
    /**
     Make sure to set these before using the NSE!
     */
    public static var config : KeanuConfig.Type?
    public static var applicationGroupIdentifier : String?
    
    private static var backgroundSyncService: MXBackgroundSyncService!
    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    
    open override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        guard let config = BaseNotificationService.config else {
            contentHandler(request.content) // Use original content
            return
        }
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        KeanuCore.setUp(with: config)
        MXSDKOptions.sharedInstance().applicationGroupIdentifier = BaseNotificationService.applicationGroupIdentifier
        MXKAccountManager.sharedManager(withReload: true)
        DomainHelper.shared.prepareSessionForActiveAccounts()
        
        guard let account = MXKAccountManager.shared().activeAccounts.first else {
            contentHandler(request.content) // Use original content
            return
        }
        
        if let bestAttemptContent = bestAttemptContent,
           let eventId = bestAttemptContent.userInfo["event_id"] as? String,
           let roomId = bestAttemptContent.userInfo["room_id"] as? String,
           let session = account.mxSession
        {
        
            let rewriteMessageText = (bestAttemptContent.body == "MESSAGE")
            
            // If the body is the deafult "MESSAGE", then translate this.
            if rewriteMessageText {
                bestAttemptContent.body = "You have a new message".localize()
            }

            session.event(withEventId: eventId, inRoom: roomId, { response in
                if response.isSuccess,
                   let event = response.value,
                   let room = MXRoom(roomId: roomId, andMatrixSession: account.mxSession) {
                    
                    let isMention = event.shouldBeHighlighted(inSession: session)
                    if isMention, rewriteMessageText {
                        bestAttemptContent.body = "Someone mentioned you".localize()
                    }
                    
                    room.state { stateResult in
                        if let state = stateResult, let pushRule = session.notificationCenter?.rule(matching: event, roomState: state)  {

                            var notify = true
                            var useSound = false
                            var sound: String? = nil
                            var vibrate: Bool = false
                            (pushRule.actions as? [MXPushRuleAction])?.forEach { action in
                                if action.actionType == MXPushRuleActionTypeSetTweak {
                                    if action.parameters["set_tweak"] as? String == "sound" {
                                        useSound = true
                                        sound = action.parameters["value"] as? String
                                    }
                                    if action.parameters["set_tweak"] as? String == "vibration" {
                                        vibrate = action.parameters["value"] as? String == "1"
                                    }
                                }
                            }
                            
                            if [NotificationSettings.keanuPushRuleId, NotificationSettings.keanuPushRuleIdEncrypted].contains(pushRule.ruleId) {
                                // One of our override rules! Use values from the notification settings.
                                switch NotificationSettings.shared.notifyCondition {
                                case NotificationSettings.NotificationCondition.Always:
                                    if isMention {
                                        useSound = NotificationSettings.shared.soundMentions
                                        sound = NotificationSettings.shared.ringtoneMentions
                                        vibrate = NotificationSettings.shared.vibrateMentions
                                    } else {
                                        useSound = NotificationSettings.shared.soundAll
                                        sound = NotificationSettings.shared.ringtoneAll
                                        vibrate = NotificationSettings.shared.vibrateAll
                                    }
                                    break
                                case NotificationSettings.NotificationCondition.Mentions:
                                    if isMention {
                                        useSound = NotificationSettings.shared.soundMentions
                                        sound = NotificationSettings.shared.ringtoneMentions
                                        vibrate = NotificationSettings.shared.vibrateMentions
                                    } else {
                                        notify = false
                                    }
                                    break
                                default:
                                    notify = false
                                    break
                                }
                            } else {
                                sound = isMention ? NotificationSettings.shared.ringtoneMentions : NotificationSettings.shared.ringtoneAll
                            }
                            
                            if useSound {
                                if let sound = sound, sound != "default" {
                                    let soundName = UNNotificationSoundName(rawValue: sound)
                                    bestAttemptContent.sound = UNNotificationSound(named: soundName)
                                } else {
                                    vibrate = false // Default sound already vibrates!
                                    bestAttemptContent.sound = UNNotificationSound.default
                                }
                            } else {
                                bestAttemptContent.sound = nil
                            }

                            if vibrate {
                                UINotificationFeedbackGenerator().notificationOccurred(.success)
                            }
                            
                            if !notify {
                                // Don't deliver the notification to the user.
                                contentHandler(UNNotificationContent())
                                return
                            }
                            
                            contentHandler(bestAttemptContent)
                        } else {
                            contentHandler(bestAttemptContent)
                        }
                    }
                    
                } else {
                    contentHandler(bestAttemptContent)
                }
            })
        } else {
            contentHandler(request.content)
        }
    }
    
    open override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
    
}
